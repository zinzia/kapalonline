<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 07:07:06
         compiled from "application/views\pengaturan/pengajuan/pengajuan_edit.html" */ ?>
<?php /*%%SmartyHeaderCode:117785665220aa587d3-16772281%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f8d0d5d1fbbc1254875594d67976883e160ebb91' => 
    array (
      0 => 'application/views\\pengaturan/pengajuan/pengajuan_edit.html',
      1 => 1441883434,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '117785665220aa587d3-16772281',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <small>Preferences</small>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/pengajuan/');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back</a></li>
        </ul>
        <div class="clear"></div>
    </div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/pengajuan/pengajuan_edit_process');?>
" method="post">
    <input type="hidden" name="field_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['field_id'])===null||$tmp==='' ? '' : $tmp);?>
">
    <table class="table-input" width="100%">
        <tr class="headrow">
            <th colspan="2">Tambah Data Batas Pengajuan</th>
        </tr>
        <tr>
            <td width='20%'>Jenis Penerbangan</td>
            <td width='80%'>
                <select name="data_type" class="data_flight">
                    <option value=""></option>
                    <option value="berjadwal" <?php if ((($tmp = @$_smarty_tpl->getVariable('result')->value['data_type'])===null||$tmp==='' ? '' : $tmp)=='berjadwal'){?>selected="selected"<?php }?>>BERJADWAL</option>
                    <option value="tidak berjadwal" <?php if ((($tmp = @$_smarty_tpl->getVariable('result')->value['data_type'])===null||$tmp==='' ? '' : $tmp)=='tidak berjadwal'){?>selected="selected"<?php }?>>TIDAK BERJADWAL</option>
                    <option value="bukan niaga" <?php if ((($tmp = @$_smarty_tpl->getVariable('result')->value['data_type'])===null||$tmp==='' ? '' : $tmp)=='bukan niaga'){?>selected="selected"<?php }?>>BUKAN NIAGA</option>
                </select>
                <select name="data_flight" class="data_flight">
                    <option value=""></option>
                    <option value="domestik" <?php if ((($tmp = @$_smarty_tpl->getVariable('result')->value['data_flight'])===null||$tmp==='' ? '' : $tmp)=='domestik'){?>selected="selected"<?php }?>>DOMESTIK</option>
                    <option value="internasional" <?php if ((($tmp = @$_smarty_tpl->getVariable('result')->value['data_flight'])===null||$tmp==='' ? '' : $tmp)=='internasional'){?>selected="selected"<?php }?>>INTERNASIONAL</option>
                </select>
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr>
            <td>Remark</td>
            <td>
                <select name="services_cd" class="services_cd">
                    <option value=""></option>
                    <?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_id')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value){
?>
                    <option value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['services_cd'])===null||$tmp==='' ? '' : $tmp);?>
" <?php if ((($tmp = @$_smarty_tpl->tpl_vars['data']->value['services_cd'])===null||$tmp==='' ? '' : $tmp)==(($tmp = @$_smarty_tpl->getVariable('result')->value['services_cd'])===null||$tmp==='' ? '' : $tmp)){?>selected="selected"<?php }?>><?php echo (($tmp = @$_smarty_tpl->tpl_vars['data']->value['services_nm'])===null||$tmp==='' ? '' : $tmp);?>
</option>
                    <?php }} ?>
                </select>
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr>
            <td>Batas</td>
            <td><input type="text" name="batasan" size="3" maxlength="3" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['batasan'])===null||$tmp==='' ? '' : $tmp);?>
" onkeyup="angka(this);"></td>
        </tr>
        <tr class="submit-box">
            <td colspan="2">
                <input type="submit" name="save" value="Simpan" class="submit-button" />
                <input type="reset" name="save" value="Reset" class="reset-button" />
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
    function angka(e) {
        if (!/^[0-9]+$/.test(e.value)) {
            e.value = e.value.substring(0, e.value.length - 1);
        }
    }
</script>