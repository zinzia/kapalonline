<?php /* Smarty version Smarty-3.0.7, created on 2015-12-04 08:26:14
         compiled from "application/views\pengaturan/aircraft/delete.html" */ ?>
<?php /*%%SmartyHeaderCode:22703566140166995d5-68248572%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '97144deae13097f6f24e495e3c78e2a9f5ff0799' => 
    array (
      0 => 'application/views\\pengaturan/aircraft/delete.html',
      1 => 1441883434,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22703566140166995d5-68248572',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/aircraft');?>
">Tipe Pesawat</a><span></span>
        <small>Delete Data</small>
    </p>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/aircraft/delete_process');?>
" method="post" onsubmit="return confirm('Apakah anda yakin akan menghapus data berikut ini?')">
    <input name="aircraft_id" type="hidden" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['aircraft_id'])===null||$tmp==='' ? '' : $tmp);?>
" />
    <table class="table-input" width="100%">
        <tr class="headrow">
            <th colspan="4">Hapus Tipe Pesawat</th>
        </tr>
        <tr>
            <th colspan="4">Apakah anda yakin akan menghapus data dibawah ini?</th>
        </tr>
        <tr>
            <td width="15%">Pembuat</td>
            <td width="35%"><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['aircraft_manufacture'])===null||$tmp==='' ? '' : $tmp);?>
</td>
            <td width="15%">Tipe</td>
            <td width="35%"><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['aircraft_model'])===null||$tmp==='' ? '' : $tmp);?>
</td>
        </tr>  
        <tr>
            <td>Tahun Pembuatan</td>
            <td><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['aircraft_product_year'])===null||$tmp==='' ? '' : $tmp);?>
</td>
            <td>Standard Capacity</td>
            <td><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['aircraft_std_capacity'])===null||$tmp==='' ? '' : $tmp);?>
</td>
        </tr>
        <tr>
            <td width="15%">Deskripsi</td>
            <td width="35%" colspan="3"><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['aircraft_desc'])===null||$tmp==='' ? '' : $tmp);?>
</td>
        </tr>
        <tr class="submit-box">
            <td colspan="4">
                <input type="submit" name="save" value="Delete" class="reset-button" />
            </td>
        </tr>
    </table>
</form>