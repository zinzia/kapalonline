<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 07:36:45
         compiled from "application/views\pengaturan/penggerak_kapal/add.html" */ ?>
<?php /*%%SmartyHeaderCode:806566528fd6bfa15-38466432%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b471cc73a80055d8eedfe543f16c9eea70de3fac' => 
    array (
      0 => 'application/views\\pengaturan/penggerak_kapal/add.html',
      1 => 1449469596,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '806566528fd6bfa15-38466432',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/penggerak_kapal');?>
">Penggerak Kapal</a><span></span>
        <small>Add Data</small>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/penggerak_kapal');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/penggerak_kapal/add_process');?>
" method="post">
    <table class="table-input" width="100%" border='0'>
        <tr class="headrow">
            <th colspan="4">Tambah Data Jenis Kapal</th>
        </tr>
        <tr>
            <td width="15%">Nama Penggerak</td>
            <td width="35%">
                <input name="penggerak_ket" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['penggerak_ket'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="50" />
                <em>* wajib diisi</em>
            </td>
        </tr>  
        <tr>
            <td>Nama Penggerak(ENG)</td>
            <td>
                <input name="penggerak_ket_eng" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['penggerak_ket_eng'])===null||$tmp==='' ? '-' : $tmp);?>
" size="30" maxlength="50" />
            </td>
        </tr>
        <tr class="submit-box">
            <td colspan="4">
                <input type="submit" name="save" value="Simpan" class="submit-button" />
                <input type="reset" name="save" value="Reset" class="reset-button" />
            </td>
        </tr>
    </table>
</form>