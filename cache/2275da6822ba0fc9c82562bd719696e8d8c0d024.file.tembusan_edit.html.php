<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 15:23:52
         compiled from "application/views\pengaturan/preferences/tembusan_edit.html" */ ?>
<?php /*%%SmartyHeaderCode:2917056654218297294-40926739%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2275da6822ba0fc9c82562bd719696e8d8c0d024' => 
    array (
      0 => 'application/views\\pengaturan/preferences/tembusan_edit.html',
      1 => 1441883433,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2917056654218297294-40926739',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <small>Preferences</small>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/preferences/');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back</a></li>
        </ul>
        <div class="clear"></div>
    </div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/preferences/tembusan_edit_process');?>
" method="post">
    <input type="hidden" name="redaksional_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['redaksional_id'])===null||$tmp==='' ? '' : $tmp);?>
">
    <table class="table-input" width="100%">
        <tr class="headrow">
            <th colspan="2">Tambah Data Tembusan</th>
        </tr>
        <tr>
            <td width='20%'>Tembusan</td>
            <td width='80%'>
                <input type="text" name="redaksional_nm" maxlength="255" size="100" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['redaksional_nm'])===null||$tmp==='' ? '' : $tmp);?>
" /> 
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr>
            <td>Email</td>
            <td>
                <input type="text" name="redaksional_mail" maxlength="255" size="50" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['redaksional_mail'])===null||$tmp==='' ? '' : $tmp);?>
" /> 
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr class="submit-box">
            <td colspan="2">
                <input type="submit" name="save" value="Simpan" class="submit-button" />
                <input type="reset" name="save" value="Reset" class="reset-button" />
            </td>
        </tr>
    </table>
</form>
