<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 17:12:32
         compiled from "application/views\pengaturan/pelabuhan/edit.html" */ ?>
<?php /*%%SmartyHeaderCode:956956655b900eb121-38121629%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5a0201fce6dc5ad4bb15e4a5269cf2454e21fe47' => 
    array (
      0 => 'application/views\\pengaturan/pelabuhan/edit.html',
      1 => 1449483127,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '956956655b900eb121-38121629',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/pelabuhan');?>
">Pelabuhan</a><span></span>
        <small>Edit Data</small>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/pelabuhan');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/pelabuhan/edit_process');?>
" method="post">
    <input name="pelabuhan_id" type="hidden" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['pelabuhan_id'])===null||$tmp==='' ? '' : $tmp);?>
" />
    <table class="table-input" width="100%" border='0'>
        <tr class="headrow">
            <th colspan="4">Edit Data Pelabuhan</th>
        </tr>
        <tr>
            <td width="15%">Nama Pelabuhan</td>
            <td width="35%">
                <input name="pelabuhan_nm" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['pelabuhan_nm'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="50" />
                <em>* wajib diisi</em>
            </td>
        </tr>  
        <tr>
            <td>Kode Pelabuhan</td>
            <td>
                <input name="pelabuhan_kd" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['pelabuhan_kd'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="3" />
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr>
            <td>Tempat Pendaftaran</td>
            <td>
                <select name="st_daftar" class="st_daftar">
                    <option value="">--- Pilih ---</option>
                    <option value="1" <?php ob_start();?><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['st_daftar'])===null||$tmp==='' ? '' : $tmp);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1=="1"){?>selected="selected"<?php }?>>YA</option>
                    <option value="0" <?php ob_start();?><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['st_daftar'])===null||$tmp==='' ? '' : $tmp);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2=="0"){?>selected="selected"<?php }?>>TIDAK</option>
                </select>
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr>
            <td>Tempat Pengukuran</td>
            <td>
                <select name="st_ukur" class="st_ukur">
                    <option value="">--- Pilih ---</option>
                    <option value="1" <?php ob_start();?><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['st_ukur'])===null||$tmp==='' ? '' : $tmp);?>
<?php $_tmp3=ob_get_clean();?><?php if ($_tmp3=="1"){?>selected="selected"<?php }?>>YA</option>
                    <option value="0" <?php ob_start();?><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['st_ukur'])===null||$tmp==='' ? '' : $tmp);?>
<?php $_tmp4=ob_get_clean();?><?php if ($_tmp4=="0"){?>selected="selected"<?php }?>>TIDAK</option>
                </select>
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr class="submit-box">
            <td colspan="4">
                <input type="submit" name="save" value="Simpan" class="submit-button" />
                <input type="reset" name="save" value="Reset" class="reset-button" />
            </td>
        </tr>
    </table>
</form>