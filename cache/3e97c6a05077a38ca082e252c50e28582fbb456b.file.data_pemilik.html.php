<?php /* Smarty version Smarty-3.0.7, created on 2015-12-08 22:22:38
         compiled from "application/views\pendaftaran_kapal/akta/data_pemilik.html" */ ?>
<?php /*%%SmartyHeaderCode:147495666f5be201ec5-91256459%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3e97c6a05077a38ca082e252c50e28582fbb456b' => 
    array (
      0 => 'application/views\\pendaftaran_kapal/akta/data_pemilik.html',
      1 => 1449588154,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '147495666f5be201ec5-91256459',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript">
    $(document).ready(function () {
        // date picker
        $(".tanggal").datepicker({
            showOn: 'both',
            changeMonth: true,
            changeYear: true,
            buttonImage: '<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
/resource/doc/images/icon/calendar.gif',
            buttonImageOnly: true,
            dateFormat: 'yy-mm-dd',
        });
        
    });
</script>
<div class="breadcrum">
    <p>
     <a href="#">Pengajuan Akta Pendaftaran</a><span></span>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('member/akta_pendaftaran/');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" />Back to Draft List</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div class="step-box">
    <ul>
        <li>
            <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(('pendaftaran_kapal/akta/data_pengajuan/').($_smarty_tpl->getVariable('result')->value['registrasi_id']));?>
" class="normal"><b>Langkah 1</b><br />Data Pengajuan</a>
        </li>
        <li>
            <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(('pendaftaran_kapal/akta/data_kapal/').($_smarty_tpl->getVariable('result')->value['registrasi_id']));?>
" class="normal"><b>Langkah 2</b><br />Data Kapal</a>
        </li>
        <li>
            <a href="#" class="active"><b>Langkah 3</b><br />Data Pemilik</a>
        </li>
        <li>
            <a href="#" class="normal"><b>Langkah 4</b><br />Upload File</a>
        </li>
        <li>
            <a href="#" class="normal"><b>Langkah 5</b><br />Review Pengajuan</a>
        </li>
    </ul>
    <div class="clear"></div>
</div>

<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(('pendaftaran_kapal/akta/data_kapal/').($_smarty_tpl->getVariable('result')->value['registrasi_id']));?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" />Langkah Sebelumnya</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pendaftaran_kapal/akta/data_pemilik_process');?>
" method="post">
    <input type="hidden" name="registrasi_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['registrasi_id'])===null||$tmp==='' ? '' : $tmp);?>
">
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            DATA PEMILIK
        </a>
    </h5>
    <table class="table-form" width="100%">
        <tr>
            <td width='10%'>
                <span style="text-decoration: underline;">Nama Pemilik</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td width='52%'>
                <input type="text" name="nama_pemilik" size="30" maxlength="50" value="<?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('result')->value['nama_pemilik'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['nama_pemilik'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['nama_pemilik'])===null||$tmp==='' ? '' : $tmp)));?>
" style="font-weight: bold;" />
            </td>
			<td width='10%'>
                <span style="text-decoration: underline;">NPWP</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td width='28%'>
                <input type="text" name="npwp" size="25" maxlength="30" value="<?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('result')->value['npwp'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['npwp'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['npwp'])===null||$tmp==='' ? '' : $tmp)));?>
" style="font-weight: bold;" />
            </td>
        </tr>
        <tr>
            <td >
                <span style="text-decoration: underline;">Alamat Pemilik</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td >
                <input type="text" name="alamat_pemilik" size="60" maxlength="100" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['alamat_pemilik'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
			<td >
                <span style="text-decoration: underline;">Kota</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td >
                <input type="text" name="kota" size="25" maxlength="25" value="<?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('result')->value['kota'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['kota'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['kota'])===null||$tmp==='' ? '' : $tmp)));?>
" style="font-weight: bold;" />
            </td>
        </tr>
	</table>
</div>	
<div>
    <table class="table-form" width='100%'>
        <tr class="submit-box">
            <td></td>
            <td align='right'>
                <input type="submit" name="save" value="Simpan dan Lanjutkan" class="submit-button" />
            </td>
        </tr>
    </table>
</div>	
</form>