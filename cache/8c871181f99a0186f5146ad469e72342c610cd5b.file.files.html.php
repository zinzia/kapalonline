<?php /* Smarty version Smarty-3.0.7, created on 2015-12-09 23:47:34
         compiled from "application/views\daftar_pending/akta_pendaftaran/files.html" */ ?>
<?php /*%%SmartyHeaderCode:1583056685b26eb9c34-87203263%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8c871181f99a0186f5146ad469e72342c610cd5b' => 
    array (
      0 => 'application/views\\daftar_pending/akta_pendaftaran/files.html',
      1 => 1449675682,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1583056685b26eb9c34-87203263',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Pengajuan Akta Pendaftaran</a><span></span>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('member/pending_daftar/');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" />Back to Draft List</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div class="step-box">
    <ul>
        <li>
            <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(('daftar_revisi/akta_pendaftaran/data_pengajuan/').($_smarty_tpl->getVariable('result')->value['registrasi_id']));?>
" class="normal"><b>Langkah 1</b><br />Data Pengajuan</a>
        </li>
        <li>
            <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(('daftar_revisi/akta_pendaftaran/data_kapal/').($_smarty_tpl->getVariable('result')->value['registrasi_id']));?>
" class="normal"><b>Langkah 2</b><br />Data Kapal</a>
        </li>
        <li>
            <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(('daftar_revisi/akta_pendaftaran/data_pemilik/').($_smarty_tpl->getVariable('result')->value['registrasi_id']));?>
" class="normal"><b>Langkah 3</b><br />Data Pemilik</a>
        </li>
        <li>
            <a href="#" class="active"><b>Langkah 4</b><br />Upload File</a>
        </li>
        <li>
            <a href="#" class="normal"><b>Langkah 5</b><br />Review Pengajuan</a>
        </li>
    </ul>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(('daftar_revisi/akta_pendaftaran/data_pemilik/').($_smarty_tpl->getVariable('result')->value['registrasi_id']));?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" />Previous Step</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('daftar_revisi/akta_pendaftaran/files_process');?>
" method="post" enctype="multipart/form-data">
    <input type="hidden" name="registrasi_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['registrasi_id'])===null||$tmp==='' ? '' : $tmp);?>
">
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            FILE ATTACHMENT:
        </a>
    </h5>
    <table class="table-form" width="100%">
        <tr>
            <td colspan="5">
                File persyaratan yang wajib di sertakan adalah sebagai berikut :
            </td>
        </tr>
        <?php $_smarty_tpl->tpl_vars['no'] = new Smarty_variable(1, null, null);?>
        <?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_files')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value){
?>
        <tr>
            <td width='5%' align="center">
                <?php echo $_smarty_tpl->getVariable('no')->value++;?>
.
            </td>
            <td width='30%'>
                <?php echo $_smarty_tpl->tpl_vars['data']->value['ref_name'];?>

            </td>
            <td width='20%'>
                <input type="file" name="<?php echo $_smarty_tpl->tpl_vars['data']->value['ref_field'];?>
" />
            </td>
            <td width='15%'>
                <small style="color: #999;">Maksimal <?php echo $_smarty_tpl->tpl_vars['data']->value['ref_size'];?>
KB (<?php echo ((mb_detect_encoding($_smarty_tpl->tpl_vars['data']->value['ref_allowed'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->tpl_vars['data']->value['ref_allowed'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->tpl_vars['data']->value['ref_allowed']));?>
)</small>
            </td>
            <td width="30%">
                <?php if (in_array($_smarty_tpl->tpl_vars['data']->value['ref_id'],$_smarty_tpl->getVariable('file_uploaded')->value)){?>
                <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(((((('daftar_revisi/akta_pendaftaran/files_download/').($_smarty_tpl->getVariable('result')->value['registrasi_id'])).('/')).($_smarty_tpl->tpl_vars['data']->value['ref_id'])).('/')).($_smarty_tpl->getVariable('name_uploaded')->value[$_smarty_tpl->tpl_vars['data']->value['ref_id']]));?>
" style="font-size: 9px;"><?php echo $_smarty_tpl->getVariable('name_uploaded')->value[$_smarty_tpl->tpl_vars['data']->value['ref_id']];?>
</a>
                <?php }else{ ?>
				<?php if ($_smarty_tpl->tpl_vars['data']->value['ref_id']!='3'){?>
                <?php if ($_smarty_tpl->tpl_vars['data']->value['ref_required']=='1'){?>
                <span style="color: red">Belum ada file yang di upload!</span>
                <?php }else{ ?>
                <span style="color: blue;">* Optional atau sesuai arahan dari regulator</span>
                <?php }?>
				<?php }else{ ?>
				<?php if ($_smarty_tpl->getVariable('result')->value['pemilik_st']=='0'){?>
				<span style="color: red">Belum ada file yang di upload!</span>
                <?php }else{ ?>
				<span style="color: blue;">* Optional atau sesuai arahan dari regulator</span>
                <?php }?>
				<?php }?>
				<?php }?>
            </td>
        </tr>
        <?php }} ?>
        <tr>
            <td colspan="5"></td>
        </tr>
        <tr>
            <td colspan="5" align="center">
                <input type="submit" name="save" value="Upload Berkas" class='button-upload' />
            </td>
        </tr>
    </table>
</div>	
</form>
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('daftar_revisi/akta_pendaftaran/files_next');?>
" method="post">
    <input type="hidden" name="registrasi_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['registrasi_id'])===null||$tmp==='' ? '' : $tmp);?>
">
    <input type="hidden" name="pemilik_st" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['pemilik_st'])===null||$tmp==='' ? '' : $tmp);?>
">
<div>	
    <table class="table-form" width='100%'>
        <tr class="submit-box">
            <td>
                Klik Upload Berkas untuk mengupload semua file
            </td>
            <td align='right'>
                <input type="submit" name="save" value="Lanjutkan" class="submit-button" />
            </td>
        </tr>
    </table>
</div>	
</form>