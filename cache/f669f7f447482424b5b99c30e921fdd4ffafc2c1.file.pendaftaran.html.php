<?php /* Smarty version Smarty-3.0.7, created on 2015-12-09 09:35:36
         compiled from "application/views\member/monitoring_daftar/pendaftaran.html" */ ?>
<?php /*%%SmartyHeaderCode:457956679378079256-42859626%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f669f7f447482424b5b99c30e921fdd4ffafc2c1' => 
    array (
      0 => 'application/views\\member/monitoring_daftar/pendaftaran.html',
      1 => 1449628531,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '457956679378079256-42859626',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Monitoring Permohonan</a><span></span>
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('member/monitoring_daftar/');?>
">Pengajuan Akta</a><span></span>
        <small><?php echo $_smarty_tpl->getVariable('detail')->value['group_nm'];?>
 [<?php echo $_smarty_tpl->getVariable('detail')->value['registrasi_id'];?>
]</small>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('member/monitoring_daftar/');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" />Back to Waiting List</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<table class="table-form" width="100%">
    <tr>
        <td width='50%' colspan="2">
            Dikirim oleh <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['pengirim'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['pengirim'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['pengirim']));?>
, <br />Tanggal : <?php echo $_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_pengajuan']);?>

        </td>
        <td width='50%' align='right' colspan="2">
            <img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/icon.waiting.png" alt="" />  
			Masih dalam proses <?php echo $_smarty_tpl->getVariable('detail')->value['task_nm'];?>

			<br />
            <small style="font-size: 11px; font-family: helvetica; color: #999; font-style: italic; margin-right: 5px;">Update Terakhir : <?php echo $_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tanggal_proses']);?>
</small>
        </td>
    </tr>
</table>
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            PERMOHONAN <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['group_nm'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['group_nm'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['group_nm']));?>

        </a>
    </h5>
    <table class="table-view" width="100%">
		<tr>
            <td width="15%">
                Tanggal Surat Permohonan
            </td width="26">
            <td><b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_surat']))===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_surat']))===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_surat']))===null||$tmp==='' ? '' : $tmp)));?>
</b></td>
            <td width='15%'>
                Nomor Surat Permohonan
            </td>
            <td width='35%'><b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['no_surat'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['no_surat'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['no_surat'])===null||$tmp==='' ? '' : $tmp)));?>
</b></td>
        </tr>
        <tr>
            <td>
                Nama Pemohon
            </td>
            <td><b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemohon'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemohon'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemohon'])===null||$tmp==='' ? '' : $tmp)));?>
</b></td>
            <td>
                Pemohon Merupakan
            </td>
            <td>
			<?php if ($_smarty_tpl->getVariable('detail')->value['pemilik_st']==1){?>
				<b>Pemilik Kapal</b>
			<?php }else{ ?>
				<b>Bukan Pemilik Kapal</b>
			<?php }?>
			</td>
        </tr>
        <tr>
            <td>
                Tempat Pendaftaran
            </td>
            <td><b><?php echo $_smarty_tpl->getVariable('detail')->value['pendaftaran_nm'];?>
 - <?php echo $_smarty_tpl->getVariable('detail')->value['pendaftaran_kd'];?>
</b></td>
            
			<td>
                Bukti Hak Milik Atas Kapal
            </td>
            <td><b><?php echo $_smarty_tpl->getVariable('detail')->value['subgroup_nm'];?>
</b></td>
        </tr>
    </table>
</div>	
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            DATA PEMILIK
        </a>
    </h5>
	<table class="table-view" width="100%">
        <tr>
            <td width='15%'>
                Nama Pemilik
            </td>
            <td width='35%'>
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemilik'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemilik'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemilik'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
			<td width='15%'>
                NPWP
            </td>
            <td width='35%'>
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['npwp'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['npwp'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['npwp'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
        </tr>
        <tr>
            <td >
                Alamat Pemilik
            </td>
            <td >
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['alamat_pemilik'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
			<td >
                Kota
            </td>
            <td >
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['kota'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['kota'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['kota'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
        </tr>
	</table>
</div>	
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            DATA KAPAL
        </a>
    </h5>
	<table class="table-view" width="100%">
        <tr>
            <td width='15%'>
                Nama Kapal
            </td>
            <td width='35%'>
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_kapal'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_kapal'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_kapal'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
			<td width='15%'>
                Eks Nama Kapal<i></i>
            </td>
            <td width='35%'>
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['eks_nama_kapal'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['eks_nama_kapal'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['eks_nama_kapal'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                Jenis Kapal
            </td>
            <td>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['jenis_ket'];?>
 - <?php echo $_smarty_tpl->getVariable('detail')->value['jenisdetail_ket'];?>
</b>				
			</td>
            <td>
                Nomor IMO<i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['nomor_imo'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                Call Sign<i></i>
            </td>
            <td>
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['call_sign'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['call_sign'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['call_sign'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
            <td>
                No. CSR<i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['no_csr'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                Harga Kapal<i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['harga_kapal'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td>
                Bendera Asal<i></i>
            </td>
            <td>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['negara_nm'];?>
</b>
			</td>
        </tr>
	</table>	
</div>	
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            SURAT UKUR & DATA PEMBANGUNAN KAPAL
        </a>
    </h5>
	<table class="table-view" width="100%">
        <tr>
            <td width='15%'>
                Tempat Terbit
            </td>
            <td width='35%'>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['penerbitan_nm'];?>
 - <?php echo $_smarty_tpl->getVariable('detail')->value['penerbitan_kd'];?>
</b>
			</td>
            <td width='15%'>
                Tempat Pembuatan<i></i>
            </td>
            <td width='35%'>
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['tempat_pembuatan'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['tempat_pembuatan'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['tempat_pembuatan'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
            
        </tr>
		<tr>
            <td>
                Tanggal Surat Ukur
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['tgl_surat_ukur'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td>
                Tanggal Peletakan Lunas<i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['tgl_peletakan_lunas'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
		<tr>
            <td>
                No. Surat Ukur
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['no_surat_ukur'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
	</table>
</div>	
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            DIMENSI
        </a>
    </h5>
	<table class="table-view" width="100%">
        <tr>
            <td width='15%'>
                Panjang
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['dimensi_panjang'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td width='15%'>
                Dalam<i></i>
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['dimensi_dalam'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
		</tr>
        <tr>
            <td width='15%'>
                Lebar
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['dimensi_lebar'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td width='15%'>
                LOA
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['dimensi_loa'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
		</tr>
        <tr>
            <td width='15%'>
                (GT) Isi Kotor
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['dimensi_isikotor'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td width='15%'>
                (NT) Isi Bersih
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['dimensi_isibersih'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
		</tr>
        <tr>
            <td width='15%'>
                Tanda Selar
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['tanda_selar'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
		</tr>
	</table>
</div>
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            KONSTRUKSI DAN MESIN
        </a>
    </h5>
	<table class="table-view" width="100%">
        <tr>
            <td width='15%'>
                Bahan Utama Kapal
            </td>
            <td width='35%'>
                <b><?php echo $_smarty_tpl->getVariable('detail')->value['bahan_ket'];?>
</b>
            </td>
            <td width='15%'>
                Jumlah Geladak
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['jml_geladak'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                Jumlah Cerobong<i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['jml_cerobong'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td>
                Jumlah Baling Baling<i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['jml_baling'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                Penggerak Utama<i></i>
            </td>
            <td><b><?php echo $_smarty_tpl->getVariable('detail')->value['penggerak_ket'];?>
</b>
            </td>
            <td>
                Mesin Merk<i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['mesin_merk'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                Putaran Mesin<i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['putaran_mesin'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td>
                Type Mesin<i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['type_mesin'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                Serial No. Mesin<i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['serial_no_mesin'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
 	</table>
</div>	
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            DAYA MESIN
        </a>
    </h5>
	<table class="table-view" width="100%">
        <tr>
            <td width='15%'>
                Daya Mesin 1<i></i>
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['daya_mesin1'])===null||$tmp==='' ? '' : $tmp);?>
<b>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['satuan_mesin1'];?>
<b>
            </td>
            <td width='15%'>
                Daya Mesin 2<i></i>
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['daya_mesin2'])===null||$tmp==='' ? '' : $tmp);?>
<b>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['satuan_mesin2'];?>
<b>
            </td>
        </tr>
        <tr>
            <td>
                Daya Mesin 3<i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['daya_mesin3'])===null||$tmp==='' ? '' : $tmp);?>
<b>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['satuan_mesin3'];?>
<b>
            </td>
			<td>
                Daya Mesin 4<i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['daya_mesin4'])===null||$tmp==='' ? '' : $tmp);?>
<b>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['satuan_mesin4'];?>
<b>
            </td>
        </tr>
    </table>
</div>	
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            FILE ATTACHMENT:
        </a>
    </h5>
    <table class="table-view" width="100%">
        <?php $_smarty_tpl->tpl_vars['no'] = new Smarty_variable(1, null, null);?>
        <?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_files')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value){
?>
        <tr>
            <td width='5%' align="center">
                <?php echo $_smarty_tpl->getVariable('no')->value++;?>
.
            </td>
            <td width='35%'>
                <?php echo $_smarty_tpl->tpl_vars['data']->value['ref_name'];?>

            </td>
            <td width="60%">
                <?php if (in_array($_smarty_tpl->tpl_vars['data']->value['ref_id'],$_smarty_tpl->getVariable('file_uploaded')->value)){?>
                <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(((((('member/monitoring_daftar/files_download/').($_smarty_tpl->getVariable('detail')->value['registrasi_id'])).('/')).($_smarty_tpl->tpl_vars['data']->value['ref_id'])).('/')).($_smarty_tpl->getVariable('name_uploaded')->value[$_smarty_tpl->tpl_vars['data']->value['ref_id']]));?>
"><?php echo $_smarty_tpl->getVariable('name_uploaded')->value[$_smarty_tpl->tpl_vars['data']->value['ref_id']];?>
</a>
                <?php }else{ ?>
                -
                <?php }?>
            </td>
        </tr>
        <?php }} ?>
	</table>
</div>