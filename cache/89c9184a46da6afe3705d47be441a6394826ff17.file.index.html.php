<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 17:34:15
         compiled from "application/views\report/payment/index.html" */ ?>
<?php /*%%SmartyHeaderCode:32310566560a7a5b276-45086476%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '89c9184a46da6afe3705d47be441a6394826ff17' => 
    array (
      0 => 'application/views\\report/payment/index.html',
      1 => 1441883444,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '32310566560a7a5b276-45086476',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!-- javascript -->
<script type="text/javascript">
    function toggle(source) {
        var checkboxes = document.getElementsByName("data_id[]");
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = source.checked;
        }
        ;
    }
</script>
<div class="breadcrum">
    <p>
        <a href="#">Penagihan dan Pembayaran</a><span></span>
        <a href="#">Flight Approval</a><span></span>
        <small>Waiting List Pembayaran Permohonan FA</small>
    </p>
    <div class="clear"></div>
</div>
<div class="content-dashboard">
    <h4>
        <a class="group down" href="#">
            Rekapitulasi Penagihan dan Pembayaran Flight Approval
        </a>
    </h4>
    <div class="clear"></div>
    <div class="group-box down" style="">
        <div class="map-box">
            <p>
                <a href="#">
                    <img alt="" src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/payment.success.png">
                     Pembayaran Berhasil<br>
                     <small>Domestik : <b><?php echo (($tmp = @$_smarty_tpl->getVariable('rs_rekap_bayar')->value['success_dom'])===null||$tmp==='' ? 0 : $tmp);?>
</b> pembayaran</small><br/>
                     <small>Internasional : <b><?php echo (($tmp = @$_smarty_tpl->getVariable('rs_rekap_bayar')->value['success_int'])===null||$tmp==='' ? 0 : $tmp);?>
</b> pembayaran</small>
                </a>
            </p>
        </div>
        <div class="map-box">
            <p>
                <a href="#">
                    <img alt="" src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/payment.pending.png">
                     Pembayaran Pending<br>
                     <small>Domestik : <b><?php echo (($tmp = @$_smarty_tpl->getVariable('rs_rekap_bayar')->value['pending_dom'])===null||$tmp==='' ? 0 : $tmp);?>
</b> pembayaran</small><br/>
                     <small>Internasional : <b><?php echo (($tmp = @$_smarty_tpl->getVariable('rs_rekap_bayar')->value['pending_int'])===null||$tmp==='' ? 0 : $tmp);?>
</b> pembayaran</small>
                </a>
            </p>
        </div>
        <div class="map-box">
            <p>
                <a href="#">
                    <img alt="" src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/payment.warning.png">
                    Kelebihan Bayar<br>
                    <small>Domestik : <b><?php echo (($tmp = @$_smarty_tpl->getVariable('rs_rekap_bayar')->value['lebih_bayar_dom'])===null||$tmp==='' ? 0 : $tmp);?>
</b> pembayaran</small><br/>
                    <small>Internasional : <b><?php echo (($tmp = @$_smarty_tpl->getVariable('rs_rekap_bayar')->value['lebih_bayar_int'])===null||$tmp==='' ? 0 : $tmp);?>
</b> pembayaran</small>
                </a>
            </p>
        </div>
        <div class="map-box">
            <p>
                <a href="#">
                    <img alt="" src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/payment.failed.png">
                    Kekurangan Bayar<br>
                    <small>Domestik : <b><?php echo (($tmp = @$_smarty_tpl->getVariable('rs_rekap_bayar')->value['kurang_bayar_dom'])===null||$tmp==='' ? 0 : $tmp);?>
</b> pembayaran</small><br/>
                     <small>Internasional : <b><?php echo (($tmp = @$_smarty_tpl->getVariable('rs_rekap_bayar')->value['kurang_bayar_int'])===null||$tmp==='' ? 0 : $tmp);?>
</b> pembayaran</small>
                </a>
            </p>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div class="sub-nav-content">
    <ul>
        <li><a class="active" href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('report/payment');?>
">Waiting List Pembayaran Permohonan FA</a></li>
        <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('report/payment/history');?>
">History Pembayaran Permohonan FA</a></li>
    </ul>
</div>
<div class="clear"></div>
<div class="sub-content">
    <!-- notification template -->
    <?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <!-- end of notification template-->
    <div class="search-box">
        <h3><a href="#">Search</a></h3>
        <form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('report/payment/proses_cari');?>
" method="post">
            <table class="table-search" width="100%" border="0">
                <tr>
                    <th width="5%">Nomor</th>
                    <td width="33%">
                        <input name="published_no" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('search')->value['published_no'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="30" />
                        <select name="data_flight">
                            <option value="domestik" <?php if ((($tmp = @$_smarty_tpl->getVariable('search')->value['data_flight'])===null||$tmp==='' ? '' : $tmp)=='domestik'){?>selected="selected"<?php }?>>Domestik</option>
                            <option value="internasional" <?php if ((($tmp = @$_smarty_tpl->getVariable('search')->value['data_flight'])===null||$tmp==='' ? '' : $tmp)=='internasional'){?>selected="selected"<?php }?>>Internasional</option>
                        </select>
                    </td>
                    <td align="right">
                        <input name="save" type="submit" value="Tampilkan" />
                        <input name="save" type="submit" value="Reset" />
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="navigation">
        <div class="pageRow">
            <div class="pageNav">
                <ul>
                    <li class="info">Total&nbsp;<strong><?php echo (($tmp = @$_smarty_tpl->getVariable('total')->value)===null||$tmp==='' ? 0 : $tmp);?>
</strong>&nbsp;Record&nbsp;</li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
        <div class="navigation">
            <div class="navigation-button">
                <ul>
                    <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('report/payment/download_payment/');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/download-icon.png" alt="" /> Download Excel</a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('report/payment/form_payment');?>
" method="post">
        <table class="table-view" width="100%">
            <tr>
                <th width='4%'>No</th>
                <th width='20%'>Nomor FA</th>
                <th width='20%'>Airline</th>
                <th width='15%'>Berjadwal / <br />Tidak Berjadwal</th>
                <th width='15%'>Domestik / <br />Internasional</th>
                <th width='10%'>Jumlah Tagihan</th>
                <th width='16%'>Batas Bayar</th>
            </tr>
            <?php $_smarty_tpl->tpl_vars['no'] = new Smarty_variable(1, null, null);?>
            <?php  $_smarty_tpl->tpl_vars['result'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_id')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['result']->key => $_smarty_tpl->tpl_vars['result']->value){
?>
            <tr <?php if (($_smarty_tpl->getVariable('no')->value%2)!=1){?>class="blink-row"<?php }?>>
                <td align="center"><?php echo $_smarty_tpl->getVariable('no')->value++;?>
.</td>
                <td><?php echo $_smarty_tpl->tpl_vars['result']->value['published_no'];?>
</td>
                <td><?php echo ((mb_detect_encoding($_smarty_tpl->tpl_vars['result']->value['airlines_nm'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->tpl_vars['result']->value['airlines_nm'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->tpl_vars['result']->value['airlines_nm']));?>
</td>
                <td><?php echo ((mb_detect_encoding($_smarty_tpl->tpl_vars['result']->value['data_type'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->tpl_vars['result']->value['data_type'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->tpl_vars['result']->value['data_type']));?>
</td>
                <td align="center"><?php echo ((mb_detect_encoding($_smarty_tpl->tpl_vars['result']->value['data_flight'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->tpl_vars['result']->value['data_flight'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->tpl_vars['result']->value['data_flight']));?>
</td>
                <td align="right"><?php echo number_format((($tmp = @$_smarty_tpl->getVariable('tarif')->value[$_smarty_tpl->tpl_vars['result']->value['data_flight']])===null||$tmp==='' ? 0 : $tmp),0,",",".");?>
</td>
                <td align="center"><?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->tpl_vars['result']->value['payment_due_date']), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->tpl_vars['result']->value['payment_due_date']),SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->tpl_vars['result']->value['payment_due_date'])));?>
</td>
            </tr>
            <?php }} else { ?>
            <tr>
                <td colspan="7">Data not found!</td>
            </tr>
            <?php } ?>
        </table>
    </form>
</div>

