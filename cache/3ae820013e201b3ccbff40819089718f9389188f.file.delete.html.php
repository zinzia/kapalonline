<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 06:23:26
         compiled from "application/views\pengaturan/jenisdetail_kapal/delete.html" */ ?>
<?php /*%%SmartyHeaderCode:30481566517ce3f1d32-12629043%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3ae820013e201b3ccbff40819089718f9389188f' => 
    array (
      0 => 'application/views\\pengaturan/jenisdetail_kapal/delete.html',
      1 => 1449462209,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '30481566517ce3f1d32-12629043',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/jenisdetail_kapal');?>
">Jenis Detail Kapal</a><span></span>
        <small>Delete Data</small>
    </p>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/jenisdetail_kapal/delete_process');?>
" method="post" onsubmit="return confirm('Apakah anda yakin akan menghapus data berikut ini?')">
    <input name="jenisdetail_id" type="hidden" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['jenisdetail_id'])===null||$tmp==='' ? '' : $tmp);?>
" />
    <table class="table-input" width="100%">
        <tr class="headrow">
            <th colspan="4">Hapus Jenis Detail Kapal</th>
        </tr>
        <tr>
            <th colspan="4">Apakah anda yakin akan menghapus data dibawah ini?</th>
        </tr>
        <tr>
            <td width="15%">Jenis Detail</td>
            <td width="35%"><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['jenisdetail_ket'])===null||$tmp==='' ? '' : $tmp);?>
</td>
        </tr>  
        <tr class="submit-box">
            <td colspan="4">
                <input type="submit" name="save" value="Delete" class="reset-button" />
            </td>
        </tr>
    </table>
</form>