<?php /* Smarty version Smarty-3.0.7, created on 2015-12-08 00:30:45
         compiled from "application/views\member/monitoring_daftar/index.html" */ ?>
<?php /*%%SmartyHeaderCode:15825665c245b7c476-22348703%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '426fd6b228c9554cc51c0ef6a23a16e1f05b36c9' => 
    array (
      0 => 'application/views\\member/monitoring_daftar/index.html',
      1 => 1449509423,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15825665c245b7c476-22348703',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Monitoring Pengajuan</a><span></span>
        <small>Pengajuan Akta</small>
    </p>
    <div class="clear"></div>
</div>
						
<div class="search-box">
    <h3><a href="#">Search</a></h3>
    <form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('member/monitoring_daftar/proses_cari');?>
" method="post">
        <table class="table-search" width="100%" border="0">
            <tr>
                <th width="15%">No. Registrasi</th>
                <td width="15%">
                    <input name="registrasi_id" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('search')->value['registrasi_id'])===null||$tmp==='' ? '' : $tmp);?>
" size="20" maxlength="50" placeholder="-- semua --" style="text-align: center;" />
                </td>
                <th width="15%">Jenis Pengajuan Akta</th>
                <td width="55%">
                    <select name="daftar_group">
                        <option value="">-- semua --</option>
						<?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_group')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value){
?>
						<?php if ((isset($_smarty_tpl->getVariable('search',null,true,false)->value['daftar_group']))){?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['group_id'];?>
" <?php if (($_smarty_tpl->tpl_vars['data']->value['group_id'])==$_smarty_tpl->getVariable('search')->value['daftar_group']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['group_nm'];?>
</option>
						<?php }else{ ?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['group_id'];?>
" ><?php echo $_smarty_tpl->tpl_vars['data']->value['group_nm'];?>
</option>
						<?php }?>
                        <?php }} ?>
                    </select>  
                </td>
			</tr>
			<tr>
				<th width="10%">Nama Kapal</th>
                <td width="10%">
                    <input name="nama_kapal" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('search')->value['nama_kapal'])===null||$tmp==='' ? '' : $tmp);?>
" size="20" maxlength="50" placeholder="-- semua --" style="text-align: center;" />
                </td>
                <th>&nbsp;</th>
                <td align='right'>
                    <input name="save" type="submit" value="Tampilkan" />
                    <input name="save" type="submit" value="Reset" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div class="navigation">
    <div class="pageRow">
        <div class="pageNav">
            <ul>
                <li class="info">Total ( <b style="color: red;"><?php echo (($tmp = @$_smarty_tpl->getVariable('total')->value)===null||$tmp==='' ? 0 : $tmp);?>
</b> ) Pengajuan Yang Sedang Diproses!</li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>
<table class="table-view" width="100%">
    <tr>
        <th width='5%'>No</th>
        <th width='15%'>Jenis Pengajuan</th>
        <th width='15%'>Registrasi</th>
        <th width='25%'>Nama Kapal</th>
        <th width='25%'>Pemohon</th>
        <th width='15%'></th>
    </tr>
    <?php $_smarty_tpl->tpl_vars['no'] = new Smarty_variable(1, null, null);?>
    <?php  $_smarty_tpl->tpl_vars['result'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_id')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['result']->key => $_smarty_tpl->tpl_vars['result']->value){
?>
    <tr <?php if (($_smarty_tpl->getVariable('no')->value%2)!=1){?>class="blink-row"<?php }?>>
        <td align="center"><?php echo $_smarty_tpl->getVariable('no')->value++;?>
.</td>
        <td align="center"><?php echo ((mb_detect_encoding($_smarty_tpl->tpl_vars['result']->value['group_nm'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->tpl_vars['result']->value['group_nm'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->tpl_vars['result']->value['group_nm']));?>
</td>
        <td align="center"><b style="color: #999;"><?php echo ((mb_detect_encoding($_smarty_tpl->tpl_vars['result']->value['registrasi_id'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->tpl_vars['result']->value['registrasi_id'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->tpl_vars['result']->value['registrasi_id']));?>
</b></td>
        <td align="center"><b style="color: #999;"><?php echo ((mb_detect_encoding($_smarty_tpl->tpl_vars['result']->value['nama_kapal'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->tpl_vars['result']->value['nama_kapal'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->tpl_vars['result']->value['nama_kapal']));?>
</b></td>
        <td align="center">
            <span style="text-decoration: underline;"><?php echo ((mb_detect_encoding($_smarty_tpl->tpl_vars['result']->value['nama_pemohon'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->tpl_vars['result']->value['nama_pemohon'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->tpl_vars['result']->value['nama_pemohon']));?>

        </td>
        <td align="center">
            <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url((($tmp = @((('member/monitoring_daftar/').($_smarty_tpl->tpl_vars['result']->value['group_alias'])).('/')).($_smarty_tpl->tpl_vars['result']->value['registrasi_id']))===null||$tmp==='' ? '' : $tmp));?>
" class="button">
                <?php if ($_smarty_tpl->tpl_vars['result']->value['selisih_hari']==0&&substr($_smarty_tpl->tpl_vars['result']->value['selisih_waktu'],0,2)==00){?>
                <?php echo substr($_smarty_tpl->tpl_vars['result']->value['selisih_waktu'],3,2);?>
 Menit yang lalu
                <?php }elseif($_smarty_tpl->tpl_vars['result']->value['selisih_hari']==0){?>
                <?php echo substr($_smarty_tpl->tpl_vars['result']->value['selisih_waktu'],0,2);?>
 Jam yang lalu
                <?php }else{ ?>
                <?php echo $_smarty_tpl->tpl_vars['result']->value['selisih_hari'];?>
 Hari yang lalu
                <?php }?>
            </a>
        </td>
    </tr>
    <?php }} else { ?>
    <tr>
        <td colspan="7">Belum ada Permohonan Akta Yang Diajukan!</td>
    </tr>
    <?php } ?>
</table>