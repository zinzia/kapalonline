<?php /* Smarty version Smarty-3.0.7, created on 2015-12-11 13:34:40
         compiled from "application/views\task/daftar_kasi_reschedule/pendaftaran.html" */ ?>
<?php /*%%SmartyHeaderCode:17944566a6e802e5d53-24138836%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3e2d2970330f52abc3ea2d9803a2f565aec6b43b' => 
    array (
      0 => 'application/views\\task/daftar_kasi_reschedule/pendaftaran.html',
      1 => 1449815675,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17944566a6e802e5d53-24138836',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!-- include javascript -->
<?php $_template = new Smarty_Internal_Template("task/daftar_kasi_reschedule/task_javascript.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of include javascript-->
<script type="text/javascript">
    $(document).ready(function () {
        // select all
        $("#proses-all").change(function () {
            var proses = $("#proses-all").val();
            $(".proses-item").val(proses);
        });
		
		// date picker
        $(".tanggal").datepicker({
            showOn: 'both',
            changeMonth: true,
            changeYear: true,
            buttonImage: '<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
/resource/doc/images/icon/calendar.gif',
            buttonImageOnly: true,
            dateFormat: 'yy-mm-dd',
			minDate: 0,
        });
	
	});
	    
</script>
<style type="text/css">
    .select2-choice {
        width: 270px !important;
    }
    .select2-default {
        width: 270px !important;
    }
</style>
<div class="breadcrum">
    <p>
        <a href="#">Task Manager Pengajuan Akta Pendaftaran</a><span></span>
        <a href="#"><?php echo $_smarty_tpl->getVariable('task')->value['task_nm'];?>
</a><span></span>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="pageRow">
        <div class="pageNav">
            <ul>
                <li class="info">
                    <b><?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['group_nm'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['group_nm'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['group_nm']));?>
</b>
                    <br /><br />
                    Nama Kapal : <?php echo (($tmp = @((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['nama_kapal'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal'])))===null||$tmp==='' ? '' : $tmp);?>

                    <br />
                    Registrasi : <?php echo (($tmp = @((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['registrasi_id'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['registrasi_id'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['registrasi_id'])))===null||$tmp==='' ? '' : $tmp);?>

                    <br />
                    Pemohon : <?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemohon'])===null||$tmp==='' ? '-' : $tmp);?>
, <?php echo (($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_pengajuan']))===null||$tmp==='' ? '-' : $tmp);?>

                </li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('task/akta_pendaftaran');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back to Waiting List</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            PENENTUAN  PENANDATANGANAN MINUTE AKTA & DAFTAR INDUK
        </a>
    </h5>
    <form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(((('task/daftar_kasi_reschedule/send_process/').($_smarty_tpl->getVariable('detail')->value['registrasi_id'])).('/')).($_smarty_tpl->getVariable('detail')->value['process_id']));?>
" method="post" >
        <input type="hidden" name="registrasi_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['registrasi_id'])===null||$tmp==='' ? '' : $tmp);?>
">
        <table class="table-form" width="100%">
			<tr>
				<td colspan="3">
					Mohon pilih tanggal penjadwalan ulang:
				</td>
			</tr>
		<?php if ($_smarty_tpl->getVariable('rs_reschedule')->value['tanggal1']!=null){?>
			<tr>
				<td width="13%" color="blue">
					<span><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('rs_reschedule')->value['tanggal1']))===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('rs_reschedule')->value['tanggal1']))===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('rs_reschedule')->value['tanggal1']))===null||$tmp==='' ? '' : $tmp)));?>
</span><br /><i></i>
				</td>
				<td>
					<input type="checkbox" name="tanggal1" size="10" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('rs_reschedule')->value['tanggal1'])===null||$tmp==='' ? '' : $tmp);?>
">
				</td>
			</tr>
		<?php }?>	
		<?php if ($_smarty_tpl->getVariable('rs_reschedule')->value['tanggal2']!=null){?>
			<tr>
				<td width="13%" color="blue">
					<span><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('rs_reschedule')->value['tanggal2']))===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('rs_reschedule')->value['tanggal2']))===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('rs_reschedule')->value['tanggal2']))===null||$tmp==='' ? '' : $tmp)));?>
</span><br /><i></i>
				</td>
				<td>
					<input type="checkbox" name="tanggal2" size="10" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('rs_reschedule')->value['tanggal2'])===null||$tmp==='' ? '' : $tmp);?>
">
				</td>
			</tr>
		<?php }?>	
		<?php if ($_smarty_tpl->getVariable('rs_reschedule')->value['tanggal3']!=null){?>
			<tr>
				<td width="13%" color="blue">
					<span><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('rs_reschedule')->value['tanggal3']))===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('rs_reschedule')->value['tanggal3']))===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('rs_reschedule')->value['tanggal3']))===null||$tmp==='' ? '' : $tmp)));?>
</span><br /><i></i>
				</td>
				<td>
					<input type="checkbox" name="tanggal3" size="10" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('rs_reschedule')->value['tanggal3'])===null||$tmp==='' ? '' : $tmp);?>
">
				</td>
			</tr>
		<?php }?>
			<tr>
				<td>
				</td>	
				<td align="right"><input value="Konfirmasi Tanggal Ke <?php echo $_smarty_tpl->getVariable('next')->value['role_nm'];?>
" class="submit-button" type="submit" onclick="return confirm('Apakah anda yakin akan mengirimkan data berikut ini ke <?php echo $_smarty_tpl->getVariable('next')->value['role_nm'];?>
?')">
				</td>
			</tr>	
		</table>
    </form>
</div>
<!-- include html -->
<?php $_template = new Smarty_Internal_Template("task/daftar_kasi_reschedule/task_form_dialog.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of include html -->