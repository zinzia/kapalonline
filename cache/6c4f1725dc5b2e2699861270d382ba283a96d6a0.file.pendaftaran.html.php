<?php /* Smarty version Smarty-3.0.7, created on 2015-12-10 00:06:32
         compiled from "application/views\task/daftar_kasubdit_tglttd/pendaftaran.html" */ ?>
<?php /*%%SmartyHeaderCode:2852456685f9853a0a2-54664873%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6c4f1725dc5b2e2699861270d382ba283a96d6a0' => 
    array (
      0 => 'application/views\\task/daftar_kasubdit_tglttd/pendaftaran.html',
      1 => 1449680788,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2852456685f9853a0a2-54664873',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!-- include javascript -->
<?php $_template = new Smarty_Internal_Template("task/daftar_kasubdit_tglttd/task_javascript.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of include javascript-->
<script type="text/javascript">
    $(document).ready(function () {
        // select all
        $("#proses-all").change(function () {
            var proses = $("#proses-all").val();
            $(".proses-item").val(proses);
        });
    });
</script>
<div class="breadcrum">
    <p>
        <a href="#">Task Manager Pengajuan Akta Pendaftaran</a><span></span>
        <a href="#"><?php echo $_smarty_tpl->getVariable('task')->value['task_nm'];?>
</a><span></span>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="pageRow">
        <div class="pageNav">
            <ul>
                <li class="info">
                    <b><?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['group_nm'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['group_nm'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['group_nm']));?>
</b>
                    <br />
                    Nama Kapal : <?php echo (($tmp = @((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['nama_kapal'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal'])))===null||$tmp==='' ? '' : $tmp);?>

                    <br />
                    Registrasi : <?php echo (($tmp = @((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['registrasi_id'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['registrasi_id'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['registrasi_id'])))===null||$tmp==='' ? '' : $tmp);?>

                    <br />
                    Pemohon : <?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemohon'])===null||$tmp==='' ? '-' : $tmp);?>
, <?php echo (($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_pengajuan']))===null||$tmp==='' ? '-' : $tmp);?>

                </li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('task/akta_pendaftaran');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back to Waiting List</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="navigation" style="background-color: #eee; padding: 10px;">
    <div class="navigation-button">
        <ul>
            <?php if ($_smarty_tpl->getVariable('action')->value['action_revisi']=='1'){?>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(((('task/daftar_kasubdit_tglttd/pending_process/').($_smarty_tpl->getVariable('detail')->value['registrasi_id'])).('/')).($_smarty_tpl->getVariable('detail')->value['process_id']));?>
" onclick="return confirm('Apakah anda yakin akan mengirimkan data berikut ke <?php echo $_smarty_tpl->getVariable('prev')->value['role_nm'];?>
?')"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/icon.notes.red.png" alt="" /> Koreksi Penentuan Tanggal ke <?php echo $_smarty_tpl->getVariable('prev')->value['role_nm'];?>
</a></li>
            <?php }?>
            <?php if ($_smarty_tpl->getVariable('action')->value['action_send']=='1'){?>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(((('task/daftar_kasubdit_tglttd/send_process/').($_smarty_tpl->getVariable('detail')->value['registrasi_id'])).('/')).($_smarty_tpl->getVariable('detail')->value['process_id']));?>
" onclick="return confirm('Apakah anda yakin akan mengirimkan data berikut ini ke <?php echo $_smarty_tpl->getVariable('detail')->value['nama_pemohon'];?>
?')"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/arrow-right.png" alt="" />Approve: Kirim ke <?php echo $_smarty_tpl->getVariable('detail')->value['nama_pemohon'];?>
</a></li>
            <?php }?>  
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            KONFIRMASI PENENTUAN TANGGAL PENANDATANGANAN MINUTE AKTA & DAFTAR INDUK
        </a>
    </h5>
    <form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('task/daftar_kasubdit_tglttd/approved_process');?>
" method="post">
        <input type="hidden" name="registrasi_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['registrasi_id'])===null||$tmp==='' ? '' : $tmp);?>
">
        <table class="table-view" width="100%">
			<td width="20%">
			    Tanggal Penandatanganan
			</td>
			<td><b>
			     <?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_ttd_akta']))===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_ttd_akta']))===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_ttd_akta']))===null||$tmp==='' ? '' : $tmp)));?>

			</td></b>
		</table>
    </form>
</div>
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            MINUTE AKTA & DAFTAR INDUK
        </a>
    </h5>
    <form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('task/daftar_kasubdit_tglttd/approved_process');?>
" method="post">
        <input type="hidden" name="registrasi_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['registrasi_id'])===null||$tmp==='' ? '' : $tmp);?>
">
        <table class="table-view" width="100%">
		</table>
    </form>
</div>

<!-- include html -->
<?php $_template = new Smarty_Internal_Template("task/daftar_kasubdit_approve/task_form_dialog.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of include html -->