<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 21:00:36
         compiled from "application/views\task/telaah_regulator/index.html" */ ?>
<?php /*%%SmartyHeaderCode:1161456659104a192d7-13586167%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '54ebf50b0b1d303a8aee4b02a6634bcbbeadaaa4' => 
    array (
      0 => 'application/views\\task/telaah_regulator/index.html',
      1 => 1444880368,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1161456659104a192d7-13586167',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Task Manager Izin Rute</a><span></span>
        <small>Upload Draft Telaah Regulator</small>
    </p>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('task/telaah_regulator/upload_process/');?>
" method="post" enctype="multipart/form-data">
    <table class="table-input" width="100%">
        <tr>
            <th colspan="2">Dokumen Draft Telaah Regulator</th>
        </tr>
        <tr>
            <td width="15%">Nama Draft Telaah</td>
            <td width="60%"><a href="<?php ob_start();?><?php echo 'task/telaah_regulator/download';?>
<?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->getVariable('config')->value->site_url($_tmp1);?>
">draft_telaah.docx</a></td>
        </tr>
        <tr>
            <td width="15%">Upload Draft Telaah</td>
            <td width="60%"><input type="file" name="draft_telaah" /></td>
        </tr>
        <tr>
            <td width="15%"></td>
            <td width="60%"><input type="submit" name="save" value="Upload" class="submit-button" /></td>
        </tr>
    </table>
</form>
