<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 15:24:19
         compiled from "application/views\pengaturan/block/add.html" */ ?>
<?php /*%%SmartyHeaderCode:199285665423338b9f5-94474020%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7f256a10c585f1573d3d1acd7a513d5408ba2c48' => 
    array (
      0 => 'application/views\\pengaturan/block/add.html',
      1 => 1441883434,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '199285665423338b9f5-94474020',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript">
    $(document).ready(function() {
        // --
        $("#airlines").select2({
            placeholder: "-- Operator Penerbangan --",
            width: 'resolve',
            allowClear: true
        });
    });
</script>
<style type="text/css">
    .select2-choice {
        width: 407px !important;
    }
</style>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/block');?>
">Block Airlines</a><span></span>
        <small>Add Data</small>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/block');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/block/add_process');?>
" method="post">
    <table class="table-input" width="100%">
        <tr class="headrow">
            <th colspan="4">Lock Registration for Airlines</th>
        </tr>
        <tr>
            <td width="20%">Alasan</td>
            <td width="80%">
                <textarea name="block_reason" cols="100" rows="5"><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['block_reason'])===null||$tmp==='' ? '' : $tmp);?>
</textarea>
                <br />
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr>
            <td>Operator / Airlines</td>
            <td colspan="3">
                <select name="airlines_id" id="airlines">
                    <?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_airlines')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value){
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['airlines_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['airlines_id']==$_smarty_tpl->getVariable('result')->value['airlines_id']){?>selected="selected"<?php }?>><?php echo ((mb_detect_encoding($_smarty_tpl->tpl_vars['data']->value['airlines_nm'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->tpl_vars['data']->value['airlines_nm'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->tpl_vars['data']->value['airlines_nm']));?>
 (<?php echo ((mb_detect_encoding($_smarty_tpl->tpl_vars['data']->value['airlines_iata_cd'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->tpl_vars['data']->value['airlines_iata_cd'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->tpl_vars['data']->value['airlines_iata_cd']));?>
)</option>
                    <?php }} ?>
                </select>
                <em>* wajib diisi</em>
            </td>
        </tr> 
        <tr class="submit-box">
            <td colspan="2">
                <input type="submit" name="save" value="Simpan" class="submit-button" />
                <input type="reset" name="save" value="Reset" class="reset-button" />
            </td>
        </tr>
    </table>
</form>