<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 16:27:10
         compiled from "application/views\pengaturan/file_references/add.html" */ ?>
<?php /*%%SmartyHeaderCode:21582566550eef33c61-70567354%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '09e6c96360302aa7d6bd822029a359e5c89e204b' => 
    array (
      0 => 'application/views\\pengaturan/file_references/add.html',
      1 => 1449480424,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21582566550eef33c61-70567354',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/file_references');?>
">File References</a><span></span>
        <small>Add Data</small>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/file_references');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/file_references/add_process');?>
" method="post">
    <table class="table-input" width="100%" border='0'>
        <tr class="headrow">
            <th colspan="4">Tambah Data File References</th>
        </tr>
        <tr>
            <td width="15%">File</td>
            <td width="35%">
                <input name="ref_field" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['ref_field'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="50" />
                <em>* wajib diisi</em>
            </td>
        </tr>  
        <tr>
            <td>Name</td>
            <td>
                <input name="ref_name" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['ref_name'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="50" />
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr>
            <td>Size</td>
            <td>
                <input name="ref_size" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['ref_size'])===null||$tmp==='' ? '2048' : $tmp);?>
" size="30" maxlength="50" />
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr>
            <td>Allowed</td>
            <td>
                <input name="ref_allowed" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['ref_allowed'])===null||$tmp==='' ? 'pdf|jpeg|jpg|doc|docx|xlsx' : $tmp);?>
" size="30" maxlength="50" />
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr>
            <td>Required</td>
            <td>
                <select name="ref_required" class="ref_required">
                    <option value="">--- Pilih ---</option>
                    <option value="1" <?php ob_start();?><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['ref_required'])===null||$tmp==='' ? '' : $tmp);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1=="1"){?>selected="selected"<?php }?>>Required</option>
                    <option value="0" <?php ob_start();?><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['ref_required'])===null||$tmp==='' ? '' : $tmp);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2=="0"){?>selected="selected"<?php }?>>Not required</option>
                </select>
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr class="submit-box">
            <td colspan="4">
                <input type="submit" name="save" value="Simpan" class="submit-button" />
                <input type="reset" name="save" value="Reset" class="reset-button" />
            </td>
        </tr>
    </table>
</form>