<?php /* Smarty version Smarty-3.0.7, created on 2015-12-11 00:10:20
         compiled from "application/views\member/daftar_member_tglttd/pendaftaran.html" */ ?>
<?php /*%%SmartyHeaderCode:168215669b1fc5bd082-83831083%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1eb655d57946a4564a3ce5e1802289776bd61947' => 
    array (
      0 => 'application/views\\member/daftar_member_tglttd/pendaftaran.html',
      1 => 1449767417,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '168215669b1fc5bd082-83831083',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!-- include javascript -->
<?php $_template = new Smarty_Internal_Template("member/daftar_member_tglttd/task_javascript.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of include javascript-->
<script type="text/javascript">
    $(document).ready(function () {
        // select all
        $("#proses-all").change(function () {
            var proses = $("#proses-all").val();
            $(".proses-item").val(proses);
        });
    });
</script>
<div class="breadcrum">
    <p>
        <a href="#">Task Manager Pengajuan Akta Pendaftaran</a><span></span>
        <a href="#"><?php echo $_smarty_tpl->getVariable('task')->value['task_nm'];?>
</a><span></span>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="pageRow">
        <div class="pageNav">
            <ul>
                <li class="info">
                    <b><?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['group_nm'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['group_nm'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['group_nm']));?>
</b>
                    <br />
                    Nama Kapal : <?php echo (($tmp = @((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['nama_kapal'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal'])))===null||$tmp==='' ? '' : $tmp);?>

                    <br />
                    Registrasi : <?php echo (($tmp = @((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['registrasi_id'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['registrasi_id'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['registrasi_id'])))===null||$tmp==='' ? '' : $tmp);?>

                    <br />
                    Pemohon : <?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemohon'])===null||$tmp==='' ? '-' : $tmp);?>
, <?php echo (($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_pengajuan']))===null||$tmp==='' ? '-' : $tmp);?>

                </li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('member/jadwal_ttd');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back to Waiting List</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="navigation" style="background-color: #eee; padding: 10px;">
    <div class="navigation-button">
        <ul>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            KONFIRMASI PENENTUAN TANGGAL PENANDATANGANAN MINUTE AKTA & DAFTAR INDUK
        </a>
    </h5>
        <table class="table-view" width="100%">
			<td width="20%">
			    Tanggal Penandatanganan
			</td>
			<td><b>
			     <?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_ttd_akta']))===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_ttd_akta']))===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_ttd_akta']))===null||$tmp==='' ? '' : $tmp)));?>

			</td></b>
		</table>
</div>
<div>
    <form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(((('member/daftar_member_tglttd/send_process/').($_smarty_tpl->getVariable('detail')->value['registrasi_id'])).('/')).($_smarty_tpl->getVariable('detail')->value['process_id']));?>
" method="post">
        <input type="hidden" name="registrasi_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['registrasi_id'])===null||$tmp==='' ? '' : $tmp);?>
">
		<table class="table-form" width='100%'>
			<tr class="submit-box">
				<td width="95%" align='right'>
					<input type="submit" name="save" value="Jadwal Ulang" class="reset-button" />
				</td>
				<td align='right'>
					<input type="submit" name="save" value="Lanjutkan" class="submit-button" onclick="return confirm('Apakah anda yakin telah menyetujui jadwal?');" />
				</td>
			</tr>
		</table>
    </form>
</div>	

<!-- include html -->
<?php $_template = new Smarty_Internal_Template("member/daftar_member_tglttd/task_form_dialog.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of include html -->