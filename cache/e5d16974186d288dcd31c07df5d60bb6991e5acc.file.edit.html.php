<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 07:37:08
         compiled from "application/views\pengaturan/penggerak_kapal/edit.html" */ ?>
<?php /*%%SmartyHeaderCode:2392856652914eee941-60470104%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e5d16974186d288dcd31c07df5d60bb6991e5acc' => 
    array (
      0 => 'application/views\\pengaturan/penggerak_kapal/edit.html',
      1 => 1449469903,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2392856652914eee941-60470104',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/penggerak_kapal');?>
">Penggerak Kapal</a><span></span>
        <small>Edit Data</small>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/penggerak_kapal');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/penggerak_kapal/edit_process');?>
" method="post">
    <input name="penggerak_id" type="hidden" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['penggerak_id'])===null||$tmp==='' ? '' : $tmp);?>
" />
    <table class="table-input" width="100%" border='0'>
        <tr class="headrow">
            <th colspan="4">Edit Data Penggerak Kapal</th>
        </tr>
        <tr>
            <td width="15%">Nama Penggerak</td>
            <td width="35%">
                <input name="penggerak_ket" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['penggerak_ket'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="50" />
                <em>* wajib diisi</em>
            </td>
        </tr>  
        <tr>
            <td>Nama Penggerak (ENG)</td>
            <td>
                <input name="penggerak_ket_eng" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['penggerak_ket_eng'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="50" />
            </td>
        </tr>
        <tr class="submit-box">
            <td colspan="4">
                <input type="submit" name="save" value="Simpan" class="submit-button" />
                <input type="reset" name="save" value="Reset" class="reset-button" />
            </td>
        </tr>
    </table>
</form>