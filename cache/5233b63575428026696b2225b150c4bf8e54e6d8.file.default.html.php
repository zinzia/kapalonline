<?php /* Smarty version Smarty-3.0.7, created on 2015-12-09 18:48:12
         compiled from "application/views\dashboard/welcome/default.html" */ ?>
<?php /*%%SmartyHeaderCode:26288566814fcde82b4-80724213%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5233b63575428026696b2225b150c4bf8e54e6d8' => 
    array (
      0 => 'application/views\\dashboard/welcome/default.html',
      1 => 1449661691,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '26288566814fcde82b4-80724213',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="dashboard-welcome">
    <h3><?php echo $_smarty_tpl->getVariable('tanggal')->value['hari'];?>
, <?php echo $_smarty_tpl->getVariable('tanggal')->value['tanggal'];?>
</h3>
    <div class="dashboard-profile">
        <div style="float: left;">
            <div class="dashboard-profile-sidebar">
                <h4>Selamat Datang</h4>
                
            </div>
        </div>
        <div class="dashboard-profile-content">
            <img src="<?php echo $_smarty_tpl->getVariable('com_user')->value['operator_photo'];?>
" alt="" class="user-img" />
            <ul>
                <li><b><?php echo $_smarty_tpl->getVariable('com_user')->value['operator_name'];?>
, <br />(<?php echo $_smarty_tpl->getVariable('com_user')->value['role_nm'];?>
)</b><br /><?php echo $_smarty_tpl->getVariable('com_user')->value['sub_direktorat'];?>
</li>
                <li><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/phone.icon.png" alt="" /><?php echo $_smarty_tpl->getVariable('com_user')->value['operator_phone'];?>
</li>
                <li><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/address.icon.png" alt="" /><?php echo $_smarty_tpl->getVariable('com_user')->value['operator_address'];?>
</li>
                <li><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/email.icon.png" alt="" /><?php echo $_smarty_tpl->getVariable('com_user')->value['user_mail'];?>
</li>
                <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('dashboard/account_settings/data_pribadi');?>
">Update my profile</a></li>
            </ul>
            <div class="clear"></div>
        </div>
        <div class="dashboard-profile-content">
            <br />
            <h5>Statistik Pelayanan <?php echo $_smarty_tpl->getVariable('tanggal')->value['tahun'];?>
</h5>
            <div id="chart">
                <script type="text/javascript">
                    var chart = new FusionCharts("<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/charts/StackedColumn2D.swf", "chart", "100%", "285", "0", "1");
                    chart.setDataURL("<?php echo $_smarty_tpl->getVariable('config')->value->site_url('dashboard/welcome/data_chart');?>
");
                    chart.render("chart");
                </script> 
            </div>

            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
