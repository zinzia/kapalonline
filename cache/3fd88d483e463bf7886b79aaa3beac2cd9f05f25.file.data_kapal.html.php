<?php /* Smarty version Smarty-3.0.7, created on 2015-12-08 22:20:01
         compiled from "application/views\pendaftaran_kapal/akta/data_kapal.html" */ ?>
<?php /*%%SmartyHeaderCode:95425666f5213b46e3-43581692%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3fd88d483e463bf7886b79aaa3beac2cd9f05f25' => 
    array (
      0 => 'application/views\\pendaftaran_kapal/akta/data_kapal.html',
      1 => 1449587831,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '95425666f5213b46e3-43581692',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript">
    $(document).ready(function () {
        // date picker
        $(".tanggal").datepicker({
            showOn: 'both',
            changeMonth: true,
            changeYear: true,
            buttonImage: '<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
/resource/doc/images/icon/calendar.gif',
            buttonImageOnly: true,
            dateFormat: 'yy-mm-dd',
        });
        
        /*
         * COMBO BOX
         */
        $(".jenisdetailkapal").select2({
            placeholder: "Pilih Jenis Kapal",
            allowClear: true,
            width: 270
        });
        $(".benderaasal").select2({
            placeholder: "Pilih Bendera Asal",
            allowClear: true,
            width: 270
        });
        $(".tempatterbit").select2({
            placeholder: "Pilih Tempat Terbit",
            allowClear: true,
            width: 270
        });
        $(".bahankapal").select2({
            placeholder: "Pilih Bahan Kapal",
            allowClear: true,
            width: 270
        });
        $(".penggerakkapal").select2({
            placeholder: "Pilih Penggerak Utama",
            allowClear: true,
            width: 270
        });
    });
</script>
<style type="text/css">
    .select2-choice {
        width: 270px !important;
    }
    .select2-default {
        width: 270px !important;
    }
</style>
<div class="breadcrum">
    <p>
     <a href="#">Pengajuan Akta Pendaftaran</a><span></span>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('member/akta_pendaftaran/');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" />Back to Draft List</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div class="step-box">
    <ul>
        <li>
            <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(('pendaftaran_kapal/akta/data_pengajuan/').($_smarty_tpl->getVariable('result')->value['registrasi_id']));?>
" class="normal"><b>Langkah 1</b><br />Data Pengajuan</a>
        </li>
        <li>
            <a href="#" class="active"><b>Langkah 2</b><br />Data Kapal</a>
        </li>
        <li>
            <a href="#" class="normal"><b>Langkah 3</b><br />Data Pemilik</a>
        </li>
        <li>
            <a href="#" class="normal"><b>Langkah 4</b><br />Upload File</a>
        </li>
        <li>
            <a href="#" class="normal"><b>Langkah 5</b><br />Review Pengajuan</a>
        </li>
    </ul>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(('pendaftaran_kapal/akta/data_pengajuan/').($_smarty_tpl->getVariable('result')->value['registrasi_id']));?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" />Langkah Sebelumnya</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pendaftaran_kapal/akta/data_kapal_process');?>
" method="post">
    <input type="hidden" name="registrasi_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['registrasi_id'])===null||$tmp==='' ? '' : $tmp);?>
">
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            DATA KAPAL
        </a>
    </h5>
    <table class="table-form" width="100%">
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Nama Kapal</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td width='35%'>
                <input type="text" name="nama_kapal" size="30" maxlength="50" value="<?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('result')->value['nama_kapal'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['nama_kapal'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['nama_kapal'])===null||$tmp==='' ? '' : $tmp)));?>
" style="font-weight: bold;" />
            </td>
			<td width='15%'>
                <span style="text-decoration: underline;">Eks Nama Kapal</span><br /><i></i>
            </td>
            <td width='35%'>
                <input type="text" name="eks_nama_kapal" size="30" maxlength="50" value="<?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('result')->value['eks_nama_kapal'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['eks_nama_kapal'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['eks_nama_kapal'])===null||$tmp==='' ? '' : $tmp)));?>
" style="font-weight: bold;" />
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Jenis Kapal</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td>
				<select name="jenisdetail_kapal" class="jenisdetailkapal">
					<option value=""></option>
					<?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_jeniskapal')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value){
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['jenisdetail_id'];?>
" <?php if (($_smarty_tpl->tpl_vars['data']->value['jenisdetail_id'])==$_smarty_tpl->getVariable('result')->value['jenisdetail_kapal']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['jenis_ket'];?>
 - <?php echo $_smarty_tpl->tpl_vars['data']->value['jenisdetail_ket'];?>
</option>
					<?php }} ?>
				</select>
			</td>
            <td>
                <span style="text-decoration: underline;">Nomor IMO</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="nomor_imo" size="20" maxlength="30" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['nomor_imo'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Call Sign</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="call_sign" size="10" maxlength="20" value="<?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('result')->value['call_sign'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['call_sign'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['call_sign'])===null||$tmp==='' ? '' : $tmp)));?>
" style="font-weight: bold;" />
            </td>
            <td>
                <span style="text-decoration: underline;">No. CSR</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="no_csr" size="15" maxlength="20" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['no_csr'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Harga Kapal</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="harga_kapal" size="20" maxlength="30" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['harga_kapal'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
            <td>
                <span style="text-decoration: underline;">Bendera Asal</span><br /><i></i>
            </td>
            <td>
				<select name="bendera_asal" class="benderaasal">
					<option value=""></option>
					<?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_benderaasal')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value){
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['negara_id'];?>
" <?php if (($_smarty_tpl->tpl_vars['data']->value['negara_id'])==$_smarty_tpl->getVariable('result')->value['bendera_asal']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['negara_nm'];?>
 </option>
					<?php }} ?>
				</select>
			</td>
        </tr>
	</table>	
</div>	
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            SURAT UKUR & DATA PEMBANGUNAN KAPAL
        </a>
    </h5>
	<table class="table-form" width="100%">
    	<tr>
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Tempat Terbit</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td width='35%'>
				<select name="tempat_terbit" class="tempatterbit">
					<option value=""></option>
					<?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_tempatterbit')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value){
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['pelabuhan_id'];?>
" <?php if (($_smarty_tpl->tpl_vars['data']->value['pelabuhan_id'])==$_smarty_tpl->getVariable('result')->value['tempat_terbit']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['pelabuhan_nm'];?>
 - <?php echo $_smarty_tpl->tpl_vars['data']->value['pelabuhan_kd'];?>
 </option>
					<?php }} ?>
				</select>
			</td>
            <td width='15%'>
                <span style="text-decoration: underline;">Tempat Pembuatan</span><br /><i></i>
            </td>
            <td width='35%'>
                <input type="text" name="tempat_pembuatan" size="25" maxlength="50" value="<?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('result')->value['tempat_pembuatan'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['tempat_pembuatan'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('result')->value['tempat_pembuatan'])===null||$tmp==='' ? '' : $tmp)));?>
" style="font-weight: bold;" />
            </td>
            
        </tr>
		<tr>
            <td>
                <span style="text-decoration: underline;">Tanggal Surat Ukur</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td>
                <input type="text" name="tgl_surat_ukur" size="10" maxlength="10" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['tgl_surat_ukur'])===null||$tmp==='' ? '' : $tmp);?>
" class="tanggal" readonly="readonly" style="text-align: center; font-weight: bold;" />
            </td>
            <td>
                <span style="text-decoration: underline;">Tanggal Peletakan Lunas</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="tgl_peletakan_lunas" size="10" maxlength="10" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['tgl_peletakan_lunas'])===null||$tmp==='' ? '' : $tmp);?>
" class="tanggal" readonly="readonly" style="text-align: center; font-weight: bold;" />
            </td>
        </tr>
		<tr>
            <td>
                <span style="text-decoration: underline;">No. Surat Ukur</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td>
                <input type="text" name="no_surat_ukur" size="25" maxlength="50" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['no_surat_ukur'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
        </tr>
	</table>
</div>	
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            DIMENSI
        </a>
    </h5>
	<table class="table-form" width="100%">
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Panjang</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td width='35%'>
                <input type="text" name="dimensi_panjang" size="10" maxlength="10" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['dimensi_panjang'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
            <td width='15%'>
                <span style="text-decoration: underline;">Dalam</span><br /><i></i>
            </td>
            <td width='35%'>
                <input type="text" name="dimensi_dalam" size="10" maxlength="10" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['dimensi_dalam'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
		</tr>
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Lebar</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td width='35%'>
                <input type="text" name="dimensi_lebar" size="10" maxlength="10" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['dimensi_lebar'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
            <td width='15%'>
                <span style="text-decoration: underline;">LOA</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td width='35%'>
                <input type="text" name="dimensi_loa" size="10" maxlength="10" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['dimensi_loa'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
		</tr>
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">(GT) Isi Kotor</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td width='35%'>
                <input type="text" name="dimensi_isikotor" size="10" maxlength="10" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['dimensi_isikotor'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
            <td width='15%'>
                <span style="text-decoration: underline;">(NT) Isi Bersih</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td width='35%'>
                <input type="text" name="dimensi_isibersih" size="10" maxlength="10" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['dimensi_isibersih'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
		</tr>
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Tanda Selar</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td width='35%'>
                <input type="text" name="tanda_selar" size="25" maxlength="30" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['tanda_selar'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
		</tr>
	</table>
</div>
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            KONSTRUKSI DAN MESIN
        </a>
    </h5>
	<table class="table-form" width="100%">
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Bahan Utama Kapal</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td width='35%'>
                <select name="bahan_kapal" class="bahankapal">
					<option value=""></option>
					<?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_bahankapal')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value){
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['bahan_id'];?>
" <?php if (($_smarty_tpl->tpl_vars['data']->value['bahan_id'])==$_smarty_tpl->getVariable('result')->value['bahan_kapal']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['bahan_ket'];?>
 </option>
					<?php }} ?>
				</select>
            </td>
            <td width='15%'>
                <span style="text-decoration: underline;">Jumlah Geladak</span><br /><i></i><em>* wajib diisi</em>
            </td>
            <td width='35%'>
                <input type="text" name="jml_geladak" size="10" maxlength="20" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['jml_geladak'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Jumlah Cerobong</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="jml_cerobong" size="10" maxlength="20" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['jml_cerobong'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
            <td>
                <span style="text-decoration: underline;">Jumlah Baling Baling</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="jml_baling" size="10" maxlength="20" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['jml_baling'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Penggerak Utama</span><br /><i></i>
            </td>
            <td>
                <select name="penggerak_kapal" class="penggerakkapal">
					<option value=""></option>
					<?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_penggerakkapal')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value){
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['penggerak_id'];?>
" <?php if (($_smarty_tpl->tpl_vars['data']->value['penggerak_id'])==$_smarty_tpl->getVariable('result')->value['penggerak_kapal']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['penggerak_ket'];?>
 </option>
					<?php }} ?>
				</select>
            </td>
            <td>
                <span style="text-decoration: underline;">Mesin Merk</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="mesin_merk" size="25" maxlength="30" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['mesin_merk'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Putaran Mesin</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="putaran_mesin" size="20" maxlength="20" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['putaran_mesin'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
            <td>
                <span style="text-decoration: underline;">Type Mesin</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="type_mesin" size="25" maxlength="30" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['type_mesin'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Serial No. Mesin</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="serial_no_mesin" size="25" maxlength="30" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['serial_no_mesin'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
            </td>
        </tr>
	</table>
</div>	
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            DAYA MESIN
        </a>
    </h5>
	<table class="table-form" width="100%">
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Daya Mesin 1</span><br /><i></i>
            </td>
            <td width='35%'>
                <input type="text" name="daya_mesin1" size="20" maxlength="20" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['daya_mesin1'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
				<select name="satuan_mesin1">
					<option value="HP" <?php if (($_smarty_tpl->getVariable('result')->value['satuan_mesin1'])=='HP'){?>selected="selected"<?php }?> >HP</option>
					<option value="KW" <?php if (($_smarty_tpl->getVariable('result')->value['satuan_mesin1'])=='KW'){?>selected="selected"<?php }?> >KW</option>
				</select>
            </td>
            <td width='15%'>
                <span style="text-decoration: underline;">Daya Mesin 2</span><br /><i></i>
            </td>
            <td width='35%'>
                <input type="text" name="daya_mesin2" size="20" maxlength="20" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['daya_mesin2'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
				<select name="satuan_mesin2">
					<option value="HP" <?php if (($_smarty_tpl->getVariable('result')->value['satuan_mesin2'])=='HP'){?>selected="selected"<?php }?> >HP</option>
					<option value="KW" <?php if (($_smarty_tpl->getVariable('result')->value['satuan_mesin2'])=='KW'){?>selected="selected"<?php }?> >KW</option>
				</select>
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Daya Mesin 3</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="daya_mesin3" size="20" maxlength="20" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['daya_mesin3'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
				<select name="satuan_mesin3">
					<option value="HP" <?php if (($_smarty_tpl->getVariable('result')->value['satuan_mesin3'])=='HP'){?>selected="selected"<?php }?> >HP</option>
					<option value="KW" <?php if (($_smarty_tpl->getVariable('result')->value['satuan_mesin3'])=='KW'){?>selected="selected"<?php }?> >KW</option>
				</select>
            </td>
			<td>
                <span style="text-decoration: underline;">Daya Mesin 4</span><br /><i></i>
            </td>
            <td>
                <input type="text" name="daya_mesin4" size="20" maxlength="20" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['daya_mesin4'])===null||$tmp==='' ? '' : $tmp);?>
" style="font-weight: bold;" />
				<select name="satuan_mesin4">
					<option value="HP" <?php if (($_smarty_tpl->getVariable('result')->value['satuan_mesin4'])=='HP'){?>selected="selected"<?php }?> >HP</option>
					<option value="KW" <?php if (($_smarty_tpl->getVariable('result')->value['satuan_mesin4'])=='KW'){?>selected="selected"<?php }?> >KW</option>
				</select>
            </td>
        </tr>
    </table>
</div>	
    <table class="table-form" width='100%'>
        <tr class="submit-box">
            <td></td>
            <td align='right'>
                <input type="submit" name="save" value="Lanjutkan" class="submit-button" />
            </td>
        </tr>
    </table>
</form>