<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 06:56:19
         compiled from "application/views\pengaturan/kategori_kapal/edit.html" */ ?>
<?php /*%%SmartyHeaderCode:1588556651f83d218b6-40995354%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ad50444c2d9ae3b20d5372f38d46eb44ff40d613' => 
    array (
      0 => 'application/views\\pengaturan/kategori_kapal/edit.html',
      1 => 1449467773,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1588556651f83d218b6-40995354',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/jenis_kapal');?>
">Kategori Kapal</a><span></span>
        <small>Edit Data</small>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/kategori_kapal');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/kategori_kapal/edit_process');?>
" method="post">
    <input name="kategori_id" type="hidden" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['kategori_id'])===null||$tmp==='' ? '' : $tmp);?>
" />
    <table class="table-input" width="100%" border='0'>
        <tr class="headrow">
            <th colspan="4">Edit Data Kategori Kapal</th>
        </tr>
        <tr>
            <td width="15%">Nama Kategori</td>
            <td width="35%">
                <input name="kategori_nm" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['kategori_nm'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="50" />
                <em>* wajib diisi</em>
            </td>
        </tr>  
        <tr>
            <td>Kode</td>
            <td>
                <input name="kategori_kd" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['kategori_kd'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="50" />
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr class="submit-box">
            <td colspan="4">
                <input type="submit" name="save" value="Simpan" class="submit-button" />
                <input type="reset" name="save" value="Reset" class="reset-button" />
            </td>
        </tr>
    </table>
</form>