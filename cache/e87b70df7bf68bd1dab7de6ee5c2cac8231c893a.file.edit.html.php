<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 08:11:32
         compiled from "application/views\pengaturan/jenis_kapal/edit.html" */ ?>
<?php /*%%SmartyHeaderCode:333656653124c29542-71037082%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e87b70df7bf68bd1dab7de6ee5c2cac8231c893a' => 
    array (
      0 => 'application/views\\pengaturan/jenis_kapal/edit.html',
      1 => 1449472268,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '333656653124c29542-71037082',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/jenis_kapal');?>
">Jenis Kapal</a><span></span>
        <small>Edit Data</small>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/jenis_kapal');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/jenis_kapal/edit_process');?>
" method="post">
    <input name="jenis_id" type="hidden" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['jenis_id'])===null||$tmp==='' ? '' : $tmp);?>
" />
    <table class="table-input" width="100%" border='0'>
        <tr class="headrow">
            <th colspan="4">Edit Data Jenis Kapal</th>
        </tr>
        <tr>
            <td width="15%">Jenis</td>
            <td width="35%">
                <input name="jenis_ket" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['jenis_ket'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="50" />
                <em>* wajib diisi</em>
            </td>
        </tr>  
        <tr>
            <td>Alias</td>
            <td>
                <input name="jenis_alias" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['jenis_alias'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="50" />
            </td>
        </tr>
        <tr>
            <td>Status</td>
            <td>
                <select name="jenis_status" class="jenis_status">
                    <option value="">--- Pilih ---</option>
                    <option value="1" <?php ob_start();?><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['jenis_status'])===null||$tmp==='' ? '' : $tmp);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1=="1"){?>selected="selected"<?php }?>>AKTIF</option>
                    <option value="0" <?php ob_start();?><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['jenis_status'])===null||$tmp==='' ? '' : $tmp);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2=="0"){?>selected="selected"<?php }?>>TIDAK AKTIF</option>
                </select>
            </td>
        </tr>
        <tr class="submit-box">
            <td colspan="4">
                <input type="submit" name="save" value="Simpan" class="submit-button" />
                <input type="reset" name="save" value="Reset" class="reset-button" />
            </td>
        </tr>
    </table>
</form>