<?php /* Smarty version Smarty-3.0.7, created on 2015-12-09 18:07:51
         compiled from "application/views\task/daftar_staff_syarat/pendaftaran.html" */ ?>
<?php /*%%SmartyHeaderCode:220656680b87352389-12205167%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '733ec6e59f6b23101ed7ee9d33d4eef3e0a56943' => 
    array (
      0 => 'application/views\\task/daftar_staff_syarat/pendaftaran.html',
      1 => 1449648646,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '220656680b87352389-12205167',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!-- include javascript -->
<?php $_template = new Smarty_Internal_Template("task/daftar_staff_syarat/task_javascript.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of include javascript-->
<script type="text/javascript">
    $(document).ready(function () {
        // select all
        $("#proses-all").change(function () {
            var proses = $("#proses-all").val();
            $(".proses-item").val(proses);
        });
    });
</script>
<div class="breadcrum">
    <p>
        <a href="#">Task Manager Pengajuan Akta Pendaftaran</a><span></span>
        <a href="#"><?php echo $_smarty_tpl->getVariable('task')->value['task_nm'];?>
</a><span></span>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="pageRow">
        <div class="pageNav">
            <ul>
                <li class="info">
                    <b><?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['group_nm'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['group_nm'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['group_nm']));?>
</b>
                    <br />
                    Nama Kapal : <?php echo (($tmp = @((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['nama_kapal'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal'])))===null||$tmp==='' ? '' : $tmp);?>

                    <br />
                    Registrasi : <?php echo (($tmp = @((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['registrasi_id'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['registrasi_id'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['registrasi_id'])))===null||$tmp==='' ? '' : $tmp);?>

                    <br />
                    Pemohon : <?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemohon'])===null||$tmp==='' ? '-' : $tmp);?>
, <?php echo (($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_pengajuan']))===null||$tmp==='' ? '-' : $tmp);?>

                </li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('task/akta_pendaftaran');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back to Waiting List</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="navigation" style="background-color: #eee; padding: 10px;">
    <div class="navigation-button">
        <ul>
            <?php if ($_smarty_tpl->getVariable('action')->value['action_revisi']=='1'){?>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(((('task/daftar_staff_syarat/pending_process/').($_smarty_tpl->getVariable('detail')->value['registrasi_id'])).('/')).($_smarty_tpl->getVariable('detail')->value['process_id']));?>
" onclick="return confirm('Apakah anda yakin akan mengirimkan data berikut ke <?php echo $_smarty_tpl->getVariable('detail')->value['nama_pemohon'];?>
?')"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/icon.notes.red.png" alt="" /> Revisi ke <?php echo $_smarty_tpl->getVariable('detail')->value['nama_pemohon'];?>
</a></li>
            <?php }?>
            <?php if ($_smarty_tpl->getVariable('action')->value['action_reject']=='1'){?>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(((('task/daftar_staff_syarat/reject_process/').($_smarty_tpl->getVariable('detail')->value['registrasi_id'])).('/')).($_smarty_tpl->getVariable('detail')->value['process_id']));?>
" onclick="return confirm('Apakah anda yakin akan menolak semua data pengajuan dibawah ini?')"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/icon.failed.png" alt="" /> Tolak Pengajuan</a></li>
            <?php }?>
            <?php if ($_smarty_tpl->getVariable('action')->value['action_send']=='1'){?>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(((('task/daftar_staff_syarat/send_process/').($_smarty_tpl->getVariable('detail')->value['registrasi_id'])).('/')).($_smarty_tpl->getVariable('detail')->value['process_id']));?>
" onclick="return confirm('Apakah anda yakin akan mengirimkan data berikut ini ke <?php echo $_smarty_tpl->getVariable('next')->value['role_nm'];?>
?')"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/arrow-right.png" alt="" /> Valid dan Kirim ke <?php echo $_smarty_tpl->getVariable('next')->value['role_nm'];?>
</a></li>
            <?php }?>  
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            VERIFIKASI KELENGKAPAN SYARAT (File Attachment) :
        </a>
    </h5>
    <form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('task/daftar_staff_syarat/approved_process');?>
" method="post">
        <input type="hidden" name="registrasi_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['registrasi_id'])===null||$tmp==='' ? '' : $tmp);?>
">
        <table class="table-view" width="100%">
		<?php $_smarty_tpl->tpl_vars['no'] = new Smarty_variable(1, null, null);?>
        <?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_files')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value){
?>
        <tr>
            <td width='5%' align="center">
                <?php echo $_smarty_tpl->getVariable('no')->value++;?>
.
            </td>
            <td width='35%'>
                <?php echo $_smarty_tpl->tpl_vars['data']->value['ref_name'];?>

            </td>
            <td width="60%">
                <?php if (in_array($_smarty_tpl->tpl_vars['data']->value['ref_id'],$_smarty_tpl->getVariable('file_uploaded')->value)){?>
                <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(((((('task/daftar_staff_syarat/files_download/').($_smarty_tpl->getVariable('detail')->value['registrasi_id'])).('/')).($_smarty_tpl->tpl_vars['data']->value['ref_id'])).('/')).($_smarty_tpl->getVariable('name_uploaded')->value[$_smarty_tpl->tpl_vars['data']->value['ref_id']]));?>
"><?php echo $_smarty_tpl->getVariable('name_uploaded')->value[$_smarty_tpl->tpl_vars['data']->value['ref_id']];?>
</a>
                <?php }else{ ?>
                -
                <?php }?>
            </td>
        </tr>
        <?php }} ?>
		
		</table>
    </form>
</div>
<div class="action">
    <div class="action-button">
        <ul>
            <li><a href="#" title="<?php echo $_smarty_tpl->getVariable('detail')->value['registrasi_id'];?>
" id="button-catatan"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/document_edit.png" alt="" />( <?php echo $_smarty_tpl->getVariable('total_catatan')->value;?>
 ) Catatan Proses Permohonan</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div></br>
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            DATA DETAIL PENGAJUAN :
        </a>
    </h5>
	<table class="table-view" width="100%">
		<tr>
            <td width="17%">
                <span style="text-decoration: underline;">Tanggal Surat Permohonan</span><br />
            </td>
            <td width="33%"><b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_surat']))===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_surat']))===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_surat']))===null||$tmp==='' ? '' : $tmp)));?>
</b></td>
            <td width='15%'>
                <span style="text-decoration: underline;">Nomor Surat Permohonan</span><br />
            </td>
            <td width='35%'><b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['no_surat'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['no_surat'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['no_surat'])===null||$tmp==='' ? '' : $tmp)));?>
</b></td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Nama Pemohon</span><br />
            </td>
            <td><b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemohon'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemohon'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemohon'])===null||$tmp==='' ? '' : $tmp)));?>
</b></td>
            <td>
                <span style="text-decoration: underline;">Pemohon Merupakan </span><br />
            </td>
            <td>
			<?php if ($_smarty_tpl->getVariable('detail')->value['pemilik_st']==1){?>
				<b>Pemilik Kapal</b>
			<?php }else{ ?>
				<b>Bukan Pemilik Kapal</b>
			<?php }?>
			</td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Tempat Pendaftaran</span><br />
            </td>
            <td><b><?php echo $_smarty_tpl->getVariable('detail')->value['pendaftaran_nm'];?>
 - <?php echo $_smarty_tpl->getVariable('detail')->value['pendaftaran_kd'];?>
</b></td>
            
			<td>
                <span style="text-decoration: underline;">Bukti Hak Milik Atas Kapal </span>
            </td>
            <td><b><?php echo $_smarty_tpl->getVariable('detail')->value['subgroup_nm'];?>
</b></td>
        </tr>
    </table>
</div>
<div class="rute-box">
	<h5>
        <a href="#" class="drop-up">
            DATA PEMILIK :
        </a>
    </h5>
	<table class="table-view" width="100%">
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Nama Pemilik</span><br />
            </td>
            <td width='35%'>
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemilik'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemilik'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemilik'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
			<td width='15%'>
                <span style="text-decoration: underline;">NPWP</span><br />
            </td>
            <td width='35%'>
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['npwp'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['npwp'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['npwp'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
        </tr>
        <tr>
            <td >
                <span style="text-decoration: underline;">Alamat Pemilik</span><br />
            </td>
            <td >
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['alamat_pemilik'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
			<td >
                <span style="text-decoration: underline;">Kota</span><br />
            </td>
            <td >
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['kota'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['kota'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['kota'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
        </tr>
	</table>
</div>
<div class="rute-box">
	<h5>
        <a href="#" class="drop-up">
            DATA KAPAL :
        </a>
    </h5>
	<table class="table-view" width="100%">
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Nama Kapal</span><br />
            </td>
            <td width='35%'>
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_kapal'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_kapal'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_kapal'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
			<td width='15%'>
                <span style="text-decoration: underline;">Eks Nama Kapal</span><br /><i></i>
            </td>
            <td width='35%'>
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['eks_nama_kapal'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['eks_nama_kapal'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['eks_nama_kapal'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Jenis Kapal</span><br />
            </td>
            <td>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['jenis_ket'];?>
 - <?php echo $_smarty_tpl->getVariable('detail')->value['jenisdetail_ket'];?>
</b>				
			</td>
            <td>
                <span style="text-decoration: underline;">Nomor IMO</span><br /><i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['nomor_imo'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Call Sign</span><br /><i></i>
            </td>
            <td>
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['call_sign'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['call_sign'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['call_sign'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
            <td>
                <span style="text-decoration: underline;">No. CSR</span><br /><i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['no_csr'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Harga Kapal</span><br /><i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['harga_kapal'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td>
                <span style="text-decoration: underline;">Bendera Asal</span><br /><i></i>
            </td>
            <td>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['negara_nm'];?>
</b>
			</td>
        </tr>
	</table>
</div>
<div class="rute-box">
	<h5>
        <a href="#" class="drop-up">
            SURAT UKUR & DATA PEMBANGUNAN KAPAL :
        </a>
    </h5>
	<table class="table-view" width="100%">
    	<tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Tempat Terbit</span><br />
            </td>
            <td width='35%'>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['penerbitan_nm'];?>
 - <?php echo $_smarty_tpl->getVariable('detail')->value['penerbitan_kd'];?>
</b>
			</td>
            <td width='15%'>
                <span style="text-decoration: underline;">Tempat Pembuatan</span><br /><i></i>
            </td>
            <td width='35%'>
                <b><?php echo ((mb_detect_encoding((($tmp = @$_smarty_tpl->getVariable('detail')->value['tempat_pembuatan'])===null||$tmp==='' ? '' : $tmp), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['tempat_pembuatan'])===null||$tmp==='' ? '' : $tmp),SMARTY_RESOURCE_CHAR_SET) : strtoupper((($tmp = @$_smarty_tpl->getVariable('detail')->value['tempat_pembuatan'])===null||$tmp==='' ? '' : $tmp)));?>
</b>
            </td>
            
        </tr>
		<tr>
            <td>
                <span style="text-decoration: underline;">Tanggal Surat Ukur</span><br />
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['tgl_surat_ukur'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td>
                <span style="text-decoration: underline;">Tanggal Peletakan Lunas</span><br /><i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['tgl_peletakan_lunas'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
		<tr>
            <td>
                <span style="text-decoration: underline;">No. Surat Ukur</span><br />
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['no_surat_ukur'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
	</table>
</div>
<div class="rute-box">
	<h5>
        <a href="#" class="drop-up">
            DIMENSI :
        </a>
    </h5>
	<table class="table-view" width="100%">
    	
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Panjang</span><br />
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['dimensi_panjang'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td width='15%'>
                <span style="text-decoration: underline;">Dalam</span><br /><i></i>
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['dimensi_dalam'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
		</tr>
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Lebar</span><br />
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['dimensi_lebar'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td width='15%'>
                <span style="text-decoration: underline;">LOA</span><br />
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['dimensi_loa'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
		</tr>
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">(GT) Isi Kotor</span><br />
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['dimensi_isikotor'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td width='15%'>
                <span style="text-decoration: underline;">(NT) Isi Bersih</span><br />
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['dimensi_isibersih'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
		</tr>
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Tanda Selar</span><br />
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['tanda_selar'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
		</tr>
	</table>
</div>
<div class="rute-box">
	<h5>
        <a href="#" class="drop-up">
            KONSTRUKSI DAN MESIN :
        </a>
    </h5>
	<table class="table-view" width="100%">
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Bahan Utama Kapal</span><br />
            </td>
            <td width='35%'>
                <b><?php echo $_smarty_tpl->getVariable('detail')->value['bahan_ket'];?>
</b>
            </td>
            <td width='15%'>
                <span style="text-decoration: underline;">Jumlah Geladak</span><br />
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['jml_geladak'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Jumlah Cerobong</span><br /><i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['jml_cerobong'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td>
                <span style="text-decoration: underline;">Jumlah Baling Baling</span><br /><i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['jml_baling'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Penggerak Utama</span><br /><i></i>
            </td>
            <td><b><?php echo $_smarty_tpl->getVariable('detail')->value['penggerak_ket'];?>
</b>
            </td>
            <td>
                <span style="text-decoration: underline;">Mesin Merk</span><br /><i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['mesin_merk'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Putaran Mesin</span><br /><i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['putaran_mesin'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
            <td>
                <span style="text-decoration: underline;">Type Mesin</span><br /><i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['type_mesin'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Serial No. Mesin</span><br /><i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['serial_no_mesin'])===null||$tmp==='' ? '' : $tmp);?>
</b>
            </td>
        </tr>
	</table>
</div>
<div class="rute-box">
	<h5>
        <a href="#" class="drop-up">
            DAYA MESIN :
        </a>
    </h5>
	<table class="table-view" width="100%">
        <tr>
            <td width='15%'>
                <span style="text-decoration: underline;">Daya Mesin 1</span><br /><i></i>
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['daya_mesin1'])===null||$tmp==='' ? '' : $tmp);?>
<b>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['satuan_mesin1'];?>
<b>
            </td>
            <td width='15%'>
                <span style="text-decoration: underline;">Daya Mesin 2</span><br /><i></i>
            </td>
            <td width='35%'>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['daya_mesin2'])===null||$tmp==='' ? '' : $tmp);?>
<b>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['satuan_mesin2'];?>
<b>
            </td>
        </tr>
        <tr>
            <td>
                <span style="text-decoration: underline;">Daya Mesin 3</span><br /><i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['daya_mesin3'])===null||$tmp==='' ? '' : $tmp);?>
<b>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['satuan_mesin3'];?>
<b>
            </td>
			<td>
                <span style="text-decoration: underline;">Daya Mesin 4</span><br /><i></i>
            </td>
            <td>
                <b><?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['daya_mesin4'])===null||$tmp==='' ? '' : $tmp);?>
<b>
				<b><?php echo $_smarty_tpl->getVariable('detail')->value['satuan_mesin4'];?>
<b>
            </td>
        </tr>
		
    </table>
</div>

<!-- include html -->
<?php $_template = new Smarty_Internal_Template("task/daftar_staff_syarat/task_form_dialog.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of include html -->