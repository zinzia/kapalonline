<?php /* Smarty version Smarty-3.0.7, created on 2015-12-11 03:47:53
         compiled from "application/views\task/daftar_staff_minute/pendaftaran.html" */ ?>
<?php /*%%SmartyHeaderCode:33485669e4f99f4ba2-37456923%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7d596417f640ab3959aabf93d16d7c46b4db9731' => 
    array (
      0 => 'application/views\\task/daftar_staff_minute/pendaftaran.html',
      1 => 1449780472,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '33485669e4f99f4ba2-37456923',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!-- include javascript -->
<?php $_template = new Smarty_Internal_Template("task/daftar_staff_minute/task_javascript.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of include javascript-->
<script type="text/javascript">
    $(document).ready(function () {
        // select all
        $("#proses-all").change(function () {
            var proses = $("#proses-all").val();
            $(".proses-item").val(proses);
        });
    });
</script>

<div class="breadcrum">
    <p>
        <a href="#">Task Manager Pengajuan Akta Pendaftaran</a><span></span>
        <a href="#"><?php echo $_smarty_tpl->getVariable('task')->value['task_nm'];?>
</a><span></span>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="pageRow">
        <div class="pageNav">
            <ul>
                <li class="info">
                    <b><?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['group_nm'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['group_nm'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['group_nm']));?>
</b>
                    <br /><br />
                    Nama Kapal : <?php echo (($tmp = @((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['nama_kapal'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal'])))===null||$tmp==='' ? '' : $tmp);?>

                    <br />
                    Registrasi : <?php echo (($tmp = @((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['registrasi_id'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['registrasi_id'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['registrasi_id'])))===null||$tmp==='' ? '' : $tmp);?>

                    <br />
                    Pemohon : <?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['nama_pemohon'])===null||$tmp==='' ? '-' : $tmp);?>
, <?php echo (($tmp = @$_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_pengajuan']))===null||$tmp==='' ? '-' : $tmp);?>

                </li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('task/akta_pendaftaran');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back to Waiting List</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="navigation" style="background-color: #eee; padding: 10px;">
    <div class="navigation-button">
        <ul>
            <?php if ($_smarty_tpl->getVariable('action')->value['action_send']=='1'){?>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url(((('task/daftar_staff_minute/send_process/').($_smarty_tpl->getVariable('detail')->value['registrasi_id'])).('/')).($_smarty_tpl->getVariable('detail')->value['process_id']));?>
" onclick="return confirm('Apakah anda yakin akan mengirimkan data berikut ini ke <?php echo $_smarty_tpl->getVariable('next')->value['role_nm'];?>
?')"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/arrow-right.png" alt="" /> Kirim ke <?php echo $_smarty_tpl->getVariable('next')->value['role_nm'];?>
</a></li>
            <?php }?>  
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<div class="rute-box">
    <h5>
        <a href="#" class="drop-up">
            PENYUSUNAN MINUTE AKTA & DAFTAR INDUK
        </a>
    </h5>
    <form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('task/daftar_staff_minute/approved_process');?>
" method="post">
        <input type="hidden" name="registrasi_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('detail')->value['registrasi_id'])===null||$tmp==='' ? '' : $tmp);?>
">
        <table class="table-view" width="100%">
		</table>
    </form>
</div>

<table style="line-height: 25px;" class="table-form" width="100%">
    <tr>
        <td align="center" colspan="2"><b>AKTA PENDAFTARAN KAPAL</b> </br> Nomor</br></br></td>
    </tr>
	<tr>
        <td style="padding-left: 90px; vertical-align: top;" width='10%'>Akta Tanggal : </br> <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('dtm')->value->get_full_date(date('Y-m-d')), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('dtm')->value->get_full_date(date('Y-m-d')),SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('dtm')->value->get_full_date(date('Y-m-d'))));?>
 
		</br> Nomor : </br> ___________________
		</br> Mengenai kapal <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['jenis_ket'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['jenis_ket'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['jenis_ket']));?>
 bernama <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['nama_kapal'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal']));?>
 </br>Milik : <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['nama_pemilik'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['nama_pemilik'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['nama_pemilik']));?>
 </br>berkedudukan di <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['kota'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['kota'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['kota']));?>

		
		</td >
        <td style="padding-right: 90px;"><p style="	text-indent: 40px;">
		Pendaftaran sebuah kapal tongkang bernama <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['nama_kapal'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['nama_kapal']));?>
 seperti diuraikan dalam Surat Ukur tertanggal <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['penerbitan_nm'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['penerbitan_nm'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['penerbitan_nm']));?>
, 
		<?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_surat_ukur']), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_surat_ukur']),SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_surat_ukur'])));?>
, <?php echo $_smarty_tpl->getVariable('detail')->value['no_surat_ukur'];?>
, dengan ukuran-ukuran ;
		
		<table width="100%">
		<tr>
			<td width="20%">
			Panjang
			</td >
			<td width="10%" align="center">:
			</td>
			<td><?php echo $_smarty_tpl->getVariable('detail')->value['dimensi_panjang'];?>
 meter ;
			</td>
		</tr>
		<tr>
			<td>
			Lebar
			</td>
			<td align="center">:
			</td>
			<td><?php echo $_smarty_tpl->getVariable('detail')->value['dimensi_lebar'];?>
 meter ;
			</td>
		</tr>
		<tr>
			<td>
			Dalam
			</td>
			<td align="center">:
			</td>
			<td><?php echo $_smarty_tpl->getVariable('detail')->value['dimensi_dalam'];?>
 meter ;
			</td>
		</tr>
		<tr>
			<td >
			Tonase Kotor (GT)
			</td>
			<td align="center">:
			</td>
			<td><?php echo $_smarty_tpl->getVariable('detail')->value['dimensi_isikotor'];?>
 ;
			</td>
		</tr>
		<tr>
			<td>
			Tonase Bersih (NT)
			</td>
			<td align="center">:
			</td>
			<td><?php echo $_smarty_tpl->getVariable('detail')->value['dimensi_isibersih'];?>
 ;
			</td>
		</tr>
		<tr>
			<td >
			Tanda Selar
			</td>
			<td align="center">:
			</td>
			<td><?php echo $_smarty_tpl->getVariable('detail')->value['tanda_selar'];?>
 ;
			</td>
		</tr>
		</table>
		
		Kapal dibuat di <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['tempat_pembuatan'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['tempat_pembuatan'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['tempat_pembuatan']));?>
 dalam tanggal <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_peletakan_lunas']), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_peletakan_lunas']),SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('dtm')->value->get_full_date($_smarty_tpl->getVariable('detail')->value['tgl_peletakan_lunas'])));?>
 terutama dari <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('detail')->value['bahan_ket'], 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('detail')->value['bahan_ket'],SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('detail')->value['bahan_ket']));?>
 dengan
		<?php echo $_smarty_tpl->getVariable('detail')->value['jml_geladak'];?>
 geladak dan dipergunakan dalam pelayaran di laut ;
		</br> Kapal belum didaftarkan dalam Daftar Kapal Indonesia ;
		</p></td>
    </tr>
	<tr>
	<td style="padding-left: 90px; padding-right: 90px;" colspan="2">
		<p style="	text-indent: 180px;" >Pada Hari Ini <?php echo $_smarty_tpl->getVariable('dtm')->value->get_day_indonesia(date('Y-m-d'));?>
 tanggal <?php echo ((mb_detect_encoding($_smarty_tpl->getVariable('dtm')->value->get_full_date(date('Y-m-d')), 'UTF-8, ISO-8859-1') === 'UTF-8') ? mb_strtoupper($_smarty_tpl->getVariable('dtm')->value->get_full_date(date('Y-m-d')),SMARTY_RESOURCE_CHAR_SET) : strtoupper($_smarty_tpl->getVariable('dtm')->value->get_full_date(date('Y-m-d'))));?>
 telah menghadap kepada kami, 
		</p>
	</td>
	</tr>
</table>

<!-- include html -->
<?php $_template = new Smarty_Internal_Template("task/daftar_staff_minute/task_form_dialog.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of include html -->