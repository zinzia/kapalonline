<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 07:05:39
         compiled from "application/views\pengaturan/airport/add.html" */ ?>
<?php /*%%SmartyHeaderCode:9037566521b365ad13-13867046%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '81f516db25b806e77b695238eff95aa1a820c0c0' => 
    array (
      0 => 'application/views\\pengaturan/airport/add.html',
      1 => 1441883433,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9037566521b365ad13-13867046',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/airport');?>
">Bandar Udara</a><span></span>
        <small>Add Data</small>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/airport');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/airport/add_process');?>
" method="post">
    <table class="table-input" width="100%">
        <tr class="headrow">
            <th colspan="4">Tambah Data Bandar Udara</th>
        </tr>
        <tr>
            <td width="15%">Nama Bandar Udara</td>
            <td width="35%">
                <input name="airport_nm" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['airport_nm'])===null||$tmp==='' ? '' : $tmp);?>
" size="35" maxlength="50" />
                <small>* wajib diisi</small>
            </td>
            <td width="15%">Domestik / Internasional</td>
            <td width="35%">
                <select name="airport_st" class="airport_st">
                    <option value="">--- Pilih ---</option>
                    <option value="domestik" <?php ob_start();?><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['airport_st'])===null||$tmp==='' ? '' : $tmp);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1=="domestik"){?>selected="selected"<?php }?>>DOMESTIK</option>
                    <option value="internasional" <?php ob_start();?><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['airport_st'])===null||$tmp==='' ? '' : $tmp);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2=="internasional"){?>selected="selected"<?php }?>>INTERNASIONAL</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>IATA</td>
            <td>
                <input name="airport_iata_cd" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['airport_iata_cd'])===null||$tmp==='' ? '' : $tmp);?>
" size="5" maxlength="3" />
                <small>* wajib diisi</small>
            </td>
            <td>ICAO</td>
            <td>
                <input name="airport_icao_cd" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['airport_icao_cd'])===null||$tmp==='' ? '' : $tmp);?>
" size="5" maxlength="4" />
                <small>* wajib diisi</small>
            </td>
        </tr>
        <tr>
            <td>Region</td>
            <td>
                <input name="airport_region" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['airport_region'])===null||$tmp==='' ? '' : $tmp);?>
" size="25" maxlength="50" />
                <small>* wajib diisi</small>
            </td>
            <td>Country</td>
            <td>
                <input name="airport_country" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['airport_country'])===null||$tmp==='' ? '' : $tmp);?>
" size="25" maxlength="50" />
                <small>* wajib diisi</small>
            </td>
        </tr>
        <tr>
            <td>Penyelenggara</td>
            <td colspan="3">
                <select name="airport_owner">
                    <option value=""></option>
                    <option value="UPT" <?php if ($_smarty_tpl->getVariable('result')->value['airport_owner']=='UPT'){?>selected="selected"<?php }?>>UPT</option>
                    <option value="Angkasa Pura I" <?php if ($_smarty_tpl->getVariable('result')->value['airport_owner']=='Angkasa Pura I'){?>selected="selected"<?php }?>>ANGKASA PURA I</option>
                    <option value="Angkasa Pura II" <?php if ($_smarty_tpl->getVariable('result')->value['airport_owner']=='Angkasa Pura II'){?>selected="selected"<?php }?>>ANGKASA PURA II</option>
                    <option value="Luar Negeri" <?php if ($_smarty_tpl->getVariable('result')->value['airport_owner']=='Luar Negeri'){?>selected="selected"<?php }?>>LUAR NEGERI</option>
                </select>
                <small>* wajib diisi</small>
            </td>
        </tr>
        <tr class="submit-box">
            <td colspan="4">
                <input type="submit" name="save" value="Simpan" class="submit-button" />
                <input type="reset" name="save" value="Reset" class="reset-button" />
            </td>
        </tr>
    </table>
</form>