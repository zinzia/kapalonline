<?php /* Smarty version Smarty-3.0.7, created on 2015-12-07 19:01:26
         compiled from "application/views\pengaturan/jenisdetail_kapal/edit.html" */ ?>
<?php /*%%SmartyHeaderCode:10589566575168df079-86329483%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a7f9dbc1d6cac09c25c4cffdadeb4cec974c62ed' => 
    array (
      0 => 'application/views\\pengaturan/jenisdetail_kapal/edit.html',
      1 => 1449489664,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10589566575168df079-86329483',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="breadcrum">
    <p>
        <a href="#">Settings</a><span></span>
        <a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/jenisdetail_kapal');?>
">Jenis Detail Kapal</a><span></span>
        <small>Edit Data</small>
    </p>
    <div class="clear"></div>
</div>
<div class="navigation">
    <div class="navigation-button">
        <ul>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/jenisdetail_kapal');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/back-icon.png" alt="" /> Back</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('pengaturan/jenisdetail_kapal/edit_process');?>
" method="post">
    <input name="jenisdetail_id" type="hidden" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['jenisdetail_id'])===null||$tmp==='' ? '' : $tmp);?>
" />
    <table class="table-input" width="100%" border='0'>
        <tr class="headrow">
            <th colspan="4">Edit Data Jenis Detail Kapal</th>
        </tr>
        <tr>
            <td width="15%">Jenis Detail</td>
            <td width="35%">
                <input name="jenisdetail_ket" type="text" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['jenisdetail_ket'])===null||$tmp==='' ? '' : $tmp);?>
" size="30" maxlength="50" />
                <em>* wajib diisi</em>
            </td>
        </tr>  
        <tr>
            <td>Jenis</td>
            <td>
                <select name="jenis_id" class="jenis_id">
                    <option value="">--- Pilih ---</option>
					<?php  $_smarty_tpl->tpl_vars['datajns'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('rs_namajenis_kapal')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['datajns']->key => $_smarty_tpl->tpl_vars['datajns']->value){
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['datajns']->value['jenis_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['datajns']->value['jenis_id']==$_smarty_tpl->getVariable('result')->value['jenis_id']){?> selected="selected" <?php }?>><?php echo $_smarty_tpl->tpl_vars['datajns']->value['jenis_ket'];?>
</option>
					<?php }} ?>
                </select>
                <em>* wajib diisi</em>
            </td>
        </tr>
        <tr class="submit-box">
            <td colspan="4">
                <input type="submit" name="save" value="Simpan" class="submit-button" />
                <input type="reset" name="save" value="Reset" class="reset-button" />
            </td>
        </tr>
    </table>
</form>