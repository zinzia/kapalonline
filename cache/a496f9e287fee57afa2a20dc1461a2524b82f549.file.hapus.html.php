<?php /* Smarty version Smarty-3.0.7, created on 2015-12-11 14:12:12
         compiled from "application/views\settings/portal/hapus.html" */ ?>
<?php /*%%SmartyHeaderCode:25734566a774ce27842-17304035%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a496f9e287fee57afa2a20dc1461a2524b82f549' => 
    array (
      0 => 'application/views\\settings/portal/hapus.html',
      1 => 1441883422,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '25734566a774ce27842-17304035',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="head-content">
    <h3>Web Portal</h3>
    <div class="head-content-nav">
        <ul>
            <li><a href="#" class="active">Hapus Data</a></li>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('settings/adminportal/add');?>
"><img src="<?php echo $_smarty_tpl->getVariable('BASEURL')->value;?>
resource/doc/images/icon/add-icon.png" alt="" /> Add Data</a></li>
            <li><a href="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('settings/adminportal');?>
">List Data</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<!-- notification template -->
<?php $_template = new Smarty_Internal_Template("base/templates/notification.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
<!-- end of notification template-->
<form action="<?php echo $_smarty_tpl->getVariable('config')->value->site_url('settings/adminportal/process_delete');?>
" method="post">
    <input type="hidden" name="portal_id" value="<?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['portal_id'])===null||$tmp==='' ? '' : $tmp);?>
" />
    <table class="table-input" width="100%">
        <tr class="headrow">
            <th colspan="2">Hapus Web Portal</th>
        </tr>
        <tr>
            <td width="25%">Nama Portal *</td>
            <td width="75%"><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['portal_nm'])===null||$tmp==='' ? '' : $tmp);?>
</td>
        </tr>
        <tr>
            <td>Judul Web *</td>
            <td><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['site_title'])===null||$tmp==='' ? '' : $tmp);?>
</td>
        </tr>
        <tr>
            <td>Deskripsi Web *</td>
            <td><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['site_desc'])===null||$tmp==='' ? '' : $tmp);?>
</td>
        </tr>
        <tr>
            <td>Meta Description *</td>
            <td><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['meta_desc'])===null||$tmp==='' ? '' : $tmp);?>
</td>
        </tr>
        <tr>
            <td>Meta Keyword *</td>
            <td><?php echo (($tmp = @$_smarty_tpl->getVariable('result')->value['meta_keyword'])===null||$tmp==='' ? '' : $tmp);?>
</td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" name="save" value="Hapus" class="edit-button" /> </td>
        </tr>
    </table>
</form>
