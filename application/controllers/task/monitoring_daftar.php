<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// load base class if needed
require_once( APPPATH . 'controllers/base/OperatorBase.php' );

class monitoring_daftar extends ApplicationBase{
    //put your code here
    public function __construct() {
        parent::__construct();
        // load model
        $this->load->model('regulator/m_monitoring_daftar');
        $this->load->model('m_monitoring');
        //$this->load->model('m_operator');
        $this->load->model('m_daftar');
        $this->load->model("regulator/m_task");
        // load library
        $this->load->library('pagination');
		$this->load->helper('download');
    }
        
    public function index() {
        $this->_set_page_rule("R");
        $this->smarty->assign("template_content","task/monitoring_daftar/index.html");
        // load javascript
        $this->smarty->load_javascript("resource/js/jquery/jquery-ui-1.9.2.custom.min.js");
        $this->smarty->load_javascript("resource/js/select2-3.4.5/select2.min.js");
        // load style ui
        $this->smarty->load_style("jquery.ui/redmond/jquery-ui-1.8.13.custom.css");
        $this->smarty->load_style("select2/select2.css");        
//var_dump($this->m_monitoring->get_list_role()); exit;		
        // load airlines
//        $this->smarty->assign("rs_airlines", $this->m_operator->get_all_airlines());
        // get search parameter
        $search = $this->tsession->userdata('search_monitoring');
        // search parameters
        $daftar_group = empty($search['daftar_group']) ? '%' : $search['daftar_group'];
        $registrasi_id = empty($search['registrasi_id']) ? '%' : '%' . $search['registrasi_id'] . '%';
        $nama_kapal = empty($search['nama_kapal']) ? '%' : '%' . $search['nama_kapal'] . '%';
        $role_id = empty($search['role_id']) ? '%' : $search['role_id'];
        $this->smarty->assign("search", $search);
        /* start of pagination --------------------- */
        // pagination
        $config['base_url'] = site_url("task/monitoring_daftar/index/");
        $config['total_rows'] = $this->m_monitoring_daftar->get_total_my_task_waiting_opr(array($daftar_group, $registrasi_id, $nama_kapal, $role_id, $this->com_user['pelabuhan_id']));
        $config['uri_segment'] = 4;
        $config['per_page'] = 50;
        $this->pagination->initialize($config);
        $pagination['data'] = $this->pagination->create_links();
        // pagination attribute
        $start = $this->uri->segment(4, 0) + 1;
        $end = $this->uri->segment(4, 0) + $config['per_page'];
        $end = (($end > $config['total_rows']) ? $config['total_rows'] : $end);
        $pagination['start'] = ($config['total_rows'] == 0) ? 0 : $start;
        $pagination['end'] = $end;
        $pagination['total'] = $config['total_rows'];
        // pagination assign value
        $this->smarty->assign("pagination", $pagination);
        $this->smarty->assign("no", $start);
        /* end of pagination ---------------------- */
        $params = array($daftar_group, $registrasi_id, $nama_kapal, $role_id, $this->com_user['pelabuhan_id'], ($start - 1), $config['per_page']);
        $this->smarty->assign("rs_id", $this->m_monitoring_daftar->get_list_my_task_waiting_opr($params));
        $this->smarty->assign("total", count($this->m_monitoring_daftar->get_list_my_task_waiting_opr($params)));
		$rs_group = $this->m_monitoring_daftar->get_all_group();
        $this->smarty->assign("rs_group", $rs_group);
        // list role
		$rs_roles = $this->m_monitoring->get_list_role();
        $this->smarty->assign("rs_roles", $rs_roles);
//var_dump($this->smarty->assign("rs_roles", $this->m_monitoring->get_list_role())); exit;		
        parent::display();
    }
    
    // proses pencarian
    public function proses_cari() {
		// set page rules
        $this->_set_page_rule("R");
        // data
        if ($this->input->post('save') == "Reset") {
            $this->tsession->unset_userdata('search_monitoring');
        } else {
            $params = array(
                "daftar_group" => $this->input->post("daftar_group"),
                "registrasi_id" => $this->input->post("registrasi_id"),
                "nama_kapal" => $this->input->post("nama_kapal"),
                "role_id" => $this->input->post("role_id"),
            );
            $this->tsession->set_userdata("search_monitoring", $params);
        }
        // redirect
        redirect("task/monitoring_daftar");
    }
    
    public function pendaftaran($registrasi_id = "") {
        // set rule
        $this->_set_page_rule("R");
        // set template content
        $this->smarty->assign("template_content", "task/monitoring_daftar/pendaftaran.html");
        // detail registrasi
        $detail = $this->m_monitoring_daftar->get_detail_registrasi_data_by_id(array($registrasi_id,$this->com_user['pelabuhan_id']));
        $this->smarty->assign("detail", $detail);
        if (empty($detail)) {
            redirect('task/monitoring_daftar');
        }
       
		// list persyaratan
        $rs_files = $this->m_monitoring_daftar->get_list_file_required(array($detail['daftar_group'], $detail['bukti_hakmilik']));
        $this->smarty->assign("rs_files", $rs_files);
        // get uploaded files
        $file_uploaded = array();
        $name_uploaded = array();
        $rs_uploaded = $this->m_monitoring_daftar->get_list_file_uploaded(array($registrasi_id));
        foreach ($rs_uploaded as $uploaded) {
            $file_uploaded[] = $uploaded['ref_id'];
            $name_uploaded[$uploaded['ref_id']] = $uploaded['file_name'];
        }
        $this->smarty->assign("file_uploaded", $file_uploaded);
        $this->smarty->assign("name_uploaded", $name_uploaded);
        // display
        parent::display();
    }
    
    // file download
    public function files_download($data_id = "", $ref_id = "") {
        // get detail data
        $params = array($data_id, $ref_id);
		$result = $this->m_monitoring_daftar->get_detail_files_by_id($params);
        if (empty($result)) {
            redirect("task/monitoring_daftar");
        }
        // filepath
        $file_path = $result['file_path'];
        if (is_file($file_path)) {
           // download
            $data = file_get_contents($file_path); // Read the file's contents
			$name = end(explode('/', $file_path));
			force_download($name, $data); 
            exit();
        } else {
            redirect('task/monitoring_daftar');
        }
    }

}
