<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// load base class if needed
require_once( APPPATH . 'controllers/base/OperatorBase.php' );

class jenisdetail_kapal extends ApplicationBase {

    // constructor
    public function __construct() {
        // parent constructor
        parent::__construct();
        // load model
        $this->load->model('m_jenisdetail_kapal');
        // load library
        $this->load->library('tnotification');
        $this->load->library('pagination');
    }

    // view
    public function index() {
        // set page rules
        $this->_set_page_rule("R");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/jenisdetail_kapal/list.html");
        // get search parameter
        $search = $this->tsession->userdata('jenisdetail_kapal_search');
        // search parameters
        $jenisdetail_ket = empty($search['jenisdetail_ket']) ? '%' : '%' . $search['jenisdetail_ket'] . '%';
         $this->smarty->assign("search", $search);
        /* start of pagination --------------------- */
        // pagination
        $config['base_url'] = site_url("pengaturan/jenisdetail_kapal/index/");
        $config['total_rows'] = $this->m_jenisdetail_kapal->get_total_jenisdetail_kapal(array($jenisdetail_ket));
        $config['uri_segment'] = 4;
        $config['per_page'] = 50;
        $this->pagination->initialize($config);
        $pagination['data'] = $this->pagination->create_links();
        // pagination attribute
        $start = $this->uri->segment(4, 0) + 1;
        $end = $this->uri->segment(4, 0) + $config['per_page'];
        $end = (($end > $config['total_rows']) ? $config['total_rows'] : $end);
        $pagination['start'] = ($config['total_rows'] == 0) ? 0 : $start;
        $pagination['end'] = $end;
        $pagination['total'] = $config['total_rows'];
        // pagination assign value
        $this->smarty->assign("pagination", $pagination);
        $this->smarty->assign("no", $start);
        /* end of pagination ---------------------- */
        // get list
        $params = array($jenisdetail_ket, ($start - 1), $config['per_page']);
        $this->smarty->assign("rs_jenisdetail_kapal", $this->m_jenisdetail_kapal->get_all_jenisdetail_kapal($params));
		//var_dump($this->m_jenisdetail_kapal->get_all_jenis_kapal());
		//exit;
        // notification
        $this->tnotification->display_notification();
		
        // output
        parent::display();
    }

    //proses pencarian
    public function proses_cari() {
        // set page rules
        $this->_set_page_rule("R");
        // data
        if ($this->input->post('save') == "Reset") {
            $this->tsession->unset_userdata('jenisdetail_kapal_search');
        } else {
            $params = array(
                "jenisdetail_ket" => $this->input->post("jenisdetail_ket"),
            );
            $this->tsession->set_userdata("jenisdetail_kapal_search", $params);
        }
        // redirect
        redirect("pengaturan/jenisdetail_kapal");
    }

    // add jns detail
    public function add() {
        $this->smarty->assign("rs_namajenis_kapal", $this->m_jenisdetail_kapal->get_all_jenis_kapal());
      // set page rules
        $this->_set_page_rule("C");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/jenisdetail_kapal/add.html");
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // add process
    public function add_process() {
        // set page rules
        $this->_set_page_rule("C");
        // cek input
        $this->tnotification->set_rules('jenisdetail_ket', 'Jenis Detail', 'required|max_length[50]');
        $this->tnotification->set_rules('jenis_id', 'Jenis ID', 'required');
        // run
        if ($this->tnotification->run() !== FALSE) {
            $params = array(
                $this->input->post('jenisdetail_ket'),
                $this->input->post('jenis_id'),
                $this->com_user['user_id']);
            // insert
            if ($this->m_jenisdetail_kapal->insert($params)) {
                // success
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal disimpan");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pengaturan/jenisdetail_kapal/add/");
    }

    // edit aircraft
    public function edit($jenisdetail_id = "") {
        $this->smarty->assign("rs_namajenis_kapal", $this->m_jenisdetail_kapal->get_all_jenis_kapal());
        // set page rules
        $this->_set_page_rule("U");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/jenisdetail_kapal/edit.html");
        // get data
        $result = $this->m_jenisdetail_kapal->get_jenisdetail_kapal_by_id($jenisdetail_id);
        $this->smarty->assign("result", $result);
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // edit process
    public function edit_process() {
        // set page rules
        $this->_set_page_rule("U");
        // cek input
        $this->tnotification->set_rules('jenisdetail_id', 'ID', 'trim|required');
        $this->tnotification->set_rules('jenisdetail_ket', 'Jenis Detail', 'required|max_length[50]');
        $this->tnotification->set_rules('jenis_id', 'Jenis ID', 'max_length[50]');
        // process
        if ($this->tnotification->run() !== FALSE) {
            $params = array(
                $this->input->post('jenisdetail_ket'),
                $this->input->post('jenis_id'),
				$this->input->post('jenisdetail_id'),
                $this->com_user['user_id'] 
            );
            if ($this->m_jenisdetail_kapal->update($params)) {
                // notifikasi
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal disimpan");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pengaturan/jenisdetail_kapal/edit/" . $this->input->post('jenisdetail_id'));
    }

    // hapus form
    public function delete($jenisdetail_id = "") {
        // set page rules
        $this->_set_page_rule("D");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/jenisdetail_kapal/delete.html");
        // get data
        $this->smarty->assign("result", $this->m_jenisdetail_kapal->get_jenisdetail_kapal_by_id($jenisdetail_id));
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // hapus process
    public function delete_process() {
        // set page rules
        $this->_set_page_rule("D");
        // cek input
        $this->tnotification->set_rules('jenisdetail_id', 'Jenis Detail ID', 'trim|required');
        // process
        if ($this->tnotification->run() !== FALSE) {
            $params = array($this->input->post('jenisdetail_id'));
            // insert
            if ($this->m_jenisdetail_kapal->delete($params)) {
                // notifikasi
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil dihapus");
                // default redirect
                redirect("pengaturan/jenisdetail_kapal");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal dihapus");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal dihapus");
        }
        // default redirect
        redirect("pengaturan/jenisdetail_kapal/delete/" . $this->input->post('jenisdetail_id'));
    }

}
