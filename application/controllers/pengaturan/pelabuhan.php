<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// load base class if needed
require_once( APPPATH . 'controllers/base/OperatorBase.php' );

class pelabuhan extends ApplicationBase {

    // constructor
    public function __construct() {
        // parent constructor
        parent::__construct();
        // load model
        $this->load->model('m_pelabuhan');
        // load library
        $this->load->library('tnotification');
        $this->load->library('pagination');
    }

    // view
    public function index() {
        // set page rules
        $this->_set_page_rule("R");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/pelabuhan/list.html");
        // get search parameter
        $search = $this->tsession->userdata('pelabuhan_search');
        // search parameters
        $pelabuhan_nm = empty($search['pelabuhan_nm']) ? '%' : '%' . $search['pelabuhan_nm'] . '%';
         $this->smarty->assign("search", $search);
        /* start of pagination --------------------- */
        // pagination
        $config['base_url'] = site_url("pengaturan/pelabuhan/index/");
        $config['total_rows'] = $this->m_pelabuhan->get_total_pelabuhan(array($pelabuhan_nm));
        $config['uri_segment'] = 4;
        $config['per_page'] = 50;
        $this->pagination->initialize($config);
        $pagination['data'] = $this->pagination->create_links();
        // pagination attribute
        $start = $this->uri->segment(4, 0) + 1;
        $end = $this->uri->segment(4, 0) + $config['per_page'];
        $end = (($end > $config['total_rows']) ? $config['total_rows'] : $end);
        $pagination['start'] = ($config['total_rows'] == 0) ? 0 : $start;
        $pagination['end'] = $end;
        $pagination['total'] = $config['total_rows'];
        // pagination assign value
        $this->smarty->assign("pagination", $pagination);
        $this->smarty->assign("no", $start);
        /* end of pagination ---------------------- */
        // get list
        $params = array($pelabuhan_nm, ($start - 1), $config['per_page']);
        $this->smarty->assign("rs_pelabuhan", $this->m_pelabuhan->get_all_pelabuhan($params));
		//var_dump($this->m_jenisdetail_kapal->get_all_jenis_kapal());
		//exit;
        // notification
        $this->tnotification->display_notification();
		
        // output
        parent::display();
    }

    //proses pencarian
    public function proses_cari() {
        // set page rules
        $this->_set_page_rule("R");
        // data
        if ($this->input->post('save') == "Reset") {
            $this->tsession->unset_userdata('pelabuhan_search');
        } else {
            $params = array(
                "pelabuhan_nm" => $this->input->post("pelabuhan_nm"),
            );
            $this->tsession->set_userdata("pelabuhan_search", $params);
        }
        // redirect
        redirect("pengaturan/pelabuhan");
    }

    // add pelabuhan
    public function add() {
      // set page rules
        $this->_set_page_rule("C");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/pelabuhan/add.html");
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // add process
    public function add_process() {
        // set page rules
        $this->_set_page_rule("C");
        // cek input
        $this->tnotification->set_rules('pelabuhan_nm', 'Nama Pelabuhan', 'required|max_length[50]');
        $this->tnotification->set_rules('pelabuhan_kd', 'Kode Pelabuhan', 'required|max_length[3]');
        $this->tnotification->set_rules('st_daftar', 'Tempat Pendaftaran', 'required');
        $this->tnotification->set_rules('st_ukur', 'Tempat Pengukuran', 'required');
        // run
        if ($this->tnotification->run() !== FALSE) {
            $params = array(
                $this->input->post('pelabuhan_nm'),
                $this->input->post('pelabuhan_kd'),
                $this->input->post('st_daftar'),
                $this->input->post('st_ukur'),
               $this->com_user['user_id']);
            // insert
            if ($this->m_pelabuhan->insert($params)) {
                // success
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal disimpan");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pengaturan/pelabuhan/add/");
    }

    // edit aircraft
    public function edit($pelabuhan_id = "") {
        // set page rules
        $this->_set_page_rule("U");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/pelabuhan/edit.html");
        // get data
        $result = $this->m_pelabuhan->get_pelabuhan_by_id($pelabuhan_id);
        $this->smarty->assign("result", $result);
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // edit process
    public function edit_process() {
        // set page rules
        $this->_set_page_rule("U");
        // cek input
        $this->tnotification->set_rules('pelabuhan_id', 'ID Pelabuhan', 'trim|required');
        $this->tnotification->set_rules('pelabuhan_nm', 'Nama Pelabuhan', 'required|max_length[50]');
        $this->tnotification->set_rules('pelabuhan_kd', 'Kode Pelabuhan', 'required|max_length[3]');
        $this->tnotification->set_rules('st_daftar', 'Tempat Pendaftaran', 'required');
        $this->tnotification->set_rules('st_ukur', 'Tempat Pengukuran', 'required');
        // process
        if ($this->tnotification->run() !== FALSE) {
            $params = array(
                $this->input->post('pelabuhan_nm'),
                $this->input->post('pelabuhan_kd'),
                $this->input->post('st_daftar'),
                $this->input->post('st_ukur'),
				$this->input->post('pelabuhan_id'),
                $this->com_user['user_id'] 
            );
            if ($this->m_pelabuhan->update($params)) {
                // notifikasi
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal disimpan");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pengaturan/pelabuhan/edit/" . $this->input->post('pelabuhan_id'));
    }

    // hapus form
    public function delete($pelabuhan_id = "") {
        // set page rules
        $this->_set_page_rule("D");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/pelabuhan/delete.html");
        // get data
        $this->smarty->assign("result", $this->m_pelabuhan->get_pelabuhan_by_id($pelabuhan_id));
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // hapus process
    public function delete_process() {
        // set page rules
        $this->_set_page_rule("D");
        // cek input
        $this->tnotification->set_rules('pelabuhan_id', 'ID Pelabuhan', 'trim|required');
        // process
        if ($this->tnotification->run() !== FALSE) {
            $params = array($this->input->post('pelabuhan_id'));
            // insert
            if ($this->m_pelabuhan->delete($params)) {
                // notifikasi
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil dihapus");
                // default redirect
                redirect("pengaturan/pelabuhan");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal dihapus");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal dihapus");
        }
        // default redirect
        redirect("pengaturan/pelabuhan/delete/" . $this->input->post('pelabuhan_id'));
    }

}
