<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// load base class if needed
require_once( APPPATH . 'controllers/base/OperatorBase.php' );

class file_references extends ApplicationBase {

    // constructor
    public function __construct() {
        // parent constructor
        parent::__construct();
        // load model
        $this->load->model('m_file_references');
        // load library
        $this->load->library('tnotification');
        $this->load->library('pagination');
    }

    // view
    public function index() {
        // set page rules
        $this->_set_page_rule("R");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/file_references/list.html");
        // get search parameter
        $search = $this->tsession->userdata('file_references_search');
        // search parameters
        $ref_field = empty($search['ref_field']) ? '%' : '%' . $search['ref_field'] . '%';
         $this->smarty->assign("search", $search);
        /* start of pagination --------------------- */
        // pagination
        $config['base_url'] = site_url("pengaturan/file_references/index/");
        $config['total_rows'] = $this->m_file_references->get_total_file_references(array($ref_field));
        $config['uri_segment'] = 4;
        $config['per_page'] = 50;
        $this->pagination->initialize($config);
        $pagination['data'] = $this->pagination->create_links();
        // pagination attribute
        $start = $this->uri->segment(4, 0) + 1;
        $end = $this->uri->segment(4, 0) + $config['per_page'];
        $end = (($end > $config['total_rows']) ? $config['total_rows'] : $end);
        $pagination['start'] = ($config['total_rows'] == 0) ? 0 : $start;
        $pagination['end'] = $end;
        $pagination['total'] = $config['total_rows'];
        // pagination assign value
        $this->smarty->assign("pagination", $pagination);
        $this->smarty->assign("no", $start);
        /* end of pagination ---------------------- */
        // get list
        $params = array($ref_field, ($start - 1), $config['per_page']);
        $this->smarty->assign("rs_file_references", $this->m_file_references->get_all_file_references($params));
		
        // notification
        $this->tnotification->display_notification();
		
        // output
        parent::display();
    }

    //proses pencarian
    public function proses_cari() {
        // set page rules
        $this->_set_page_rule("R");
        // data
        if ($this->input->post('save') == "Reset") {
            $this->tsession->unset_userdata('file_references_search');
        } else {
            $params = array(
                "ref_field" => $this->input->post("ref_field"),
            );
            $this->tsession->set_userdata("file_references_search", $params);
        }
        // redirect
        redirect("pengaturan/file_references");
    }

    // add file
    public function add() {
        // set page rules
        $this->_set_page_rule("C");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/file_references/add.html");
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // add process
    public function add_process() {
        // set page rules
        $this->_set_page_rule("C");
        // cek input
        $this->tnotification->set_rules('ref_field', 'File', 'required|max_length[50]');
        $this->tnotification->set_rules('ref_name', 'Name', 'required|max_length[50]');
        $this->tnotification->set_rules('ref_size', 'Size', 'required');
        $this->tnotification->set_rules('ref_allowed', 'Allowed', 'required|max_length[50]');
        $this->tnotification->set_rules('ref_required', 'Required', 'required');
        // run
        if ($this->tnotification->run() !== FALSE) {
            $params = array(
                $this->input->post('ref_field'),
                $this->input->post('ref_name'),
                $this->input->post('ref_size'),
                $this->input->post('ref_allowed'),
                $this->input->post('ref_required'),
                $this->com_user['user_id']);
            // insert
            if ($this->m_file_references->insert($params)) {
                // success
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal disimpan");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pengaturan/file_references/add/");
    }

    // edit aircraft
    public function edit($ref_id = "") {
        // set page rules
        $this->_set_page_rule("U");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/file_references/edit.html");
        // get data
        $result = $this->m_file_references->get_file_references_by_id($ref_id);
        $this->smarty->assign("result", $result);
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // edit process
    public function edit_process() {
        // set page rules
        $this->_set_page_rule("U");
        // cek input
        $this->tnotification->set_rules('ref_id', 'ID', 'trim|required');
        $this->tnotification->set_rules('ref_field', 'File', 'required|max_length[50]');
        $this->tnotification->set_rules('ref_name', 'Name', 'max_length[50]');
        $this->tnotification->set_rules('ref_size', 'Size', 'required');
        $this->tnotification->set_rules('ref_allowed', 'Allowed', 'required|max_length[50]');
        $this->tnotification->set_rules('ref_required', 'Required', 'required');
        // process
        if ($this->tnotification->run() !== FALSE) {
            $params = array(
                $this->input->post('ref_field'),
                $this->input->post('ref_name'),
                $this->input->post('ref_size'),
                $this->input->post('ref_allowed'),
                $this->input->post('ref_required'),
				$this->input->post('ref_id'),
                $this->com_user['user_id'] 
            );
            if ($this->m_file_references->update($params)) {
                // notifikasi
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal disimpan");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pengaturan/file_references/edit/" . $this->input->post('ref_id'));
    }

    // hapus form
    public function delete($ref_id = "") {
        // set page rules
        $this->_set_page_rule("D");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/file_references/delete.html");
        // get data
        $this->smarty->assign("result", $this->m_file_references->get_file_references_by_id($ref_id));
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // hapus process
    public function delete_process() {
        // set page rules
        $this->_set_page_rule("D");
        // cek input
        $this->tnotification->set_rules('ref_id', 'Reference ID', 'trim|required');
        // process
        if ($this->tnotification->run() !== FALSE) {
            $params = array($this->input->post('ref_id'));
            // insert
            if ($this->m_file_references->delete($params)) {
                // notifikasi
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil dihapus");
                // default redirect
                redirect("pengaturan/file_references");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal dihapus");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal dihapus");
        }
        // default redirect
        redirect("pengaturan/file_references/delete/" . $this->input->post('ref_id'));
    }

}
