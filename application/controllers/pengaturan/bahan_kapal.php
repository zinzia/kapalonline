<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// load base class if needed
require_once( APPPATH . 'controllers/base/OperatorBase.php' );

class bahan_kapal extends ApplicationBase {

    // constructor
    public function __construct() {
        // parent constructor
        parent::__construct();
        // load model
        $this->load->model('m_bahan_kapal');
        // load library
        $this->load->library('tnotification');
        $this->load->library('pagination');
    }

    // view
    public function index() {
        // set page rules
        $this->_set_page_rule("R");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/bahan_kapal/list.html");
        // get search parameter
        $search = $this->tsession->userdata('bahan_kapal_search');
        // search parameters
        $bahan_ket = empty($search['bahan_ket']) ? '%' : '%' . $search['bahan_ket'] . '%';
        $this->smarty->assign("search", $search);
        /* start of pagination --------------------- */
        // pagination
        $config['base_url'] = site_url("pengaturan/bahan_kapal/index/");
        $config['total_rows'] = $this->m_bahan_kapal->get_total_bahan_kapal(array($bahan_ket));
        $config['uri_segment'] = 4;
        $config['per_page'] = 50;
        $this->pagination->initialize($config);
        $pagination['data'] = $this->pagination->create_links();
        // pagination attribute
        $start = $this->uri->segment(4, 0) + 1;
        $end = $this->uri->segment(4, 0) + $config['per_page'];
        $end = (($end > $config['total_rows']) ? $config['total_rows'] : $end);
        $pagination['start'] = ($config['total_rows'] == 0) ? 0 : $start;
        $pagination['end'] = $end;
        $pagination['total'] = $config['total_rows'];
        // pagination assign value
        $this->smarty->assign("pagination", $pagination);
        $this->smarty->assign("no", $start);
        /* end of pagination ---------------------- */
        // get list
        $params = array($bahan_ket, ($start - 1), $config['per_page']);
        $this->smarty->assign("rs_bahan_kapal", $this->m_bahan_kapal->get_all_bahan_kapal($params));
		
        // notification
        $this->tnotification->display_notification();
		
        // output
        parent::display();
    }

    //proses pencarian
    public function proses_cari() {
        // set page rules
        $this->_set_page_rule("R");
        // data
        if ($this->input->post('save') == "Reset") {
            $this->tsession->unset_userdata('bahan_kapal_search');
        } else {
            $params = array(
                "bahan_ket" => $this->input->post("bahan_ket"),
               // "registration" => $this->input->post("registration"),
            );
            $this->tsession->set_userdata("bahan_kapal_search", $params);
        }
        // redirect
        redirect("pengaturan/bahan_kapal");
    }

    // add airlines
    public function add() {
        // set page rules
        $this->_set_page_rule("C");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/bahan_kapal/add.html");
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // add process
    public function add_process() {
        // set page rules
        $this->_set_page_rule("C");
        // cek input
        $this->tnotification->set_rules('bahan_ket', 'Bahan', 'required|max_length[50]');
        $this->tnotification->set_rules('bahan_ket_eng', 'Bahan Eng', 'max_length[50]');
        // run
        if ($this->tnotification->run() !== FALSE) {
            $params = array(
                $this->input->post('bahan_ket'),
                $this->input->post('bahan_ket_eng'),
                $this->com_user['user_id']);
            // insert
            if ($this->m_bahan_kapal->insert($params)) {
                // success
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal disimpan");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pengaturan/bahan_kapal/add/");
    }

    // edit bahan kapal
    public function edit($bahan_id = "") {
        // set page rules
        $this->_set_page_rule("U");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/bahan_kapal/edit.html");
        // get data
        $result = $this->m_bahan_kapal->get_bahan_kapal_by_id($bahan_id);
        $this->smarty->assign("result", $result);
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // edit process
    public function edit_process() {
        // set page rules
        $this->_set_page_rule("U");
        // cek input
        $this->tnotification->set_rules('bahan_id', 'ID', 'trim|required');
        $this->tnotification->set_rules('bahan_ket', 'Bahan', 'required|max_length[50]');
        $this->tnotification->set_rules('bahan_ket_eng', 'Bahan Eng', 'max_length[50]');
        // process
        if ($this->tnotification->run() !== FALSE) {
            $params = array(
                $this->input->post('bahan_ket'),
                $this->input->post('bahan_ket_eng'),
				$this->input->post('bahan_id'),
                $this->com_user['user_id'] 
            );
            if ($this->m_bahan_kapal->update($params)) {
                // notifikasi
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal disimpan");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pengaturan/bahan_kapal/edit/" . $this->input->post('bahan_id'));
    }

    // hapus form
    public function delete($bahan_id = "") {
        // set page rules
        $this->_set_page_rule("D");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/bahan_kapal/delete.html");
        // get data
        $this->smarty->assign("result", $this->m_bahan_kapal->get_bahan_kapal_by_id($bahan_id));
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // hapus process
    public function delete_process() {
        // set page rules
        $this->_set_page_rule("D");
        // cek input
        $this->tnotification->set_rules('bahan_id', 'Bahan Kapal ID', 'trim|required');
        // process
        if ($this->tnotification->run() !== FALSE) {
            $params = array($this->input->post('bahan_id'));
            // insert
            if ($this->m_bahan_kapal->delete($params)) {
                // notifikasi
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil dihapus");
                // default redirect
                redirect("pengaturan/bahan_kapal");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal dihapus");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal dihapus");
        }
        // default redirect
        redirect("pengaturan/bahan_kapal/delete/" . $this->input->post('bahan_id'));
    }

}
