<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// load base class if needed
require_once( APPPATH . 'controllers/base/OperatorBase.php' );

class kategori_kapal extends ApplicationBase {

    // constructor
    public function __construct() {
        // parent constructor
        parent::__construct();
        // load model
        $this->load->model('m_kategori_kapal');
        // load library
        $this->load->library('tnotification');
        $this->load->library('pagination');
    }

    // view
    public function index() {
        // set page rules
        $this->_set_page_rule("R");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/kategori_kapal/list.html");
        // get search parameter
        $search = $this->tsession->userdata('kategori_kapal_search');
        // search parameters
        $kategori_nm = empty($search['kategori_nm']) ? '%' : '%' . $search['kategori_nm'] . '%';
         $this->smarty->assign("search", $search);
        /* start of pagination --------------------- */
        // pagination
        $config['base_url'] = site_url("pengaturan/kategori_kapal/index/");
        $config['total_rows'] = $this->m_kategori_kapal->get_total_kategori_kapal(array($kategori_nm));
        $config['uri_segment'] = 4;
        $config['per_page'] = 50;
        $this->pagination->initialize($config);
        $pagination['data'] = $this->pagination->create_links();
        // pagination attribute
        $start = $this->uri->segment(4, 0) + 1;
        $end = $this->uri->segment(4, 0) + $config['per_page'];
        $end = (($end > $config['total_rows']) ? $config['total_rows'] : $end);
        $pagination['start'] = ($config['total_rows'] == 0) ? 0 : $start;
        $pagination['end'] = $end;
        $pagination['total'] = $config['total_rows'];
        // pagination assign value
        $this->smarty->assign("pagination", $pagination);
        $this->smarty->assign("no", $start);
        /* end of pagination ---------------------- */
        // get list
        $params = array($kategori_nm, ($start - 1), $config['per_page']);
        $this->smarty->assign("rs_kategori_kapal", $this->m_kategori_kapal->get_all_kategori_kapal($params));
		
        // notification
        $this->tnotification->display_notification();
		
        // output
        parent::display();
    }

    //proses pencarian
    public function proses_cari() {
        // set page rules
        $this->_set_page_rule("R");
        // data
        if ($this->input->post('save') == "Reset") {
            $this->tsession->unset_userdata('kategori_kapal_search');
        } else {
            $params = array(
                "kategori_nm" => $this->input->post("kategori_nm"),
            );
            $this->tsession->set_userdata("kategori_kapal_search", $params);
        }
        // redirect
        redirect("pengaturan/kategori_kapal");
    }

    // add kategori
    public function add() {
        // set page rules
        $this->_set_page_rule("C");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/kategori_kapal/add.html");
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // add process
    public function add_process() {
        // set page rules
        $this->_set_page_rule("C");
        // cek input
        $this->tnotification->set_rules('kategori_nm', 'Nama Kategori', 'required|max_length[50]');
        $this->tnotification->set_rules('kategori_kd', 'Kode', 'required|max_length[50]');
        // run
        if ($this->tnotification->run() !== FALSE) {
            $params = array(
                $this->input->post('kategori_nm'),
                $this->input->post('kategori_kd'),
                $this->com_user['user_id']);
            // insert
            if ($this->m_kategori_kapal->insert($params)) {
                // success
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal disimpan");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pengaturan/kategori_kapal/add/");
    }

    // edit aircraft
    public function edit($kategori_id = "") {
        // set page rules
        $this->_set_page_rule("U");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/kategori_kapal/edit.html");
        // get data
        $result = $this->m_kategori_kapal->get_kategori_kapal_by_id($kategori_id);
        $this->smarty->assign("result", $result);
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // edit process
    public function edit_process() {
        // set page rules
        $this->_set_page_rule("U");
        // cek input
        $this->tnotification->set_rules('kategori_id', 'Kategori ID', 'trim|required');
        $this->tnotification->set_rules('kategori_nm', 'Nama Kategori', 'required|max_length[50]');
        $this->tnotification->set_rules('kategori_kd', 'Kode', 'max_length[50]');
        // process
        if ($this->tnotification->run() !== FALSE) {
            $params = array(
                $this->input->post('kategori_nm'),
                $this->input->post('kategori_kd'),
				$this->input->post('kategori_id'),
                $this->com_user['user_id'] 
            );
            if ($this->m_kategori_kapal->update($params)) {
                // notifikasi
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal disimpan");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pengaturan/kategori_kapal/edit/" . $this->input->post('kategori_id'));
    }

    // hapus form
    public function delete($kategori_id = "") {
        // set page rules
        $this->_set_page_rule("D");
        // set template content
        $this->smarty->assign("template_content", "pengaturan/kategori_kapal/delete.html");
        // get data
        $this->smarty->assign("result", $this->m_kategori_kapal->get_kategori_kapal_by_id($kategori_id));
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // hapus process
    public function delete_process() {
        // set page rules
        $this->_set_page_rule("D");
        // cek input
        $this->tnotification->set_rules('kategori_id', 'Kategori ID', 'trim|required');
        // process
        if ($this->tnotification->run() !== FALSE) {
            $params = array($this->input->post('kategori_id'));
            // insert
            if ($this->m_kategori_kapal->delete($params)) {
                // notifikasi
                $this->tnotification->delete_last_field();
                $this->tnotification->sent_notification("success", "Data berhasil dihapus");
                // default redirect
                redirect("pengaturan/kategori_kapal");
            } else {
                // default error
                $this->tnotification->sent_notification("error", "Data gagal dihapus");
            }
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal dihapus");
        }
        // default redirect
        redirect("pengaturan/kategori_kapal/delete/" . $this->input->post('kategori_id'));
    }

}
