<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// load base class if needed
require_once( APPPATH . 'controllers/base/MemberBase.php' );

class jadwal_ttd extends ApplicationBase {

    private $group_id = 1;
    private $flow_id = 9;
    private $next_id = 10;
	
    // constructor
    public function __construct() {
        // parent constructor
        parent::__construct();
        // load model
        $this->load->model('m_jadwal_ttd');
        $this->load->model('m_email');
        $this->load->model('regulator/m_task');
        // load library
        $this->load->library('tnotification');
        //load helper
        $this->load->library('doslibrary');
        $this->smarty->assign("dos",  $this->doslibrary);
    }

    // index
    public function index() {
        // set page rules
        $this->_set_page_rule("R");
        // set template content
        $this->smarty->assign("template_content", "member/jadwal_ttd/index.html");
        // list waiting
        $rs_id = $this->m_jadwal_ttd->get_list_pending_task_waiting(array($this->com_user['role_id'], $this->com_user['user_id'], $this->group_id));
        $this->smarty->assign("rs_id", $rs_id);
        $this->smarty->assign("total", count($rs_id));
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }
 
 // BARU
    public function reschedule($registrasi_id = "") {
//var_dump($registrasi_id); exit;
	// set rule
        $this->_set_page_rule("C");

        // set template content
        $this->smarty->assign("template_content", "member/daftar_reschedule/pendaftaran.html");
        // url path
        $this->smarty->assign("url_path", "member/daftar_reschedule/pendaftaran");
        // load javascript
        $this->smarty->load_javascript("resource/js/jquery/jquery-ui-1.9.2.custom.min.js");
        $this->smarty->load_javascript("resource/js/select2-3.4.5/select2.min.js");
        // load style ui
        $this->smarty->load_style("jquery.ui/redmond/jquery-ui-1.8.13.custom.css");
        $this->smarty->load_style("select2/select2.css");
        /*
          * TASK
         */
        // detail task
        $task = $this->m_jadwal_ttd->get_detail_task_by_id($this->flow_id);
        $this->smarty->assign("task", $task);
        // next task
        $next = $this->m_jadwal_ttd->get_detail_task_by_id(array($this->next_id));
        $this->smarty->assign("next", $next);
        // list process
        //$this->smarty->assign("rs_process", $this->m_jadwal_ttd->get_list_process_by_id(array($registrasi_id)));
        /*
        * REGISTRASI
         */
        // detail registrasi
        $detail = $this->m_jadwal_ttd->get_detail_registrasi_member_by_id(array($registrasi_id, $this->com_user['user_id']));
        $this->smarty->assign("detail", $detail);
        if (empty($detail)) {
            redirect('member/jadwal_ttd');
        }
        /*
         * ACTION
         */
        $action = $this->m_jadwal_ttd->get_action_control(array($this->com_user['role_id'], $detail['daftar_group']));
        $this->smarty->assign("action", $action);
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        /*
         // * TARIF
         // */
        // $tarif = $this->m_task->get_tarif_rute_baru();
        // $this->smarty->assign("tarif", $tarif);
        // display
        parent::display();
    }
    // send process
    public function send_schedule($registrasi_id = "", $process_id = "") {
        // set page rules
        $this->_set_page_rule("C");

        $this->tnotification->set_rules('tgl_ttd_akta_1', 'Opsi Tanggal 1', 'trim|required');
        $this->tnotification->set_rules('tgl_ttd_akta_2', 'Opsi Tanggal 2', 'trim|required');
        $this->tnotification->set_rules('tgl_ttd_akta_3', 'Opsi Tanggal 3', 'trim|required');
        // get task
//var_dump($this->tnotification->run());		
        if ($this->tnotification->run() !== FALSE) {
			$detail = $this->m_task->get_registrasi_waiting_by_id_user(array($registrasi_id, $this->com_user['role_id'], $this->com_user['user_id']));
			if (empty($detail)) {
				redirect('member/jadwal_ttd');
			}
			// action
			$action = $this->m_task->get_action_control(array($this->com_user['role_id'], $detail['daftar_group']));
			if ($action['action_send'] == '0') {
				redirect('member/jadwal_ttd');
			}

			// update
			$params = array('approve', 'done', $this->com_user['user_id'], $process_id);
			if ($this->m_task->action_update($params)) {
				$process_id = $this->m_task->get_process_id();
            // process flow
				$params = array($process_id, $registrasi_id, $this->next_id, $this->com_user['user_id']);
				$this->m_task->insert_process($params);
				$tanggal1 = $this->input->post('tgl_ttd_akta_1');
				$tanggal2 = $this->input->post('tgl_ttd_akta_2');
				$tanggal3 = $this->input->post('tgl_ttd_akta_3');
				$data = array($registrasi_id, $this->com_user['role_id'], $tanggal1, $tanggal2, $tanggal3);
				$this->m_task->insert_schedule($data);
//var_dump($this->m_task->insert_process($params)); exit;			
            // // send mail notification
				// get role next flow
				$next_role = $this->m_task->get_role_next_from(array($this->next_id));
            
				$this->m_email->mail_konfirm_reschedule($registrasi_id, $next_role['role_id'], $detail['pelabuhan_id']);
				
				// success
				$this->tnotification->delete_last_field();
				$this->tnotification->sent_notification("success", "Data berhasil disimpan");
				// default redirect
				redirect("member/jadwal_ttd");
			} else {
				// // default error
				$this->tnotification->sent_notification("error", "Data gagal disimpan");
			}
			// default redirect
			redirect("member/jadwal_ttd/" . $detail['group_alias'] . "/" . $registrasi_id);
		}
		else{
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
			redirect("member/jadwal_ttd/reschedule/" . $registrasi_id);

		}
	}	
		    
}