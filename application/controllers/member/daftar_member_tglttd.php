<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// load base class if needed
require_once( APPPATH . 'controllers/base/MemberBase.php' );

class daftar_member_tglttd extends ApplicationBase {

    private $flow_id = 9;
    private $next_id = 10;
    private $prev_id = 7;

    //put your code here
    public function __construct() {
        parent::__construct();
        // load model
        $this->load->model("regulator/m_task");
        $this->load->model("m_email");
        // load library
        $this->load->library("tnotification");
    }

    /*
     * GROUP
     */

    // BARU
    public function pendaftaran($registrasi_id = "") {
        // set rule
        $this->_set_page_rule("R");
        // set template content
        $this->smarty->assign("template_content", "member/daftar_member_tglttd/pendaftaran.html");
        // url path
        $this->smarty->assign("url_path", "member/daftar_member_tglttd/pendaftaran");
        // load javascript
        $this->smarty->load_javascript("resource/js/jquery/jquery-ui-1.9.2.custom.min.js");
        $this->smarty->load_javascript("resource/js/select2-3.4.5/select2.min.js");
        // load style ui
        $this->smarty->load_style("jquery.ui/redmond/jquery-ui-1.8.13.custom.css");
        $this->smarty->load_style("select2/select2.css");
        /*
         * TASK
         */
        // detail task
        $task = $this->m_task->get_detail_task_by_id($this->flow_id);
        $this->smarty->assign("task", $task);
        // // prev task
        $prev = $this->m_task->get_detail_task_by_id(array($this->prev_id));
        $this->smarty->assign("prev", $prev);
        // next task
        $next = $this->m_task->get_detail_task_by_id(array($this->next_id));
        $this->smarty->assign("next", $next);
        // list process
        $this->smarty->assign("rs_process", $this->m_task->get_list_process_by_id(array($registrasi_id)));
        /*
         * REGISTRASI
         */
        // detail registrasi
        $detail = $this->m_task->get_detail_registrasi_member_by_id(array($registrasi_id, $this->com_user['user_id']));
        $this->smarty->assign("detail", $detail);
        if (empty($detail)) {
            redirect('member/jadwal_ttd');
        }
        // total catatan permohonan
        $this->smarty->assign("total_catatan", $this->m_task->get_total_catatan_by_registrasi($registrasi_id));
        
		// list persyaratan
        $rs_files = $this->m_task->get_list_file_required(array($detail['daftar_group'], $detail['bukti_hakmilik']));
        $this->smarty->assign("rs_files", $rs_files);
        // get uploaded files
        $file_uploaded = array();
        $rs_uploaded = $this->m_task->get_list_file_uploaded(array($registrasi_id));
        foreach ($rs_uploaded as $uploaded) {
            $file_uploaded[] = $uploaded['ref_id'];
            $name_uploaded[$uploaded['ref_id']] = $uploaded['file_name'];
        }
        $this->smarty->assign("file_uploaded", $file_uploaded);
        $this->smarty->assign("name_uploaded", $name_uploaded);
        // -----------------------
        // $this->smarty->assign("notes", $notes);
        /*
         * ACTION
         */
        $action = $this->m_task->get_action_control(array($this->com_user['role_id'], $detail['daftar_group']));
        $this->smarty->assign("action", $action);
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        /*
         // * TARIF
         // */
        // $tarif = $this->m_task->get_tarif_rute_baru();
        // $this->smarty->assign("tarif", $tarif);
        // display
        parent::display();
    }

    /*
     * ACTION
     */

    // approval
    public function approved_process() {
        // set page rules
        $this->_set_page_rule("U");
        // cek input
        $this->tnotification->set_rules('registrasi_id', 'ID Registrasi', 'required');
        $this->tnotification->set_rules('daftar_approval[]', 'Status', 'required');
        // registrasi id
        $registrasi_id = $this->input->post('registrasi_id');
        // get detail data
        $result = $this->m_task->get_detail_registrasi_waiting_by_id(array($registrasi_id, $this->com_user['role_id']));
        if (empty($result)) {
            redirect('task/daftar_rute');
        }
        // process
        if ($this->tnotification->run() !== FALSE) {
            // approve selected
            $daftar_approval = $this->input->post('daftar_approval');
            // --
            if (!empty($daftar_approval)) {
                foreach ($daftar_approval as $daftar_id => $value) {
                    // update
                    $params = array(
                        'daftar_approval' => $value,
                    );
                    $where = array(
                        'daftar_id' => $daftar_id,
                        'daftar_completed' => '0',
                        'registrasi_id' => $registrasi_id,
                    );
                    $this->m_task->update_daftar($params, $where);
                }
            }
            $this->tnotification->sent_notification("success", "Data berhasil disimpan");
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("task/daftar_kasi_tglttd/" . $result['group_alias'] . "/" . $registrasi_id);
    }

    // pending process
    public function pending_process($registrasi_id = "", $process_id = "") {
		
		// set page rules
        $this->_set_page_rule("U");
        // get task
        $detail = $this->m_task->get_registrasi_waiting_by_id(array($registrasi_id, $this->com_user['role_id'], $this->com_user['pelabuhan_id']));
        if (empty($detail)) {
            redirect('task/akta_pendaftaran');
        }
        // action
        $action = $this->m_task->get_action_control(array($this->com_user['role_id'], $detail['daftar_group']));
        if ($action['action_revisi'] == '0') {
            redirect('task/akta_pendaftaran');
        }
        // catatan
/*        if (empty($detail['catatan'])) {
            // default error
            $this->tnotification->sent_notification("error", "Catatan kepada Pemohon belum diisikan!");
            // default redirect
            redirect("task/daftar_kasi_tglttd/" . $detail['group_alias'] . "/" . $registrasi_id);
        }*/
        // update
        $params = array('reject', 'done', $this->com_user['user_id'], $process_id);
        if ($this->m_task->action_update($params)) {
            $next_flow = 7; // member flow;
            $process_id = $this->m_task->get_process_id();
            // process flow
            $params = array($process_id, $registrasi_id, $next_flow, $this->com_user['user_id']);
            $this->m_task->insert_process($params);
            // // send mail pending
            $this->m_email->mail_daftar_pending_tgl_ttd($registrasi_id, $this->com_user['role_id'], $this->com_user['pelabuhan_id']);
            // success
            $this->tnotification->delete_last_field();
            $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            // default redirect
            redirect("task/akta_pendaftaran");
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("task/daftar_kasubdit_tglttd/" . $detail['group_alias'] . "/" . $registrasi_id);
    }

    // send process
    public function send_process($registrasi_id = "", $process_id = "") {
        // set page rules
        $this->_set_page_rule("U");
        // get task
//var_dump(array($registrasi_id, $this->com_user['role_id'], $this->com_user['user_id'])); exit;		
        $detail = $this->m_task->get_registrasi_waiting_by_id_user(array($registrasi_id, $this->com_user['role_id'], $this->com_user['user_id']));
        if (empty($detail)) {
            redirect('member/jadwal_ttd');
        }
        // action
        $action = $this->m_task->get_action_control(array($this->com_user['role_id'], $detail['daftar_group']));
        if ($action['action_send'] == '0') {
            redirect('member/jadwal_ttd');
        }

	if($action = $this->input->post('save')=="Jadwal Ulang"){
		$tanggal = $detail['tgl_ttd_akta'];
        // default redirect
        redirect("member/jadwal_ttd/reschedule/" . $registrasi_id);
	}
	else{
        // update
        $params = array('approve', 'done', $this->com_user['user_id'], $process_id);
        if ($this->m_task->action_update($params)) {
            $next_flow = 10;
            $process_id = $this->m_task->get_process_id();
            // process flow
            $params = array($process_id, $registrasi_id, $next_flow, $this->com_user['user_id']);
            $this->m_task->insert_process($params);
//var_dump($this->m_task->insert_process($params)); exit;			
           
            // get role next flow
            $next_role = $this->m_task->get_role_next_from(array($next_flow));
            // success
            $this->tnotification->delete_last_field();
            $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            // default redirect
            redirect("member/jadwal_ttd");
        } else {
            // // default error
             $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("member/jadwal_ttd/" . $detail['group_alias'] . "/" . $registrasi_id);
	}
		    
    }
	
    // reject process
    public function reject_process($registrasi_id = "", $process_id = "") {
        // set page rules
        $this->_set_page_rule("U");
        // get task
        $detail = $this->m_task->get_registrasi_waiting_by_id(array($registrasi_id, $this->com_user['role_id'], $this->com_user['pelabuhan_id']));
        if (empty($detail)) {
            redirect('task/akta_pendaftaran');
        }
        // action

        $action = $this->m_task->get_action_control(array($this->com_user['role_id'], $detail['izin_group']));
        if ($action['action_reject'] == '0') {
            redirect('task/akta_pendaftaran');
        }
        // update

        $params = array('reject', 'done', $this->com_user['user_id'], $process_id);
        if ($this->m_task->action_update($params)) {

            // update data izin registrasi
            $params = array(
                'daftar_completed' => '1',
                'daftar_approval' => 'rejected',
                'tgl_tanda_pendaftaran' => date('Y-m-d'),
                // 'izin_published_by' => $this->com_user['user_id'],
                // 'izin_published_role' => $this->com_user['role_id'],
                // 'payment_st' => '22',
            );
            $where = array(
                'registrasi_id' => $registrasi_id,
            );
            $this->m_task->registrasi_done_process($params, $where);
            
            // send mail reject
            $this->m_email->mail_daftar_reject($registrasi_id, $this->com_user['user_id']);
            // success
            $this->tnotification->delete_last_field();
            $this->tnotification->sent_notification("success", "Data berhasil disimpan");
            // default redirect
            redirect("task/akta_pendaftaran/");
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("task/daftar_staff_syarat/" . $detail['group_alias'] . "/" . $registrasi_id);
    }
	
    // save catatan
    public function save_catatan() {
        // set page rules
        $this->_set_page_rule("U");
        // cek input
        $this->tnotification->set_rules('registrasi_id', 'ID Pendaftaran', 'required');
        $this->tnotification->set_rules('process_id', 'ID Proses', 'required');
        $this->tnotification->set_rules('catatan', 'Catatan', 'trim|max_length[5000]');
        // params
        $registrasi_id = $this->input->post('registrasi_id');
        $process_id = $this->input->post('process_id');
        $catatan = $this->input->post('catatan');
        // process
        if ($this->tnotification->run() !== FALSE) {
            // params
            $params = array(
                "catatan" => $catatan,
            );
            $where = array(
                'registrasi_id' => $registrasi_id,
                'process_id' => $process_id,
            );
            // execute update
            if ($this->m_task->update_process($params, $where)) {
                $message = array('success' => 'Catatan berhasil disimpan!');
                echo json_encode($message);
            } else {
                $message = array('success' => 'Catatan gagal disimpan!');
                echo json_encode($message);
            }
        } else {
            $message = array('success' => 'Catatan gagal disimpan!');
            echo json_encode($message);
        }
    }
}
