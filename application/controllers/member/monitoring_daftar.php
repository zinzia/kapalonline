<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// load base class if needed
require_once( APPPATH . 'controllers/base/MemberBase.php' );

class monitoring_daftar extends ApplicationBase {

    // constructor
    public function __construct() {
        // parent constructor
        parent::__construct();
        // load model
        $this->load->model('monitoring/m_monitoring_daftar');
        // load library
        $this->load->library('tnotification');
        $this->load->library('pagination');
		$this->load->helper('download');
    }

    // index
    public function index() {
        // set page rules
        $this->_set_page_rule("R");
        // set template content
        $this->smarty->assign("template_content", "member/monitoring_daftar/index.html");
        // get search parameter
        $search = $this->tsession->userdata('search_monitoring_daftar');
        // search parameters
		
		//initialize
		
        $daftar_group = empty($search['daftar_group']) ? '%' : $search['daftar_group'];
        $registrasi_id = empty($search['registrasi_id']) ? '%' : '%' . $search['registrasi_id'] . '%';
        $nama_kapal = empty($search['nama_kapal']) ? '%' : '%' . $search['nama_kapal'] . '%';
        // assign search
        $this->smarty->assign("search", $search);
        // get list data
        $rs_id = $this->m_monitoring_daftar->get_list_my_task_waiting(array($this->com_user['user_id'], $daftar_group, $registrasi_id, $nama_kapal));
        $this->smarty->assign("rs_id", $rs_id);
        $this->smarty->assign("total", count($rs_id));
        // get all group
		$rs_group = $this->m_monitoring_daftar->get_all_group();
		$this->smarty->assign("rs_group", $rs_group);
        
		// output
        parent::display();
    }

    // proses pencarian
    public function proses_cari() {
		// set page rules
        $this->_set_page_rule("R");
        // data
        if ($this->input->post('save') == "Reset") {
            $this->tsession->unset_userdata('search_monitoring_daftar');
        } else {
            $params = array(
                "daftar_group" => $this->input->post("daftar_group"),
                "registrasi_id" => $this->input->post("registrasi_id"),
                "nama_kapal" => $this->input->post("nama_kapal"),
            );
            $this->tsession->set_userdata("search_monitoring_daftar", $params);
        }
        // redirect
        redirect("member/monitoring_daftar");
    }

    // download
    public function files_download($data_id = "", $ref_id = "") {
        // get detail data
        $params = array($data_id, $ref_id);
        $result = $this->m_monitoring_daftar->get_detail_files_by_id($params);
        if (empty($result)) {
            redirect("member/monitoring_daftar");
        }
        // filepath
        $file_path = $result['file_path'];
        if (is_file($file_path)) {
            
			$data = file_get_contents($file_path); // Read the file's contents
			$name = end(explode('/', $file_path));
			force_download($name, $data); 
			exit();
        } else {
            redirect('member/monitoring_daftar');
        }
    }

    /*
     * DISPLAY
     * 
     */

    // baru
    public function pendaftaran($registrasi_id = "") {
        // set rule
        $this->_set_page_rule("R");
        // set template content
        $this->smarty->assign("template_content", "member/monitoring_daftar/pendaftaran.html");
        // detail registrasi
        $detail = $this->m_monitoring_daftar->get_detail_registrasi_data_by_id(array($registrasi_id,$this->com_user['user_id']));
        $this->smarty->assign("detail", $detail);
        if (empty($detail)) {
            redirect('member/monitoring_daftar');
        }
       
		// list persyaratan
        $rs_files = $this->m_monitoring_daftar->get_list_file_required(array($detail['daftar_group'], $detail['bukti_hakmilik']));
        $this->smarty->assign("rs_files", $rs_files);
        // get uploaded files
        $file_uploaded = array();
        $name_uploaded = array();
        $rs_uploaded = $this->m_monitoring_daftar->get_list_file_uploaded(array($registrasi_id));
        foreach ($rs_uploaded as $uploaded) {
            $file_uploaded[] = $uploaded['ref_id'];
            $name_uploaded[$uploaded['ref_id']] = $uploaded['file_name'];
        }
        $this->smarty->assign("file_uploaded", $file_uploaded);
        $this->smarty->assign("name_uploaded", $name_uploaded);
        // display
        parent::display();
    }
}
