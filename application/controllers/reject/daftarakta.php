<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// load base class if needed
require_once( APPPATH . 'controllers/base/OperatorBase.php' );

class daftarakta extends ApplicationBase {

    private $group_id = 1;
    // constructor
    public function __construct() {
        // parent constructor
        parent::__construct();
        // load model
        $this->load->model("reject/m_rejectakta");
        // load library
        $this->load->library('pagination');
		$this->load->helper('download');
    }

    // index
    public function index() {
        // set page rules
//        $this->_set_page_rule("R");
        // set template content
        $this->smarty->assign("template_content", "reject/daftarakta/index.html");
        // get tahun
        $this->smarty->assign("rs_tahun", $this->m_rejectakta->get_list_tahun_report($this->com_user['pelabuhan_id']));
        // bulan
        $bulan = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        );
        $this->smarty->assign("rs_bulan", $bulan);
        // get search parameter
        $search = $this->tsession->userdata('search_reject');
        // search parameters
        $bulan = empty($search['bulan']) ? date('m') : $search['bulan'];
        $tahun = empty($search['tahun']) ? date('Y') : $search['tahun'];
        $daftar_group = empty($search['daftar_group']) ? '%' : $search['daftar_group'];
        // default
        $search['bulan'] = empty($search['bulan']) ? date('m') : $search['bulan'];
        $search['tahun'] = empty($search['tahun']) ? date('Y') : $search['bulan'];
        // assign
        $this->smarty->assign("search", $search);
        /* start of pagination --------------------- */
        // pagination
        $config['base_url'] = site_url("reject/daftarakta/index/");
        $config['total_rows'] = $this->m_rejectakta->get_total_finished_daftar_registrasi(array($this->com_user['pelabuhan_id'], $tahun, $bulan, $daftar_group));
        $config['uri_segment'] = 4;
        $config['per_page'] = 50;
        $this->pagination->initialize($config);
        $pagination['data'] = $this->pagination->create_links();
        // pagination attribute
        $start = $this->uri->segment(4, 0) + 1;
        $end = $this->uri->segment(4, 0) + $config['per_page'];
        $end = (($end > $config['total_rows']) ? $config['total_rows'] : $end);
        $pagination['start'] = ($config['total_rows'] == 0) ? 0 : $start;
        $pagination['end'] = $end;
        $pagination['total'] = $config['total_rows'];
        // pagination assign value
        $this->smarty->assign("pagination", $pagination);
        $this->smarty->assign("no", $start);
        /* end of pagination ---------------------- */
        // get list
        $params = array($this->com_user['pelabuhan_id'], $tahun, $bulan, $daftar_group, ($start - 1), $config['per_page']);
        $rs_id = $this->m_rejectakta->get_list_finished_daftar_registrasi($params);
		$rs_group = $this->m_rejectakta->get_all_group();
        $this->smarty->assign("rs_id", $rs_id);
        $this->smarty->assign("rs_group", $rs_group);

        // output
        parent::display();
    }

    // proses pencarian
    public function proses_cari() {
        // set page rules
        $this->_set_page_rule("R");
        // data
        if ($this->input->post('save') == "Reset") {
            $this->tsession->unset_userdata('search_reject');
        } else {
            $params = array(
                "bulan" => $this->input->post("bulan"),
                "tahun" => $this->input->post("tahun"),
                "daftar_group" => $this->input->post("daftar_group"),
            );
            $this->tsession->set_userdata("search_reject", $params);
        }
        // redirect
        redirect("reject/daftarakta");
    }

    /*
     * BARU
     */

    public function pendaftaran($registrasi_id = "") {
        // set rule
        $this->_set_page_rule("R");
        // set template content
        $this->smarty->assign("template_content", "reject/daftarakta/pendaftaran.html");
        // detail registrasi
        $detail = $this->m_rejectakta->get_detail_registrasi_data_by_id(array($registrasi_id,$this->com_user['pelabuhan_id']));
//var_dump(array($registrasi_id,$this->com_user['pelabuhan_id'])); exit;		
        $this->smarty->assign("detail", $detail);
        if (empty($detail)) {
            redirect('reject/daftarakta');
        }
       
		// list persyaratan
        $rs_files = $this->m_rejectakta->get_list_file_required(array($detail['daftar_group'], $detail['bukti_hakmilik']));
        $this->smarty->assign("rs_files", $rs_files);
        // get uploaded files
        $file_uploaded = array();
        $name_uploaded = array();
        $rs_uploaded = $this->m_rejectakta->get_list_file_uploaded(array($registrasi_id));
        foreach ($rs_uploaded as $uploaded) {
            $file_uploaded[] = $uploaded['ref_id'];
            $name_uploaded[$uploaded['ref_id']] = $uploaded['file_name'];
        }
        $this->smarty->assign("file_uploaded", $file_uploaded);
        $this->smarty->assign("name_uploaded", $name_uploaded);
        // display
        parent::display();
    }
	
	
    // file download
    public function files_download($data_id = "", $ref_id = "") {
        // get detail data
        $params = array($data_id, $ref_id);
		$result = $this->m_rejectakta->get_detail_files_by_id($params);
        if (empty($result)) {
            redirect("reject/daftarakta");
        }
        // filepath
        $file_path = $result['file_path'];
        if (is_file($file_path)) {
           // download
            $data = file_get_contents($file_path); // Read the file's contents
			$name = end(explode('/', $file_path));
			force_download($name, $data); 
            exit();
        } else {
            redirect('reject/daftarakta');
        }
    }
	
}
