<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// load base class if needed
require_once( APPPATH . 'controllers/base/MemberBase.php' );

// --

class editakta extends ApplicationBase {

    private $group_id = 1;
    
    // constructor
    public function __construct() {
        // parent constructor
        parent::__construct();
        // load model
        $this->load->model('pendaftaran/m_editregistrasi');
        $this->load->model('m_email');
        // load library
        $this->load->library('tnotification');
        $this->load->library('pagination');
    }

    /*
     * STEP 1 : DATA PERMOHONAN
     */

    // Form Permohonan
    public function index() {
        // set page rules
        $this->_set_page_rule("C");
        // set template content
        $this->smarty->assign("template_content", "pendaftaran_kapal/akta/index.html");
        // load javascript
        $this->smarty->load_javascript("resource/js/jquery/jquery-ui-1.9.2.custom.min.js");
        $this->smarty->load_javascript("resource/js/select2-3.4.5/select2.min.js");
        // load style ui
        $this->smarty->load_style("jquery.ui/redmond/jquery-ui-1.8.13.custom.css");
        $this->smarty->load_style("select2/select2.css");
        // get detail data
        
		// load pelabuhan
        $this->smarty->assign("rs_pelabuhan", $this->m_registrasi->get_tempat_daftar());
        
		$rs_subgroup = $this->m_registrasi->get_subgroup_by_group_id(array($this->group_id));
		$this->smarty->assign("rs_subgroup", $rs_subgroup);
		
		//initialize
		$result['pemilik_st'] = '';
		$result['bukti_hakmilik'] = '';
		$result['pelabuhan_id'] = '';
		
		$this->smarty->assign("result", $result);
        
		// notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

	// Form Permohonan
    public function data_pengajuan($registrasi_id = "") {
        // set page rules
        $this->_set_page_rule("C");
        // set template content
        $this->smarty->assign("template_content", "pendaftaran_kapal/editakta/index.html");
        // load javascript
        $this->smarty->load_javascript("resource/js/jquery/jquery-ui-1.9.2.custom.min.js");
        $this->smarty->load_javascript("resource/js/select2-3.4.5/select2.min.js");
        // load style ui
        $this->smarty->load_style("jquery.ui/redmond/jquery-ui-1.8.13.custom.css");
        $this->smarty->load_style("select2/select2.css");
        // get detail data
        
		// load pelabuhan
        $this->smarty->assign("rs_pelabuhan", $this->m_editregistrasi->get_tempat_daftar());
        
		$rs_subgroup = $this->m_editregistrasi->get_subgroup_by_group_id(array($this->group_id));
		$this->smarty->assign("rs_subgroup", $rs_subgroup);
		
		$result = $this->m_editregistrasi->get_daftar_registrasi($registrasi_id, $this->com_user['user_id'], $this->group_id);
		if (empty($result)) {
            redirect('member/pending_daftar');
        }
		
		$this->smarty->assign("result", $result);
        
		// notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }
	
	// Step 1 : Process
    public function add_process() {
        // set page rules
        $this->_set_page_rule("U");
        // cek input
        // $this->tnotification->set_rules('registrasi_id', 'ID', 'trim|required');
        $this->tnotification->set_rules('tgl_surat', 'Tanggal Surat Permohonan', 'trim|required');
        $this->tnotification->set_rules('pelabuhan_id', 'Tempat Pendaftaran', 'trim|required');
        $this->tnotification->set_rules('nama_pemohon', 'Nama Pemohon', 'trim|required|maxlength[30]');
        $this->tnotification->set_rules('pemilik_st', 'Pilihan Pemohon', 'trim|required');
        $this->tnotification->set_rules('bukti_hakmilik', 'Pilihan Bukti Hak Milik', 'trim|required');
        $this->tnotification->set_rules('no_surat', 'No Surat', 'trim');
        // process
        if ($this->tnotification->run() !== FALSE) {
			
				// data
				$params = array(
					"tgl_surat" => $this->input->post('tgl_surat'),
					"no_surat" => $this->input->post('no_surat'),
					"nama_pemohon" => $this->input->post('nama_pemohon'),
					"pemilik_st" => $this->input->post('pemilik_st'),
					"bukti_hakmilik" => $this->input->post('bukti_hakmilik'),
					"pelabuhan_id" => $this->input->post('pelabuhan_id'),
					"daftar_group" => $this->group_id,
					"request_by" => $this->com_user['user_id'],
					"mdb" => $this->com_user['user_id']
				);
			
			if($this->input->post('registrasi_id')){
				// update data
				
				$where = array("registrasi_id" => $this->input->post('registrasi_id'));
				
				$registrasi_id = $this->m_registrasi->update_daftar_registrasi($params, $where);
				// insert
				if ($registrasi_id) {
					// success
					// default redirect
					redirect("pendaftaran_kapal/akta/data_kapal/" . $this->input->post('registrasi_id') );
				} else {
					// default error
					$this->tnotification->sent_notification("error", "Data gagal disimpan");
				}
			
			exit;		
			}else {
				// insert data
				
				$registrasi_id = $this->m_registrasi->create_group_data($params);
				// insert
				if ($registrasi_id) {
					// success
					// default redirect
					redirect("pendaftaran_kapal/akta/data_kapal/" . $registrasi_id );
				} else {
					// default error
					$this->tnotification->sent_notification("error", "Data gagal disimpan");
				}
			}
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pendaftaran_kapal/akta/index/" . $this->input->post('registrasi_id'));
    }
	
	 public function data_kapal($registrasi_id = "") {
		// set page rules
        $this->_set_page_rule("C");
        // set template content
        $this->smarty->assign("template_content", "pendaftaran_kapal/akta/data_kapal.html");
        // load javascript
        $this->smarty->load_javascript("resource/js/jquery/jquery-ui-1.9.2.custom.min.js");
        $this->smarty->load_javascript("resource/js/select2-3.4.5/select2.min.js");
        // load style ui
        $this->smarty->load_style("jquery.ui/redmond/jquery-ui-1.8.13.custom.css");
        $this->smarty->load_style("select2/select2.css");
        // get detail data
        
		// load jenis kapal
        $this->smarty->assign("rs_jeniskapal", $this->m_registrasi->get_jenis_kapal());
        // load bendera asal
        $this->smarty->assign("rs_benderaasal", $this->m_registrasi->get_bendera_asal());
		// load tempat terbit
        $this->smarty->assign("rs_tempatterbit", $this->m_registrasi->get_tempat_terbit());
		// load bahan kapal
        $this->smarty->assign("rs_bahankapal", $this->m_registrasi->get_bahan_kapal());
		// load penggerak kapal
        $this->smarty->assign("rs_penggerakkapal", $this->m_registrasi->get_penggerak_kapal());
        
		$rs_subgroup = $this->m_registrasi->get_subgroup_by_group_id(array($this->group_id));
		$this->smarty->assign("rs_subgroup", $rs_subgroup);
		
		$cekregistrasi = $this->m_registrasi->get_daftar_registrasi($registrasi_id, $this->com_user['user_id'], $this->group_id);
		if (empty($cekregistrasi)) {
            redirect('member/akta_pendaftaran');
        }
		
		$result = $this->m_registrasi->get_daftar_baru($registrasi_id);
		
		// initialize
		if(empty($result)){
			$result['satuan_mesin1'] = '';
			$result['satuan_mesin2'] = '';
			$result['satuan_mesin3'] = '';
			$result['satuan_mesin4'] = '';
			$result['jenisdetail_kapal'] = '';
			$result['bendera_asal'] = '';
			$result['tempat_terbit'] = '';
			$result['bahan_kapal'] = '';
			$result['penggerak_kapal'] = '';
		}
		
		$result['registrasi_id'] = $registrasi_id;
		
		$this->smarty->assign("result", $result);
        
		// notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }
	
		// Step 1 : Process
    public function data_kapal_process() {
        // set page rules
        $this->_set_page_rule("U");
        // cek input
        $this->tnotification->set_rules('registrasi_id', 'ID', 'trim|required');
        $this->tnotification->set_rules('nama_kapal', 'Nama Kapal', 'trim|required|maxlength[50]');
        $this->tnotification->set_rules('jenisdetail_kapal', 'Jenis Kapal', 'trim|required');
        $this->tnotification->set_rules('tempat_terbit', 'Tempat Terbit', 'trim|required');
        $this->tnotification->set_rules('tgl_surat_ukur', 'Tanggal Surat Ukur', 'trim|required');
        $this->tnotification->set_rules('no_surat_ukur', 'No Surat Ukur', 'trim|required');
        $this->tnotification->set_rules('dimensi_panjang', 'Dimensi Panjang', 'trim|required|maxlength[10]');
        $this->tnotification->set_rules('dimensi_lebar', 'Dimensi Lebar', 'trim|required|maxlength[10]');
        $this->tnotification->set_rules('dimensi_loa', 'Dimensi LOA', 'trim|required|maxlength[10]');
        $this->tnotification->set_rules('dimensi_isikotor', '(GT) Isi Kotor', 'trim|required|maxlength[10]');
        $this->tnotification->set_rules('dimensi_isibersih', '(NT) Isi Bersih', 'trim|required|maxlength[10]');
        $this->tnotification->set_rules('tanda_selar', 'Tanda Selar', 'trim|required|maxlength[30]');
        $this->tnotification->set_rules('bahan_kapal', 'Bahan Utama Kapal', 'trim|required');
        $this->tnotification->set_rules('jml_geladak', 'Jumlah Geladak', 'trim|required|maxlength[20]');
        $this->tnotification->set_rules('satuan_mesin1', 'Satuan Mesin 1', 'trim');
        $this->tnotification->set_rules('satuan_mesin2', 'Satuan Mesin 2', 'trim');
        $this->tnotification->set_rules('satuan_mesin3', 'Satuan Mesin 3', 'trim');
        $this->tnotification->set_rules('satuan_mesin4', 'Satuan Mesin 4', 'trim');
        $this->tnotification->set_rules('bendera_asal', 'Bendera Asal', 'trim');
        $this->tnotification->set_rules('penggerak_kapal', 'Penggerak Kapal', 'trim');
        
		// process
        if ($this->tnotification->run() !== FALSE) {
            // data
            $params = array(
                "registrasi_id" => $this->input->post('registrasi_id'),
                "nama_kapal" => $this->input->post('nama_kapal'),
                "eks_nama_kapal" => $this->input->post('eks_nama_kapal'),
                "jenisdetail_kapal" => $this->input->post('jenisdetail_kapal'),
                "nomor_imo" => $this->input->post('nomor_imo'),
                "call_sign" => $this->input->post('call_sign'),
                "no_csr" => $this->input->post('no_csr'),
                "harga_kapal" => $this->input->post('harga_kapal'),
                "bendera_asal" => $this->input->post('bendera_asal'),
                "tempat_terbit" => $this->input->post('tempat_terbit'),
                "tempat_pembuatan" => $this->input->post('tempat_pembuatan'),
                "tgl_surat_ukur" => $this->input->post('tgl_surat_ukur'),
                "tgl_peletakan_lunas" => $this->input->post('tgl_peletakan_lunas'),
                "no_surat_ukur" => $this->input->post('no_surat_ukur'),
                "dimensi_panjang" => $this->input->post('dimensi_panjang'),
                "dimensi_lebar" => $this->input->post('dimensi_lebar'),
                "dimensi_dalam" => $this->input->post('dimensi_dalam'),
                "dimensi_loa" => $this->input->post('dimensi_loa'),
                "dimensi_isikotor" => $this->input->post('dimensi_isikotor'),
                "dimensi_isibersih" => $this->input->post('dimensi_isibersih'),
                "tanda_selar" => $this->input->post('tanda_selar'),
                "bahan_kapal" => $this->input->post('bahan_kapal'),
                "jml_geladak" => $this->input->post('jml_geladak'),
                "jml_cerobong" => $this->input->post('jml_cerobong'),
                "jml_baling" => $this->input->post('jml_baling'),
                "penggerak_kapal" => $this->input->post('penggerak_kapal'),
                "mesin_merk" => $this->input->post('mesin_merk'),
                "putaran_mesin" => $this->input->post('putaran_mesin'),
                "type_mesin" => $this->input->post('type_mesin'),
                "serial_no_mesin" => $this->input->post('serial_no_mesin'),
                "daya_mesin1" => $this->input->post('daya_mesin1'),
                "satuan_mesin1" => $this->input->post('satuan_mesin1'),
                "daya_mesin2" => $this->input->post('daya_mesin2'),
                "satuan_mesin2" => $this->input->post('satuan_mesin2'),
                "daya_mesin3" => $this->input->post('daya_mesin3'),
                "satuan_mesin3" => $this->input->post('satuan_mesin3'),
                "daya_mesin4" => $this->input->post('daya_mesin4'),
                "satuan_mesin4" => $this->input->post('satuan_mesin4'),
                "mdb" => $this->com_user['user_id'],
                "mdd" => date('Y-m-d H:i:s')
            );
			
			$result = $this->m_registrasi->get_daftar_baru($this->input->post('registrasi_id'));
		
			// initialize
			if(empty($result)){
		
				// insert
				if ($this->m_registrasi->insert_daftar_baru($params)) {
					// sukses
					redirect("pendaftaran_kapal/akta/data_pemilik/" . $this->input->post('registrasi_id') );
				} else {
					// default error
					$this->tnotification->sent_notification("error", "Data gagal disimpan");
				}
			} else{
				// update
				$where = array("registrasi_id" => $this->input->post('registrasi_id'));
				
				if ($this->m_registrasi->update_daftar_baru($params, $where)) {
					
					// sukses
					redirect("pendaftaran_kapal/akta/data_pemilik/" . $this->input->post('registrasi_id') );
				} else {
					// default error
					$this->tnotification->sent_notification("error", "Data gagal disimpan");
				}
				
			}
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pendaftaran_kapal/akta/data_kapal/" . $this->input->post('registrasi_id'));
    }

	public function data_pemilik($registrasi_id = "") {
		// set page rules
        $this->_set_page_rule("C");
        // set template content
        $this->smarty->assign("template_content", "pendaftaran_kapal/akta/data_pemilik.html");
        // load javascript
        $this->smarty->load_javascript("resource/js/jquery/jquery-ui-1.9.2.custom.min.js");
        $this->smarty->load_javascript("resource/js/select2-3.4.5/select2.min.js");
        // load style ui
        $this->smarty->load_style("jquery.ui/redmond/jquery-ui-1.8.13.custom.css");
        $this->smarty->load_style("select2/select2.css");
        // get detail data
        		
		$cekregistrasi = $this->m_registrasi->get_daftar_registrasi($registrasi_id, $this->com_user['user_id'], $this->group_id);
		if (empty($cekregistrasi)) {
            redirect('member/akta_pendaftaran');
        }
		
		$result = $this->m_registrasi->get_daftar_baru($registrasi_id);
		
		$result['registrasi_id'] = $registrasi_id;
		
		$this->smarty->assign("result", $result);
        
		// notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }
	

		// Step 1 : Process
    public function data_pemilik_process() {
        // set page rules
        $this->_set_page_rule("U");
        // cek input
        $this->tnotification->set_rules('registrasi_id', 'ID', 'trim|required');
        $this->tnotification->set_rules('nama_pemilik', 'Nama Pemilik', 'trim|required|maxlength[50]');
        $this->tnotification->set_rules('alamat_pemilik', 'Alamat Pemilik', 'trim|required|maxlength[100]');
        $this->tnotification->set_rules('npwp', 'NPWP', 'trim|required|maxlength[30]');
        $this->tnotification->set_rules('kota', 'Kota', 'trim|required|maxlength[30]');
        
		// process
        if ($this->tnotification->run() !== FALSE) {
            // data
            $params = array(
                "registrasi_id" => $this->input->post('registrasi_id'),
                "nama_pemilik" => $this->input->post('nama_pemilik'),
                "alamat_pemilik" => $this->input->post('alamat_pemilik'),
                "npwp" => $this->input->post('npwp'),
                "kota" => $this->input->post('kota'),
                "mdb" => $this->com_user['user_id'],
                "mdd" => date('Y-m-d H:i:s')
            );
			
			$where = array("registrasi_id" => $this->input->post('registrasi_id'));
			
				if ($this->m_registrasi->update_data_pemilik($params, $where)) {
					// sukses
					redirect("pendaftaran_kapal/akta/list_files/" . $this->input->post('registrasi_id') );
				} else {
					// default error
					$this->tnotification->sent_notification("error", "Data gagal disimpan");
				}
				
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pendaftaran_kapal/akta/data_pemilik/" . $this->input->post('registrasi_id'));
    }

    /*
     * STEP 4 : FILE ATTACHMENT
     */

    // files attachment
    public function list_files($registrasi_id = "") {
        // set page rules
        $this->_set_page_rule("C");
        // set template content
        $this->smarty->assign("template_content", "pendaftaran_kapal/akta/files.html");
        // get detail data
        $result = $this->m_registrasi->get_daftar_registrasi($registrasi_id, $this->com_user['user_id'], $this->group_id);
		if (empty($result)) {
            redirect('member/akta_pendaftaran');
        }
		
        $this->smarty->assign("result", $result);
        // list persyaratan
        $rs_files = $this->m_registrasi->get_list_file_required(array($result['daftar_group'], $result['bukti_hakmilik']));
        $this->smarty->assign("rs_files", $rs_files);
        // get uploaded files
        $file_uploaded = array();
        $name_uploaded = array();
        $rs_uploaded = $this->m_registrasi->get_list_file_uploaded(array($registrasi_id));
        foreach ($rs_uploaded as $uploaded) {
            $file_uploaded[] = $uploaded['ref_id'];
            $name_uploaded[$uploaded['ref_id']] = $uploaded['file_name'];
        }
        $this->smarty->assign("file_uploaded", $file_uploaded);
        $this->smarty->assign("name_uploaded", $name_uploaded);
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // file process
    public function files_process() {
        // set page rules
        $this->_set_page_rule("C");
        // load
        $this->load->library('tupload');
        // cek input
        $this->tnotification->set_rules('registrasi_id', 'ID', 'trim|required');
        // registrasi id
        $registrasi_id = $this->input->post('registrasi_id');
        // get detail data
        $result = $this->m_registrasi->get_daftar_registrasi($registrasi_id, $this->com_user['user_id'], $this->group_id);
		if (empty($result)) {
            redirect('member/akta_pendaftaran');
        }
		// process
        if ($this->tnotification->run() !== FALSE) {
            // upload 1 per 1
            $rs_files = $this->m_registrasi->get_list_file_required(array($result['daftar_group'], $result['bukti_hakmilik']));
            foreach ($rs_files as $k => $files) {
                $file = $_FILES[$files['ref_field']];
                // upload 1 per 1
                if (!empty($file['tmp_name'])) {
                    // upload config
                    $config['upload_path'] = 'resource/doc/daftar/' . $registrasi_id . '/' . $files['ref_id'];
                    $config['allowed_types'] = $files['ref_allowed'];
                    $config['max_size'] = $files['ref_size'];
                    $this->tupload->initialize($config);
                    // process upload
                    if ($this->tupload->do_upload($files['ref_field'])) {
                        // jika berhasil
                        $data = $this->tupload->data();
                        // update
                        $file_id = $this->m_registrasi->get_data_id() + $k;
                        $filepath = 'resource/doc/daftar/' . $registrasi_id . '/' . $files['ref_id'] . '/' . $data['file_name'];
                        $this->m_registrasi->update_files(array($registrasi_id, $files['ref_id']), array($file_id, $registrasi_id, $filepath, $data['file_name'], $files['ref_id']));
                        // notification
                        $this->tnotification->delete_last_field();
                        $this->tnotification->sent_notification("success", "Data berhasil disimpan");
                    } else {
                        // jika gagal
                        $this->tnotification->set_error_message($this->tupload->display_errors());
                    }
                }
            }
            // notification
            $this->tnotification->delete_last_field();
            $this->tnotification->sent_notification("success", "Data berhasil disimpan");
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pendaftaran_kapal/akta/list_files/" . $registrasi_id);
    }

    // download
    public function files_download($data_id = "", $ref_id = "") {
        // get detail data
        $params = array($data_id, $ref_id);
        $result = $this->m_registrasi->get_detail_files_by_id($params);
        if (empty($result)) {
            redirect("pendaftaran_kapal/akta/list_files/" . $data_id);
        }
        // filepath
        $file_path = $result['file_path'];
        if (is_file($file_path)) {
            // download
            header('Content-Description: Download Files');
            header('Content-Type: application/octet-stream');
            header('Content-Length: ' . filesize($file_path));
            header('Content-Disposition: attachment;
            filename = "' . end(explode('/', $file_path)) . '"');
            readfile($file_path);
            exit();
        } else {
            redirect('member/akta_pendaftaran');
        }
    }

    // file process
    public function files_next() {
        // cek input
        $this->tnotification->set_rules('registrasi_id', 'ID', 'trim|required');
        // data id
        $registrasi_id = $this->input->post('registrasi_id');
        // get detail data
        $result = $this->m_registrasi->get_daftar_registrasi($registrasi_id, $this->com_user['user_id'], $this->group_id);
		if (empty($result)) {
            redirect('member/akta_pendaftaran');
        }
		// validation
        if (!$this->m_registrasi->is_file_completed(array($registrasi_id, $result['daftar_group'], $result['bukti_hakmilik']))) {
            $this->tnotification->set_error_message('File persyaratan belum diupload!');
        }
        // process
        if ($this->tnotification->run() !== FALSE) {
            // default redirect
            redirect("pendaftaran_kapal/akta/review/" . $registrasi_id);
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pendaftaran_kapal/akta/list_files/" . $registrasi_id);
    }

    /*
     * STEP 5 : REVIEW PERMOHONAN
     */

    // review
    public function review($registrasi_id = "") {
		// exit;
        // set rules
        $this->_set_page_rule('R');
        // set template content
        $this->smarty->assign("template_content", "pendaftaran_kapal/akta/review.html");
        // get detail data
        $result = $this->m_registrasi->get_detail_registrasi($registrasi_id, $this->com_user['user_id'], $this->group_id);
		if (empty($result)) {
            redirect('member/akta_pendaftaran');
        }
		
		$this->smarty->assign("result", $result);
		
		$rs_subgroup = $this->m_registrasi->get_subgroup_by_group_id(array($this->group_id));
		$this->smarty->assign("rs_subgroup", $rs_subgroup);
		
        // list rute
        $data = array();
        $pairing = array();
        $frekuensi = array();
        // $rs_id = $this->m_registrasi->get_list_izin_rute_waiting_by_id(array($registrasi_id, $this->com_user['airlines_id']));
        // $this->smarty->assign("rs_id", $data);
        
		// list persyaratan
        $rs_files = $this->m_registrasi->get_list_file_required(array($result['daftar_group'], $result['bukti_hakmilik']));
        $this->smarty->assign("rs_files", $rs_files);
        // get uploaded files
        $file_uploaded = array();
        $rs_uploaded = $this->m_registrasi->get_list_file_uploaded(array($registrasi_id));
        foreach ($rs_uploaded as $uploaded) {
            $file_uploaded[] = $uploaded['ref_id'];
            $name_uploaded[$uploaded['ref_id']] = $uploaded['file_name'];
        }
        $this->smarty->assign("file_uploaded", $file_uploaded);
        $this->smarty->assign("name_uploaded", $name_uploaded);
        // notification
        $this->tnotification->display_notification();
        $this->tnotification->display_last_field();
        // output
        parent::display();
    }

    // send process
    public function send_process() {
        // cek input
        $this->tnotification->set_rules('registrasi_id', 'ID', 'trim|required');
        // data id
        $registrasi_id = $this->input->post('registrasi_id');
        // get detail data
        $result = $this->m_registrasi->get_daftar_registrasi($registrasi_id, $this->com_user['user_id'], $this->group_id);
		if (empty($result)) {
            redirect('member/akta_pendaftaran');
        }
		// validation
        if (!$this->m_registrasi->is_file_completed(array($registrasi_id, $result['daftar_group'], $result['bukti_hakmilik']))) {
            $this->tnotification->set_error_message('File persyaratan belum diupload!');
        }
        // process
        if ($this->tnotification->run() !== FALSE) {
            // process flow
            $process_id = $this->m_registrasi->get_data_id();
            $params = array(
                'process_id' => $process_id,
                'registrasi_id' => $registrasi_id,
                'flow_id' => 1,
                'mdb' => $this->com_user['user_id'],
                'mdd' => date('Y-m-d H:i:s'),
            );
            $this->m_registrasi->insert_daftar_process($params);
            // update status
            $params = array(
                'daftar_request_st' => '1',
                'tgl_pengajuan' => date('Y-m-d H:i:s'),
            );
            $where = array(
                'registrasi_id' => $registrasi_id,
                'request_by' => $this->com_user['user_id'],
            );
            $this->m_registrasi->update_daftar_registrasi($params, $where);
            
			// send mail
			$this->m_email->mail_pengajuan_to_staff($result['registrasi_id'], $result['pelabuhan_id']);
           
			// notification
            $this->tnotification->delete_last_field();
            $this->tnotification->sent_notification("success", "Terima kasih telah melakukan pengajuan Akta Pendaftaran Kapal melalui sistem pelayanan berbasis online.");
            // default redirect
            redirect("member/akta_pendaftaran");
        } else {
            // default error
            $this->tnotification->sent_notification("error", "Data gagal disimpan");
        }
        // default redirect
        redirect("pendaftaran_kapal/akta/review/" . $registrasi_id);
    }

}
