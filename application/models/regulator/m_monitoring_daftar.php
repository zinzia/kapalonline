<?php

class m_monitoring_daftar extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function get_list_file_required($params) {
        $sql = "SELECT a.* FROM daftar_file_references a
                INNER JOIN daftar_rules_files b ON a.ref_id = b.ref_id
                WHERE b.group_id = ? AND b.subgroup_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
	// get list group request
    function get_all_group() {
        $sql = "SELECT * FROM daftar_group WHERE group_st = 'show' ORDER BY group_id ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    // get list waiting
    function get_list_my_task_waiting($params) {
        $sql = "SELECT c.registrasi_id, c.daftar_flight, c.daftar_rute_start, c.daftar_rute_end,
                c.daftar_request_letter, c.daftar_request_letter_date, c.daftar_request_date,
                group_nm, airlines_nm, task_link, operator_name,
                DATEDIFF(CURDATE(), a.mdd)'selisih_hari', 
                TIMEDIFF(CURTIME(), SUBSTR(a.mdd, 12, 8))'selisih_waktu', 
                group_alias
                FROM daftar_process a
                INNER JOIN daftar_flow b ON a.flow_id = b.flow_id
                INNER JOIN daftar_registrasi c ON a.registrasi_id = c.registrasi_id
                INNER JOIN daftar_rute r ON c.registrasi_id = r.registrasi_id
                INNER JOIN daftar_group d ON c.daftar_group = d.group_id
                INNER JOIN airlines e ON c.airlines_id = e.airlines_id
                LEFT JOIN com_user u ON c.daftar_request_by = u.user_id
                WHERE action_st = 'process' AND c.daftar_completed = '0' AND c.daftar_request_st = '1' 
                AND c.airlines_id = ? AND c.daftar_request_letter LIKE ?
                AND c.daftar_flight LIKE ? AND r.daftar_st LIKE ?
                AND input_by = 'member'
                GROUP BY c.registrasi_id
                ORDER BY c.daftar_request_date ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get detail registrasi by id
    function get_detail_registrasi_data_by_id($params) {
        $sql = "SELECT a.*, l.flow_id, c.*,b.pelabuhan_nm as pendaftaran_nm, b.pelabuhan_kd as pendaftaran_kd, c.*, d.jenisdetail_ket, e.jenis_ket, 
				f.negara_nm, g.pelabuhan_nm as penerbitan_nm, g.pelabuhan_kd as penerbitan_kd, h.bahan_ket, i.penggerak_ket, j.subgroup_nm,
                task_nm, k.mdd'tanggal_proses', m.group_nm, u.operator_name'pengirim'
                FROM daftar_registrasi a
                INNER JOIN daftar_process k ON a.registrasi_id = k.registrasi_id
                INNER JOIN daftar_flow l ON k.flow_id = l.flow_id
                INNER JOIN daftar_group m ON a.daftar_group = m.group_id
				LEFT JOIN com_user u ON a.request_by = u.user_id
				LEFT JOIN pelabuhan b on b.pelabuhan_id = a.pelabuhan_id and b.st_daftar = '1'
				LEFT JOIN daftar_baru c on c.registrasi_id = a.registrasi_id
				LEFT JOIN daftar_jenisdetail_kapal d on d.jenisdetail_id = c.jenisdetail_kapal
				LEFT JOIN daftar_jenis_kapal e on e.jenis_id = d.jenis_id
				LEFT JOIN negara f on f.negara_id = c.bendera_asal
				LEFT JOIN pelabuhan g on g.pelabuhan_id = c.tempat_terbit and g.st_ukur = '1'
				LEFT JOIN daftar_bahan_kapal h on h.bahan_id = c.bahan_kapal
				LEFT JOIN daftar_penggerak_kapal i on i.penggerak_id = c.penggerak_kapal
				LEFT JOIN daftar_subgroup j on j.subgroup_id = a.bukti_hakmilik
				WHERE a.registrasi_id = ? AND a.pelabuhan_id = ? AND daftar_request_st = '1'  AND k.action_st = 'process'
                GROUP BY a.registrasi_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar rute by id
    function get_list_daftar_rute_by_id($params) {
        $sql = "SELECT daftar_id, airlines_id, kode_daftar, kode_frekuensi, registrasi_id, 
                daftar_completed, daftar_approval, daftar_type, daftar_flight, daftar_st, daftar_start_date, daftar_expired_date, 
                daftar_rute_start, daftar_rute_end, pairing 
                FROM daftar_rute a
                WHERE a.registrasi_id = ? AND a.airlines_id = ?
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar data by id
    function get_list_daftar_data_by_id($params) {
        $sql = "SELECT rute_id, a.daftar_id, rute_all, tipe, capacity, 
                IF(LENGTH(TRIM(flight_no)) > 4, TRIM(flight_no), CONCAT(airlines_iata_cd, TRIM(flight_no)))'flight_no', 
                etd, eta, doop, roon, 
                start_date, end_date, a.is_used_score, GET_FREKUENSI_FROM_DOS(doop)'frekuensi',
                kode_daftar, kode_frekuensi, b.daftar_st
                FROM daftar_data a
                INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                INNER JOIN airlines c ON b.airlines_id = c.airlines_id
                WHERE a.daftar_id = ?
                ORDER BY rute_all ASC, etd ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi by registrasi
    function get_total_frekuensi_by_registrasi_id($params) {
        $sql = "SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', SUM(frekuensi)'frekuensi' 
                FROM 
                (
                        SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.registrasi_id = ?
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi by daftar_id
    function get_total_frekuensi_by_daftar_id($params) {
        $sql = "SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(daftar_id, flight_no))'frekuensi'
                FROM daftar_data a
                WHERE daftar_id = ?
                GROUP BY a.daftar_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get detail airport
    function get_airport_score_by_code($params) {
        $sql = "SELECT airport_iata_cd, is_used_score, airport_utc_sign, airport_utc FROM airport WHERE airport_iata_cd = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list files references
    function get_list_file_required_domestik($params) {
        $sql = "SELECT a.* FROM daftar_file_references a
                INNER JOIN daftar_rules_files b ON a.ref_id = b.ref_id
                WHERE b.group_id = ? AND b.data_flight = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list files references
    function get_list_file_required_internasional($params) {
        $sql = "SELECT a.* FROM daftar_file_references a
                INNER JOIN daftar_rules_files b ON a.ref_id = b.ref_id
                WHERE b.group_id = ? AND b.data_flight = ? AND b.airlines_st = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list files uploaded
    function get_list_file_uploaded($params) {
        $sql = "SELECT * FROM daftar_files WHERE registrasi_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get detail files  by id
    function get_detail_files_by_id($params) {
        $sql = "SELECT * FROM daftar_files WHERE registrasi_id = ? AND ref_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list slot by id
    function get_list_data_slot_by_id($params) {
        $sql = "SELECT a.* FROM daftar_slot_time a
                INNER JOIN daftar_registrasi b ON a.registrasi_id = b.registrasi_id
                WHERE a.registrasi_id = ? AND airlines_id = ?";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get detail slot  by id
    function get_detail_slot_by_id($params) {
        $sql = "SELECT * FROM daftar_slot_time WHERE registrasi_id = ? AND slot_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list rute by id
    function get_list_data_rute_by_id($params) {
        $sql = "SELECT a.*, kode_daftar, kode_frekuensi, daftar_approval,
                (
                IF(SUBSTRING(b.dos, 1, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 2, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 3, 1) = 0, 0, 1) +
                IF(SUBSTRING(b.dos, 4, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 5, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 6, 1) = 0, 0, 1) +
                IF(SUBSTRING(b.dos, 7, 1) = 0, 0, 1)
                )'frekuensi', 
                aircraft_type, aircraft_capacity, dos, ron, pairing, daftar_start_date, daftar_expired_date, 
                daftar_penundaan_start, daftar_penundaan_end
                FROM daftar_data a
                INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                WHERE b.registrasi_id = ? AND airlines_id = ? AND b.daftar_completed = '0'";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get_rute_by_kode_daftar
    function get_rute_by_kode_daftar($params) {
        $sql = "SELECT daftar.*, MIN(rute_all)'daftar_rute_start', MAX(rute_all)'daftar_rute_end' FROM
                (
                        SELECT a.daftar_id, kode_daftar, kode_frekuensi, a.dos,
                        SUM(
                                IF(SUBSTRING(a.dos, 1, 1) = 0, 0, 1) + IF(SUBSTRING(a.dos, 2, 1) = 0, 0, 1) + IF(SUBSTRING(a.dos, 3, 1) = 0, 0, 1) +
                                IF(SUBSTRING(a.dos, 4, 1) = 0, 0, 1) + IF(SUBSTRING(a.dos, 5, 1) = 0, 0, 1) + IF(SUBSTRING(a.dos, 6, 1) = 0, 0, 1) +
                                IF(SUBSTRING(a.dos, 7, 1) = 0, 0, 1)
                        )'frekuensi'
                        FROM daftar_rute a
                        WHERE kode_daftar = ? AND daftar_active = '1'
                        GROUP BY kode_daftar
                ) daftar
                LEFT JOIN daftar_data b ON daftar.daftar_id = b.daftar_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list waiting
    function get_total_my_task_waiting_opr($params) {
	$sql = "SELECT COUNT(*) AS 'jumlah'
                FROM daftar_process a
                INNER JOIN daftar_flow b ON a.flow_id = b.flow_id
                INNER JOIN daftar_registrasi c ON a.registrasi_id = c.registrasi_id
                INNER JOIN daftar_group d ON c.daftar_group = d.group_id
                INNER JOIN daftar_baru e ON c.registrasi_id = e.registrasi_id
                LEFT JOIN com_user u ON c.request_by = u.user_id
                WHERE action_st = 'process' AND c.daftar_completed = '0' AND c.daftar_request_st = '1' 
                AND c.daftar_group LIKE ? AND e.nama_kapal LIKE ? AND b.role_id LIKE ?
                AND c.pelabuhan_id = ?
                ORDER BY c.mdd ASC";
//var_dump($sql); exit;
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result["jumlah"];
        } else {
            return 0;
        }
    }

    function get_list_my_task_waiting_opr($params) {
        $sql = "SELECT c.*, group_nm, e.nama_kapal, task_link, operator_name,
                DATEDIFF(CURDATE(), a.mdd) AS selisih_hari, 
                TIMEDIFF(CURTIME(), SUBSTR(a.mdd, 12, 8)) AS selisih_waktu, group_alias, r.role_nm
                FROM daftar_process a
                INNER JOIN daftar_flow b ON a.flow_id = b.flow_id
                INNER JOIN daftar_registrasi c ON a.registrasi_id = c.registrasi_id
                INNER JOIN daftar_group d ON c.daftar_group = d.group_id
                INNER JOIN daftar_baru e ON c.registrasi_id = e.registrasi_id
                LEFT JOIN com_user u ON c.request_by = u.user_id
                LEFT JOIN com_role r ON b.role_id = r.role_id
                WHERE action_st = 'process' AND c.daftar_completed = '0' AND c.daftar_request_st = '1' 
				AND c.daftar_group LIKE ?
                AND c.registrasi_id LIKE ? 
				AND e.nama_kapal LIKE ? 
				AND b.role_id LIKE ?
                AND c.pelabuhan_id = ?
                ORDER BY c.tgl_pengajuan ASC";
        $query = $this->db->query($sql, $params);
//var_dump($sql); exit;		
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list rute by kode frekuensi
    function get_list_data_rute_by_kode_frekuensi($params) {
        $sql = "SELECT a.*, kode_daftar, kode_frekuensi,
                (
                IF(SUBSTRING(b.dos, 1, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 2, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 3, 1) = 0, 0, 1) +
                IF(SUBSTRING(b.dos, 4, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 5, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 6, 1) = 0, 0, 1) +
                IF(SUBSTRING(b.dos, 7, 1) = 0, 0, 1)
                )'frekuensi', 
                aircraft_type, aircraft_capacity, dos, ron, pairing, daftar_start_date, daftar_expired_date,
                daftar_penundaan_start, daftar_penundaan_end
                FROM daftar_data a
                INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                WHERE b.kode_frekuensi = ? AND airlines_id = ? AND b.daftar_active = '1'";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // total frekuensi
    function get_total_frekuensi($params) {
        $sql = "SELECT SUM(result.total)'total' 
            FROM 
            (
            SELECT a.registrasi_id, MAX(SPLIT_STRING(GET_FREKUENSI_TOTAL(b.daftar_id, b.flight_no), '/', 1))'total', b.* 
            FROM daftar_rute a 
            LEFT JOIN daftar_data b ON b.daftar_id = a.daftar_id
            WHERE a.registrasi_id = ? 
            GROUP BY a.daftar_id 
            ORDER BY b.rute_id ASC, b.etd ASC
            )result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return array();
        }
    }

    // get total frekuensi by kode_daftar
    function get_total_frekuensi_existing_by_kode_daftar($params) {
        $sql = "SELECT COUNT(daftar_id)'total_rute', MIN(start_date)'start_date', MAX(end_date)'end_date', SUM(frekuensi)'frekuensi' 
                FROM 
                (
                        SELECT b.daftar_id, MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.kode_daftar = ? AND b.daftar_completed = '1'
                        AND b.daftar_approval = 'approved'
                        AND b.daftar_active = '1'
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar rute by kode daftar
    function get_list_daftar_rute_aktif_by_kode_daftar($params) {
        $sql = "SELECT daftar_id, airlines_id, kode_daftar, kode_frekuensi, registrasi_id, 
                daftar_completed, daftar_approval, daftar_type, daftar_flight, daftar_st, daftar_start_date, daftar_expired_date, 
                daftar_rute_start, daftar_rute_end, pairing, kode_daftar, kode_frekuensi
                FROM daftar_rute a
                WHERE a.kode_daftar = ? AND a.airlines_id = ? 
                AND a.daftar_completed = '1'
                AND a.daftar_approval = 'approved'
                AND a.daftar_active = '1'
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi by registrasi by kode frekuensi
    function get_total_frekuensi_by_kode_frekuensi($params) {
        $sql = "SELECT b.kode_frekuensi, MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi', b.daftar_st
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.registrasi_id = ?
                        GROUP BY a.daftar_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi existing by kode_frekuensi
    function get_total_frekuensi_existing_by_kode_frekuensi($params) {
        $sql = " SELECT b.kode_frekuensi, MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.kode_daftar = ? AND b.daftar_completed = '1'
                        AND b.daftar_approval = 'approved'
                        AND b.daftar_active = '1'
                        GROUP BY a.daftar_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar rute by kode frekuensi
    function get_list_daftar_rute_aktif_by_kode_frekuensi($params) {
        $sql = "SELECT a.daftar_id, airlines_id, kode_daftar, kode_frekuensi, registrasi_id, 
                daftar_completed, daftar_approval, daftar_type, daftar_flight, daftar_st, daftar_start_date, daftar_expired_date, 
                daftar_rute_start, daftar_rute_end, pairing, notes,
                MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'total_frekuensi'
                FROM daftar_rute a
                INNER JOIN daftar_data b On a.daftar_id = b.daftar_id
                WHERE a.kode_frekuensi = ? AND a.airlines_id = ?
                AND a.daftar_completed = '1'
                AND a.daftar_approval = 'approved'
                AND a.daftar_active = '1'
                GROUP BY a.daftar_id
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    
    // get list files pencabutan
    function get_list_file_pencabutan_uploaded($params) {
        $sql = "SELECT * FROM daftar_file_pencabutan WHERE registrasi_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    
    // get detail files pencabutan by id
    function get_detail_files_pencabutan_by_id($params) {
        $sql = "SELECT * FROM daftar_file_pencabutan WHERE registrasi_id = ? AND letter_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /***** PERUBAHAN *****/
    // get total frekuensi by registrasi
    function get_total_frekuensi_perubahan_by_registrasi_id($params) {
        $sql = "SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', SUM(frekuensi)'frekuensi' 
                FROM 
                (
                        SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.registrasi_id = ?
                        AND b.daftar_st <> 'pencabutan'
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
}
