<?php

class m_task extends CI_Model {

    // constructor
    public function __construct() {
        parent::__construct();
    }

    // list group
    function get_list_group() {
        $sql = "SELECT group_id, group_nm FROM daftar_group WHERE group_st = 'show'";
        $query = $this->db->query($sql);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
	
	// get detail registrasi waiting by id
    function get_registrasi_waiting_by_id($params) {
        $sql = "SELECT a.*, b.*, 
                task_link, d.group_alias, d.group_nm,
                u.operator_name'pengirim', 
                IF(tgl_tanda_pendaftaran IS NULL, CURRENT_DATE, tgl_tanda_pendaftaran)'tgl_tanda_pendaftaran'
                FROM daftar_registrasi a
                INNER JOIN daftar_process b ON a.registrasi_id = b.registrasi_id
                INNER JOIN daftar_flow c ON b.flow_id = c.flow_id
                INNER JOIN daftar_group d ON a.daftar_group = d.group_id
                LEFT JOIN com_user u ON a.request_by = u.user_id
				WHERE a.registrasi_id = ? AND c.role_id = ? 
				AND a.pelabuhan_id = ? AND b.action_st = 'process'";
				
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

	// get detail registrasi waiting by id
    function get_registrasi_waiting_by_id_user($params) {
        $sql = "SELECT a.*, b.*, 
                task_link, d.group_alias, d.group_nm,
                u.operator_name'pengirim', 
                IF(tgl_tanda_pendaftaran IS NULL, CURRENT_DATE, tgl_tanda_pendaftaran)'tgl_tanda_pendaftaran'
                FROM daftar_registrasi a
                INNER JOIN daftar_process b ON a.registrasi_id = b.registrasi_id
                INNER JOIN daftar_flow c ON b.flow_id = c.flow_id
                INNER JOIN daftar_group d ON a.daftar_group = d.group_id
                LEFT JOIN com_user u ON a.request_by = u.user_id
				WHERE a.registrasi_id = ? AND c.role_id = ? 
				AND a.request_by = ? AND b.action_st = 'process'";
				
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    // get detail registrasi waiting by id
    function get_detail_registrasi_waiting_by_id($params) {
        $sql = "SELECT a.*, b.*, e.*, f.pelabuhan_nm as pendaftaran_nm, f.pelabuhan_kd as pendaftaran_kd, g.jenisdetail_ket, h.jenis_ket, i.negara_nm,
				j.pelabuhan_nm as penerbitan_nm, j.pelabuhan_kd as penerbitan_kd, k.bahan_ket, l.penggerak_ket, m.subgroup_nm,
                task_link, d.group_alias, d.group_nm,
                u.operator_name'pengirim', 
                IF(tgl_tanda_pendaftaran IS NULL, CURRENT_DATE, tgl_tanda_pendaftaran)'tgl_tanda_pendaftaran'
                FROM daftar_registrasi a
                INNER JOIN daftar_process b ON a.registrasi_id = b.registrasi_id
                INNER JOIN daftar_flow c ON b.flow_id = c.flow_id
                INNER JOIN daftar_group d ON a.daftar_group = d.group_id
				INNER JOIN daftar_baru e ON e.registrasi_id = a.registrasi_id
				LEFT JOIN pelabuhan f ON f.pelabuhan_id = a.pelabuhan_id and f.st_daftar = '1'
				LEFT JOIN daftar_jenisdetail_kapal g on g.jenisdetail_id = e.jenisdetail_kapal
				LEFT JOIN daftar_jenis_kapal h on h.jenis_id = g.jenis_id
				LEFT JOIN negara i on i.negara_id = e.bendera_asal
				LEFT JOIN pelabuhan j on j.pelabuhan_id = e.tempat_terbit and j.st_ukur = '1'
				LEFT JOIN daftar_bahan_kapal k on k.bahan_id = e.bahan_kapal
				LEFT JOIN daftar_penggerak_kapal l on l.penggerak_id = e.penggerak_kapal
				LEFT JOIN daftar_subgroup m on m.subgroup_id = a.bukti_hakmilik
                LEFT JOIN com_user u ON a.request_by = u.user_id
				WHERE a.registrasi_id = ? AND c.role_id = ? 
				AND a.pelabuhan_id = ? AND b.action_st = 'process'";
				
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get detail registrasi waiting by id
    function get_detail_registrasi_member_by_id($params) {
        $sql = "SELECT a.*, b.*, e.*, f.pelabuhan_nm as pendaftaran_nm, f.pelabuhan_kd as pendaftaran_kd, g.jenisdetail_ket, h.jenis_ket, i.negara_nm,
				j.pelabuhan_nm as penerbitan_nm, j.pelabuhan_kd as penerbitan_kd, k.bahan_ket, l.penggerak_ket, m.subgroup_nm,
                task_link, d.group_alias, d.group_nm,
                u.operator_name'pengirim', 
                IF(tgl_tanda_pendaftaran IS NULL, CURRENT_DATE, tgl_tanda_pendaftaran)'tgl_tanda_pendaftaran'
                FROM daftar_registrasi a
                INNER JOIN daftar_process b ON a.registrasi_id = b.registrasi_id
                INNER JOIN daftar_flow c ON b.flow_id = c.flow_id
                INNER JOIN daftar_group d ON a.daftar_group = d.group_id
				INNER JOIN daftar_baru e ON e.registrasi_id = a.registrasi_id
				LEFT JOIN pelabuhan f ON f.pelabuhan_id = a.pelabuhan_id and f.st_daftar = '1'
				LEFT JOIN daftar_jenisdetail_kapal g on g.jenisdetail_id = e.jenisdetail_kapal
				LEFT JOIN daftar_jenis_kapal h on h.jenis_id = g.jenis_id
				LEFT JOIN negara i on i.negara_id = e.bendera_asal
				LEFT JOIN pelabuhan j on j.pelabuhan_id = e.tempat_terbit and j.st_ukur = '1'
				LEFT JOIN daftar_bahan_kapal k on k.bahan_id = e.bahan_kapal
				LEFT JOIN daftar_penggerak_kapal l on l.penggerak_id = e.penggerak_kapal
				LEFT JOIN daftar_subgroup m on m.subgroup_id = a.bukti_hakmilik
                LEFT JOIN com_user u ON a.request_by = u.user_id
				WHERE a.registrasi_id = ? AND a.request_by = ? AND b.action_st = 'process'";
//var_dump($); exit;	
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /*
     * TASK
     */

    // list waiting
    function get_list_my_task_waiting($params) {
        $sql = "SELECT c.*, group_nm, task_link, d.group_alias, i.*, task_nm,
                DATEDIFF(CURDATE(), a.mdd) AS selisih_hari, 
                TIMEDIFF(CURTIME(), SUBSTR(a.mdd, 12, 8)) AS selisih_waktu,
                u.operator_name'pengirim'
                FROM daftar_process a
                INNER JOIN daftar_flow b ON a.flow_id = b.flow_id
                INNER JOIN daftar_registrasi c ON a.registrasi_id = c.registrasi_id
                INNER JOIN daftar_group d ON c.daftar_group = d.group_id
                INNER JOIN com_role_user f ON f.role_id = b.role_id
                INNER JOIN com_user g ON g.user_id = f.user_id
                INNER JOIN daftar_baru i ON i.registrasi_id = c.registrasi_id
                LEFT JOIN com_user u ON c.request_by = u.user_id
                WHERE action_st = 'process' AND c.daftar_completed = '0' AND c.daftar_request_st = '1' 
				AND b.role_id = ? 
				AND g.user_id = ? 
				AND c.pelabuhan_id = ?
                AND nama_kapal LIKE ? 
				AND d.group_nm LIKE ?
                GROUP BY c.registrasi_id 
                ORDER BY selisih_hari DESC, selisih_waktu DESC";
				
        $query = $this->db->query($sql, $params);
//var_dump($params); exit;
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // detail task sesuai flow
    function get_detail_task_by_id($params) {
        $sql = "SELECT a.flow_id, task_nm, role_nm 
                FROM daftar_flow a
                INNER JOIN com_role b On a.role_id = b.role_id
                WHERE flow_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // detail notes
    function get_rute_notes_by_id($params) {
        $sql = "SELECT daftar_id, notes FROM daftar_rute WHERE daftar_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // detail catatan permohonan
    function get_total_catatan_by_registrasi($params) {
        $sql = "SELECT COUNT(process_id)'total' FROM daftar_process WHERE registrasi_id = ?
                AND catatan IS NOT NULL AND mdd_finish IS NOT NULL";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }

    // get list proses
    function get_list_process_by_id($params) {
        $sql = "SELECT a.*, role_nm, operator_name, task_nm
                FROM daftar_process a
                INNER JOIN daftar_flow b ON a.flow_id = b.flow_id
                INNER JOIN com_role c ON b.role_id = c.role_id
                LEFT JOIN com_user u ON a.mdb_finish = u.user_id
                WHERE registrasi_id = ? AND catatan IS NOT NULL AND mdd_finish IS NOT NULL
                ORDER BY mdd_finish DESC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /*
     * REGISTRASI dan RUTE PENERBANGAN
     */

    // get detail airport
    function get_airport_score_by_code($params) {
        $sql = "SELECT airport_nm, is_used_score, airport_region FROM airport WHERE airport_iata_cd = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get published number domestik
    function get_published_number_dom($kode = "DRJU-DAU") {
        // -- AU.008/page/dossier/DRJU-DAU-TAHUN
        $sql = "SELECT SPLIT_STRING(daftar_published_letter, '/', 2)'pages', SPLIT_STRING(daftar_published_letter, '/', 3)'number'
                FROM daftar_registrasi a 
                WHERE RIGHT(SPLIT_STRING(daftar_published_letter, '/', 4), 4) = YEAR(CURRENT_DATE) AND SPLIT_STRING(daftar_published_letter, '/', 1) = 'AU.012'
                ORDER BY ABS(SPLIT_STRING(daftar_published_letter, '/', 2)) DESC, ABS(SPLIT_STRING(daftar_published_letter, '/', 3)) DESC
                LIMIT 0, 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            // create next number
            $pages = intval($result['pages']);
            $number = intval($result['number']) + 1;
            if ($number > 25) {
                $number = 1;
                $pages++;
            }
            return 'AU.012/' . $pages . '/' . $number . '/' . $kode . '-' . date('Y');
        } else {
            return 'AU.012/1/1/' . $kode . '-' . date('Y');
        }
    }

    // get published number internasional
    function get_published_number_int($kode = "DRJU-DAU") {
        // -- AU.008/page/dossier/DRJU-DAU-TAHUN
        $sql = "SELECT SPLIT_STRING(daftar_published_letter, '/', 2)'pages', SPLIT_STRING(daftar_published_letter, '/', 3)'number'
                FROM daftar_registrasi a 
                WHERE RIGHT(SPLIT_STRING(daftar_published_letter, '/', 4), 4) = YEAR(CURRENT_DATE) AND SPLIT_STRING(daftar_published_letter, '/', 1) = 'AU.013'
                ORDER BY ABS(SPLIT_STRING(daftar_published_letter, '/', 2)) DESC, ABS(SPLIT_STRING(daftar_published_letter, '/', 3)) DESC
                LIMIT 0, 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            // create next number
            $pages = intval($result['pages']);
            $number = intval($result['number']) + 1;
            if ($number > 25) {
                $number = 1;
                $pages++;
            }
            return 'AU.013/' . $pages . '/' . $number . '/' . $kode . '-' . date('Y');
        } else {
            return 'AU.013/1/1/' . $kode . '-' . date('Y');
        }
    }

    // get kode daftar
    function get_kode_daftar_domestik($params) {
        // D001-001
        $sql = "SELECT RIGHT(kode_daftar, 3)'last_number', airlines_id, UPPER(SUBSTRING(daftar_flight, 1, 1))'kode_flight'
                FROM daftar_rute 
                WHERE airlines_id = ? AND kode_daftar IS NOT NULL AND daftar_flight = ?
                ORDER BY RIGHT(kode_daftar, 3) DESC
                LIMIT 1";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            // create next number
            $number = intval($result['last_number']) + 1;
            $zero = '';
            for ($i = strlen($number); $i < 3; $i++) {
                $zero .= '0';
            }
            return $result['kode_flight'] . $params[0] . '-' . $zero . $number;
        } else {
            $zero = '';
            for ($i = 1; $i < 3; $i++) {
                $zero .= '0';
            }
            return strtoupper(substr($params[1], 0, 1)) . $params[0] . '-' . $zero . '1';
        }
    }

    // get kode daftar
    function get_kode_daftar_internasional($params) {
        // D001-001
        $sql = "SELECT RIGHT(kode_daftar, 3)'last_number', airlines_id, UPPER(SUBSTRING(daftar_flight, 1, 1))'kode_flight'
                FROM daftar_rute 
                WHERE airlines_id = ? AND kode_daftar IS NOT NULL AND daftar_flight = ?
                ORDER BY RIGHT(kode_daftar, 3) DESC
                LIMIT 1";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            // create next number
            $number = intval($result['last_number']) + 1;
            $zero = '';
            for ($i = strlen($number); $i < 3; $i++) {
                $zero .= '0';
            }
            return $result['kode_flight'] . $params[0] . '-' . $zero . $number;
        } else {
            $zero = '';
            for ($i = 1; $i < 3; $i++) {
                $zero .= '0';
            }
            return strtoupper(substr($params[1], 0, 1)) . $params[0] . '-' . $zero . '1';
        }
    }

    // get kode daftar yang terdaftar
    function get_kode_daftar_terdaftar($params) {
        // airlines id $detail['daftar_rute_start'], $detail['daftar_rute_end'], $detail['daftar_season']
        $sql = "SELECT a.kode_daftar 
                FROM daftar_rute a
                INNER JOIN daftar_registrasi b ON a.registrasi_id = b.registrasi_id
                WHERE a.airlines_id = ?
                AND (a.daftar_rute_start = ? OR a.daftar_rute_start = ?) 
                AND (a.daftar_rute_end = ? OR a.daftar_rute_end = ?) 
                AND daftar_season = ?
                AND b.registrasi_id <> ?
                GROUP BY a.kode_daftar";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['kode_daftar'];
        } else {
            return false;
        }
    }

    // get kode frekuensi
    function get_kode_frekuensi($kode_daftar) {
        // D001-001
        $sql = "SELECT RIGHT(kode_frekuensi, 3)'last_number'
                FROM daftar_rute 
                WHERE kode_daftar = ?
                ORDER BY RIGHT(kode_frekuensi, 3) DESC
                LIMIT 1";
        $query = $this->db->query($sql, $kode_daftar);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            // create next number
            $number = intval($result['last_number']) + 1;
            $zero = '';
            for ($i = strlen($number); $i < 3; $i++) {
                $zero .= '0';
            }
            return $kode_daftar . '-' . $zero . $number;
        } else {
            $zero = '';
            for ($i = 1; $i < 3; $i++) {
                $zero .= '0';
            }
            return $kode_daftar . '-' . $zero . '1';
        }
    }
    // get list daftar rute by registrasi
    function get_list_rute_by_registrasi($params) {
        $sql = "SELECT * FROM daftar_rute WHERE registrasi_id = ?";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar rute by id
    function get_list_daftar_rute_by_id($params) {
        $sql = "SELECT a.daftar_id, airlines_id, kode_daftar, kode_frekuensi, registrasi_id, 
                daftar_completed, daftar_approval, daftar_type, daftar_flight, daftar_st, daftar_start_date, daftar_expired_date, 
                daftar_rute_start, daftar_rute_end, pairing, notes,
                MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'total_frekuensi'
                FROM daftar_rute a
                INNER JOIN daftar_data b On a.daftar_id = b.daftar_id
                WHERE a.registrasi_id = ? AND a.airlines_id = ?
                GROUP BY a.daftar_id
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar rute by id
    function get_list_daftar_rute_approved_by_id($params) {
        $sql = "SELECT a.daftar_id, a.airlines_id, kode_daftar, kode_frekuensi, registrasi_id, 
                daftar_completed, daftar_approval, daftar_type, daftar_flight, daftar_st, daftar_start_date, daftar_expired_date, 
                daftar_rute_start, daftar_rute_end, pairing, notes,
                MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'total_frekuensi' 
                FROM daftar_rute a
                INNER JOIN daftar_data b On a.daftar_id = b.daftar_id 
                WHERE a.registrasi_id = ? AND a.airlines_id = ? AND a.daftar_approval = 'approved'
                GROUP BY a.daftar_id
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar data by id
    function get_list_daftar_data_by_id($params) {
        $sql = "SELECT rute_id, a.daftar_id, rute_all, tipe, capacity, 
                IF(LENGTH(TRIM(flight_no)) > 4, TRIM(flight_no), CONCAT(airlines_iata_cd, TRIM(flight_no)))'flight_no', 
                etd, eta, doop, roon, 
                start_date, end_date, a.is_used_score, GET_FREKUENSI_FROM_DOS(doop)'frekuensi',
                kode_daftar, kode_frekuensi, daftar_approval, c.airlines_iata_cd
                FROM daftar_data a
                INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id 
                INNER JOIN airlines c ON c.airlines_id = b.airlines_id 
                WHERE a.daftar_id = ?
                ORDER BY rute_all ASC, etd ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi by registrasi
    function get_total_frekuensi_by_registrasi_id($params) {
        $sql = "SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', SUM(frekuensi)'frekuensi' 
                FROM 
                (
                        SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.registrasi_id = ?
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi approved by registrasi
    function get_total_frekuensi_approved_by_registrasi_id($params) {
        $sql = "SELECT 
                MIN(start_date)'start_date', MAX(end_date)'end_date', SUM(frekuensi)'frekuensi',
                IF(MIN(start_date) < CURRENT_DATE, CURRENT_DATE, MIN(start_date))'valid_start_date',
                IF(MAX(end_date) < CURRENT_DATE, CURRENT_DATE, MAX(end_date))'valid_end_date'
                FROM 
                (
                        SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.registrasi_id = ? AND b.daftar_approval = 'approved'
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi by kode_daftar
    function get_total_frekuensi_existing_by_kode_daftar($params) {
        $sql = "SELECT COUNT(daftar_id)'total_rute', MIN(start_date)'start_date', MAX(end_date)'end_date', SUM(frekuensi)'frekuensi' 
                FROM 
                (
                        SELECT b.daftar_id, MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.kode_daftar = ? AND b.daftar_completed = '1'
                        AND b.daftar_approval = 'approved'
                        AND b.daftar_active = '1'
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list waiting by registrasi
    function get_list_waiting_approval_by_registrasi($params) {
        $sql = "SELECT COUNT(*)'total' 
                FROM daftar_rute 
                WHERE registrasi_id = ? AND daftar_completed = '0' AND daftar_approval = 'waiting'";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }

    // get list all rejected by registrasi
    function get_list_reject_all_by_registrasi($params) {
        $sql = "SELECT COUNT(*)'total'
                FROM daftar_rute 
                WHERE registrasi_id = ?
                AND daftar_completed = '0' 
                AND daftar_approval <> 'rejected'";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }

    // update daftar
    function update_daftar($params, $where) {
        $this->db->where($where);
        return $this->db->update('daftar_rute', $params);
    }

    // edit process
    function update_process($params, $where) {
        $this->db->where($where);
        return $this->db->update('daftar_process', $params);
    }

    // update registrasi
    function update_registrasi($params, $where) {
        $this->db->where($where);
        return $this->db->update('daftar_registrasi', $params);
    }

    // done process registrasi
    function registrasi_done_process($params, $where) {
        $this->db->where($where);
        return $this->db->update('daftar_registrasi', $params);
    }

    // update st active
    function update_st_by_kode_frekuensi($params) {
        $sql = "UPDATE daftar_rute SET daftar_active = ? WHERE kode_frekuensi = ?";
        return $this->db->query($sql, $params);
    }

    // update st bayar
    function update_status_bayar_approved($params, $where) {
        $this->db->where($where);
        return $this->db->update('daftar_rute', $params);
    }

    /*
     * RUTE SEBELUMNYA
     */

    // get list daftar rute by kode frekuensi
    function get_list_daftar_rute_aktif_by_kode_frekuensi($params) {
        $sql = "SELECT a.daftar_id, airlines_id, kode_daftar, kode_frekuensi, registrasi_id, 
                daftar_completed, daftar_approval, daftar_type, daftar_flight, daftar_st, daftar_start_date, daftar_expired_date, 
                daftar_rute_start, daftar_rute_end, pairing, notes,
                MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'total_frekuensi'
                FROM daftar_rute a
                INNER JOIN daftar_data b On a.daftar_id = b.daftar_id
                WHERE a.kode_frekuensi = ? AND a.airlines_id = ?
                AND a.daftar_completed = '1'
                AND a.daftar_approval = 'approved'
                AND a.daftar_active = '1'
                GROUP BY a.daftar_id
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar rute by kode daftar
    function get_list_daftar_rute_aktif_by_kode_daftar($params) {
        $sql = "SELECT a.daftar_id, airlines_id, kode_daftar, kode_frekuensi, registrasi_id, 
                daftar_completed, daftar_approval, daftar_type, daftar_flight, daftar_st, daftar_start_date, daftar_expired_date, 
                daftar_rute_start, daftar_rute_end, pairing, notes,
                MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'total_frekuensi'
                FROM daftar_rute a
                INNER JOIN daftar_data b On a.daftar_id = b.daftar_id
                WHERE a.kode_daftar = ? AND a.airlines_id = ?
                AND a.daftar_completed = '1'
                AND a.daftar_approval = 'approved'
                AND a.daftar_active = '1'
                GROUP BY a.daftar_id
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /*
     * FILES DOWNLOAD
     */

    // check list files uploaded
    function check_list_file_uploaded($params) {
        $sql = "SELECT COUNT(*)'total'
                FROM daftar_file_references a
                INNER JOIN daftar_rules_files b ON a.ref_id = b.ref_id
                LEFT JOIN daftar_files c ON a.ref_id = c.ref_id AND c.registrasi_id = ?
                LEFT JOIN com_user d ON c.check_by = d.user_id
                WHERE b.group_id = ? AND b.data_flight = ? 
                AND file_check IS NOT NULL AND file_check = '0'";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }

    // get list files uploaded
    function get_list_file_uploaded($params) {
        $sql = "SELECT * FROM daftar_files WHERE registrasi_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
	
    // get tgl ttd
    function get_tgl_ttd($params) {
        $sql = "SELECT tgl_ttd_akta FROM daftar_registrasi WHERE registrasi_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

	// get list files references
    function get_list_file_required($params) {
        $sql = "SELECT a.* FROM daftar_file_references a
                INNER JOIN daftar_rules_files b ON a.ref_id = b.ref_id
                WHERE b.group_id = ? AND b.subgroup_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

	
    // get list files uploaded
    function get_list_file_uploaded_int($params) {
        $sql = "SELECT a.*, c.file_id, file_path, file_name, file_check, check_date, d.operator_name'check_by'
                FROM daftar_file_references a
                INNER JOIN daftar_rules_files b ON a.ref_id = b.ref_id
                LEFT JOIN daftar_files c ON a.ref_id = c.ref_id AND c.registrasi_id = ?
                LEFT JOIN com_user d ON c.check_by = d.user_id
                WHERE b.group_id = ? AND b.data_flight = ? 
                AND b.airlines_st = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list file pencabutan
    function get_list_file_pencabutan_by_id($params) {
        $sql = "SELECT a.* FROM daftar_file_pencabutan a
                INNER JOIN daftar_registrasi b ON a.registrasi_id = b.registrasi_id
                WHERE a.registrasi_id = ? AND airlines_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list slot uploaded
    function get_list_slot_time_by_id($params) {
        $sql = "SELECT a.* FROM daftar_slot_time a
                INNER JOIN daftar_registrasi b ON a.registrasi_id = b.registrasi_id
                WHERE a.registrasi_id = ? AND airlines_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list slot iasm
    function get_list_slot_iasm_by_id($params) {
        $sql = "SELECT a.* 
                FROM daftar_data_slot a
                INNER JOIN daftar_data b ON a.rute_id = b.rute_id
                INNER JOIN daftar_rute c ON b.daftar_id = c.daftar_id
                WHERE c.registrasi_id = ? AND c.airlines_id = ?
                ORDER BY a.flight_no ASC, a.services_cd ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get detail files  by id
    function get_detail_files_by_id($params) {
        $sql = "SELECT * FROM daftar_files WHERE registrasi_id = ? AND ref_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get detail slot  by id
    function get_detail_slot_by_id($params) {
        $sql = "SELECT * FROM daftar_slot_time WHERE registrasi_id = ? AND slot_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get detail telaah
    function get_detail_telaah_by_id($params) {
        $sql = "SELECT a.*, operator_name 
                FROM daftar_telaah a
                LEFT JOIN com_user b ON a.telaah_by = b.user_id 
                WHERE registrasi_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get detail file pencabutan  by id
    function get_detail_file_pencabutan_by_id($params) {
        $sql = "SELECT * FROM daftar_file_pencabutan WHERE registrasi_id = ? AND letter_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // add process
    function insert_telaah($params) {
        $sql = "INSERT INTO daftar_telaah (registrasi_id, telaah_file, telaah_by, telaah_date)
                VALUES (?, ?, ?, NOW())";
        return $this->db->query($sql, $params);
    }

    // delete telaah
    function delete_telaah($params) {
        $sql = "DELETE FROM daftar_telaah WHERE registrasi_id = ?";
        return $this->db->query($sql, $params);
    }

    // insert memo process
    function insert_memo_process($params) {
        return $this->db->insert('daftar_memos', $params);
    }

    // delete memo process
    function delete_memo_process($params) {
        return $this->db->delete('daftar_memos', $params);
    }

    // delete tembusan
    function delete_tembusan($params) {
        return $this->db->delete('daftar_tembusan', $params);
    }

    // insert tembusan
    function insert_tembusan($params) {
        return $this->db->insert('daftar_tembusan', $params);
    }

    // checklist files
    function check_list_files_all($params) {
        $sql = "UPDATE daftar_files SET file_check = ?, check_by = ?, check_date = ? WHERE registrasi_id = ?";
        return $this->db->query($sql, $params);
    }

    // checklist files
    function check_list_files($params) {
        $sql = "UPDATE daftar_files SET check_by = ?, check_date = NOW(), file_check = ? WHERE file_id = ?";
        return $this->db->query($sql, $params);
    }

    /*
     * ACTION
     */

    // get process id
    function get_process_id() {
        $time = microtime(true);
        $id = str_replace('.', '', $time);
        return $id;
    }

    // action control
    function get_action_control($params) {
        $sql = "SELECT action_reject, action_revisi, action_send, action_rollback, action_publish 
                FROM com_role_action
                WHERE role_id = ? ";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // update process
    function action_update($params) {
        $sql = "UPDATE daftar_process SET process_st = ?, action_st = ? , mdb_finish = ?, mdd_finish = NOW()          
                WHERE process_id = ?";
        return $this->db->query($sql, $params);
    }

    // add process
    function insert_process($params) {
        $sql = "INSERT INTO daftar_process (process_id, registrasi_id, flow_id, mdb, mdd)
                VALUES (?, ?, ?, ?, NOW())";
        return $this->db->query($sql, $params);
    }

    // get role next flow
    function get_role_next_from($params) {
		$sql = "SELECT role_id 
                FROM daftar_flow 
                WHERE flow_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function get_list_date_reschedule($params) {
//var_dump($params); exit;
        $sql = "SELECT tanggal1, tanggal2, tanggal3 
                FROM daftar_reschedule 
                WHERE registrasi_id = ? and role_id= ?
				ORDER BY mdd DESC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function insert_schedule($params) {
        $sql = "INSERT INTO daftar_reschedule (registrasi_id, role_id, tanggal1, tanggal2, tanggal3, mdd)
                VALUES (?, ?, ?, ?, ?, NOW())";
        return $this->db->query($sql, $params);
    }

    /*
     * TARIF
     */

    // get tarif baru
    function get_tarif_rute_baru() {
        $sql = "SELECT pref_value FROM com_preferences WHERE pref_group = 'tarif_rute' AND pref_nm = 'baru'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['pref_value'];
        } else {
            return 0;
        }
    }

    // get tarif per frekuensi
    function get_tarif_rute_frekuensi() {
        $sql = "SELECT pref_value FROM com_preferences WHERE pref_group = 'tarif_rute' AND pref_nm = 'frekuensi'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['pref_value'];
        } else {
            return 0;
        }
    }

    // utilities
    function terbilang($x) {
        $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        if ($x < 12)
            return " " . $abil[$x];
        elseif ($x < 20)
            return Terbilang($x - 10) . "belas";
        elseif ($x < 100)
            return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
        elseif ($x < 200)
            return " seratus" . Terbilang($x - 100);
        elseif ($x < 1000)
            return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
        elseif ($x < 2000)
            return " seribu" . Terbilang($x - 1000);
        elseif ($x < 1000000)
            return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
        elseif ($x < 1000000000)
            return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
    }

    // get by id for searching
    function get_preferences_by_group_and_name($params) {
        $sql = "SELECT * FROM com_preferences WHERE pref_group = ? AND pref_nm = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list memos
    function get_list_memos_by_daftar($params) {
        $sql = "SELECT a.*, operator_name 
                FROM daftar_memos a
                LEFT JOIN com_user b On a.memo_by = b.user_id
                WHERE registrasi_id = ?
                ORDER BY memo_date DESC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get surat penerbitan by kode frekuensi
    function get_surat_penerbitan_existing_by_kode_frekuensi($params) {
        $sql = "SELECT b.registrasi_id, daftar_published_letter, daftar_published_date, group_nm, 
                ((SPLIT_STRING(daftar_published_letter, '/', 2)*25) + SPLIT_STRING(daftar_published_letter, '/', 3))'published_number'
                FROM daftar_rute a
                INNER JOIN daftar_registrasi b ON a.registrasi_id = b.registrasi_id
                INNER JOIN daftar_group c ON b.daftar_group = c.group_id
                WHERE kode_frekuensi = ? AND a.registrasi_id <> ?
                AND ((SPLIT_STRING(daftar_published_letter, '/', 2)*25) + SPLIT_STRING(daftar_published_letter, '/', 3)) <= ?
                ORDER BY daftar_published_letter DESC";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list editorial
    function get_list_editorial($params) {
        $sql = "SELECT * 
                FROM redaksional 
                WHERE redaksional_group = ? 
                ORDER BY redaksional_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list editorial by registrasi
    function get_list_editorial_by_registrasi($params) {
        $sql = "SELECT a.tembusan_id, redaksional_nm 
                FROM daftar_tembusan a 
                INNER JOIN redaksional b ON b.redaksional_id = a.tembusan_value 
                WHERE a.registrasi_id = ? AND b.redaksional_group = ? 
                ORDER BY a.tembusan_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list redaksional by registrasi
    function get_list_redaksional_by_registrasi($params) {
        $sql = "SELECT a.pref_id, a.pref_group, a.pref_nm, a.pref_value 
                FROM com_preferences a 
                RIGHT JOIN daftar_tembusan b ON b.tembusan_value = a.pref_nm
                WHERE a.pref_group = 'redaksional' AND b.registrasi_id = ?
                ORDER BY a.pref_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get user by role
    function get_com_user_by_role($params) {
        $sql = "SELECT operator_name, operator_nip, operator_pangkat
                FROM com_user a
                INNER JOIN com_role_user b ON a.user_id = b.user_id
                WHERE role_id = ? AND operator_pangkat IS NOT NULL
                LIMIT 0, 1";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get barcode value
    function get_barcode_value($params) {
        $sql = "SELECT * 
            FROM com_preferences a 
            WHERE a.pref_group = 'barcode' AND a.pref_nm = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get nomor surat sebelumnya
    function get_published_letter_old($params) {
        $sql = "SELECT a.daftar_published_letter, a.daftar_published_date, a.daftar_perihal, a.daftar_flight, b.group_nm 
        FROM daftar_registrasi a 
        LEFT JOIN daftar_group b ON b.group_id = a.daftar_group 
            WHERE a.registrasi_id = ? AND airlines_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /* ==================== DOWNLOAD PUBLISHED LETTER ==================== */

    // get list daftar rute by kode daftar
    function get_daftar_data_by_registrasi_id($params) {
        $sql = "SELECT rute_id, a.daftar_id, rute_all, tipe, capacity, flight_no, etd, eta, doop, roon, 
                start_date, end_date, a.is_used_score, GET_FREKUENSI_FROM_DOS(doop)'frekuensi',
                kode_daftar, kode_frekuensi, daftar_approval, b.daftar_penundaan_start, b.daftar_penundaan_end, c.airlines_iata_cd, b.daftar_st
                FROM daftar_data a
                INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id 
                LEFT JOIN airlines c ON c.airlines_id = b.airlines_id 
                WHERE b.registrasi_id = ? AND b.daftar_approval = 'approved' 
                ORDER BY b.daftar_id ASC, a.rute_all ASC, a.etd ASC";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi
    function get_total_frekuensi($params) {
        $sql = "SELECT a.daftar_id, pairing, MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'total_frekuensi' 
                FROM daftar_rute a
                INNER JOIN daftar_data b On a.daftar_id = b.daftar_id 
                WHERE a.registrasi_id = ? AND a.airlines_id = ? AND a.daftar_id = ? AND a.daftar_approval = 'approved'
                GROUP BY a.daftar_id
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // add blacklist
    function insert_daftar_blacklist($params) {
        return $this->db->insert('daftar_pencabutan', $params);
    }

    // get list files pencabutan
    function get_list_file_pencabutan_uploaded($params) {
        $sql = "SELECT * FROM daftar_file_pencabutan WHERE registrasi_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get detail registrasi waiting by registrasi_id 
    function get_detail_registrasi_waiting_by_registrasi_id($params) {
        $sql = "SELECT a.*, b.*, 
                task_link, d.group_alias, d.group_nm, daftar_perihal,
                u.operator_name'pengirim', airlines_nm, airlines_iata_cd, 
                airlines_nationality, f.operator_name AS 'daftar_verified_by',
                IF(daftar_published_date IS NULL, CURRENT_DATE, daftar_published_date)'daftar_published_date'
                FROM daftar_registrasi a
                INNER JOIN daftar_process b ON a.registrasi_id = b.registrasi_id
                INNER JOIN daftar_flow c ON b.flow_id = c.flow_id
                INNER JOIN daftar_group d ON a.daftar_group = d.group_id
                INNER JOIN airlines e ON a.airlines_id = e.airlines_id
                LEFT JOIN com_user u ON a.daftar_request_by = u.user_id
                LEFT JOIN com_user f ON a.daftar_valid_by = f.user_id
                WHERE a.registrasi_id = ? AND b.action_st = 'process'";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /*     * * PERUBAHAN ** */

    // get total frekuensi approved by registrasi
    function get_total_frekuensi_approved_perubahan_by_registrasi_id($params) {
        $sql = "SELECT 
                MIN(start_date)'start_date', MAX(end_date)'end_date', SUM(frekuensi)'frekuensi',
                IF(MIN(start_date) < CURRENT_DATE, CURRENT_DATE, MIN(start_date))'valid_start_date',
                IF(MAX(end_date) < CURRENT_DATE, CURRENT_DATE, MAX(end_date))'valid_end_date'
                FROM 
                (
                        SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.registrasi_id = ? AND b.daftar_approval = 'approved' AND b.daftar_st <> 'pencabutan'
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi approved by registrasi
    function get_total_frekuensi_approved_pencabutan_by_registrasi_id($params) {
        $sql = "SELECT 
                MIN(start_date)'start_date', MAX(end_date)'end_date', SUM(frekuensi)'frekuensi',
                IF(MIN(start_date) < CURRENT_DATE, CURRENT_DATE, MIN(start_date))'valid_start_date',
                IF(MAX(end_date) < CURRENT_DATE, CURRENT_DATE, MAX(end_date))'valid_end_date'
                FROM 
                (
                        SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.registrasi_id = ? AND b.daftar_approval = 'approved' AND b.daftar_st = 'pencabutan'
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

}
