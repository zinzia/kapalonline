<?php

class m_jenis_kapal extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    // get total aircraft
    function get_total_jenis_kapal($params) {
        $sql = "SELECT count(*)'total' FROM daftar_jenis_kapal
                WHERE jenis_ket LIKE ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return array();
        }
    }

    // get all data airlines
    function get_all_jenis_kapal($params) {
        $sql = "SELECT * FROM daftar_jenis_kapal
                WHERE jenis_ket LIKE ? 
                ORDER BY jenis_ket ASC
                LIMIT ?, ?";
				//var_dump($params);
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list data aircraft
    function get_list_jenis_kapal() {
        $sql = "SELECT * FROM daftar_jenis_kapal ORDER BY jenis_ket ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// get all data airport by id
    function get_jenis_kapal_by_id($params) {
        $sql = "SELECT * FROM daftar_jenis_kapal 
                WHERE jenis_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// insert
    function insert($params) {
        $sql = "INSERT INTO daftar_jenis_kapal (jenis_ket, jenis_alias, jenis_status) 
                VALUES (?, ?, ?)";
        return $this->db->query($sql, $params);
    }

// update
    function update($params) {
        $sql = "UPDATE daftar_jenis_kapal set jenis_ket = ?, jenis_alias = ?, jenis_status = ? 
               WHERE jenis_id = ?";
        return $this->db->query($sql, $params);
    }

// delete
    function delete($params) {
        $sql = "DELETE FROM daftar_jenis_kapal WHERE jenis_id = ? ";
        return $this->db->query($sql, $params);
    }

    function is_exist_jenis_kapal($params) {
        $sql = "SELECT * FROM daftar_jenis_kapal WHERE jenis_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $query->free_result();
            // return
            return true;
        } else {
            return false;
        }
    }

}
