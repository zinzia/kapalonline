<?php

class m_pelabuhan extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    // get total aircraft
    function get_total_pelabuhan($params) {
        $sql = "SELECT count(*)'total' FROM pelabuhan
                WHERE pelabuhan_nm LIKE ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return array();
        }
    }

    // get all data
    function get_all_pelabuhan($params) {
        $sql = "SELECT * FROM pelabuhan 
                WHERE pelabuhan_nm LIKE ? 
                ORDER BY pelabuhan_nm ASC
                LIMIT ?, ?";
				//var_dump($params);
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list data aircraft
    function get_list_pelabuhan() {
        $sql = "SELECT * FROM pelabuhan ORDER BY pelabuhan_nm ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// get all data airport by id
    function get_pelabuhan_by_id($params) {
        $sql = "SELECT * FROM pelabuhan 
                WHERE pelabuhan_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// insert
    function insert($params) {
        $sql = "INSERT INTO pelabuhan (pelabuhan_nm, pelabuhan_kd, st_daftar, st_ukur) 
                VALUES (?, ?, ?, ?)";
        return $this->db->query($sql, $params);
    }

// update
    function update($params) {
        $sql = "UPDATE pelabuhan set pelabuhan_nm = ?, pelabuhan_kd = ?, st_daftar = ?, st_ukur = ? 
               WHERE pelabuhan_id = ?";
        return $this->db->query($sql, $params);
    }

// delete
    function delete($params) {
        $sql = "DELETE FROM pelabuhan WHERE pelabuhan_id = ? ";
        return $this->db->query($sql, $params);
    }

    function is_exist_pelabuhan($params) {
        $sql = "SELECT * FROM pelabuhan WHERE pelabuhan_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $query->free_result();
            // return
            return true;
        } else {
            return false;
        }
    }

}
