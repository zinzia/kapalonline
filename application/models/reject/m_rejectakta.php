<?php

class m_rejectakta extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

	
    // get detail files  by id
    function get_detail_files_by_id($params) {
        $sql = "SELECT * FROM daftar_files WHERE registrasi_id = ? AND ref_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
	
    // get list tahun
    function get_list_tahun_report($params) {
        $sql = "SELECT DISTINCT tahun FROM
                (
                        SELECT YEAR(tgl_tanda_pendaftaran)'tahun'
                        FROM daftar_registrasi
                        WHERE request_by = ? AND daftar_completed = '1' AND daftar_approval = 'rejected'
                        UNION ALL
                        SELECT YEAR(CURRENT_DATE)'tahun'
                ) rs
                ORDER BY tahun ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

	// get list group request
    function get_all_group() {
        $sql = "SELECT * FROM daftar_group WHERE group_st = 'show' ORDER BY group_id ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    // get total task
    function get_total_finished_daftar_registrasi($params) {
        $sql = "SELECT COUNT(*)'total'
                FROM daftar_registrasi
                WHERE pelabuhan_id = ?
                AND YEAR(tgl_tanda_pendaftaran) = ? 
				AND MONTH(tgl_tanda_pendaftaran) = ?
                AND daftar_group LIKE ?
                AND daftar_completed = '1' 
				AND daftar_approval = 'rejected'";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }

    // get list task
    function get_list_finished_daftar_registrasi($params) {
        $sql = "SELECT c.registrasi_id, c.tgl_pengajuan, f.nama_kapal, d.group_nm, c.nama_pemohon,
                task_link, operator_name,
                DATEDIFF(CURDATE(), a.mdd)'selisih_hari', 
                TIMEDIFF(CURTIME(), SUBSTR(a.mdd, 12, 8))'selisih_waktu', 
                group_alias
                FROM daftar_process a
                INNER JOIN daftar_flow b ON a.flow_id = b.flow_id
                INNER JOIN daftar_registrasi c ON a.registrasi_id = c.registrasi_id
                INNER JOIN daftar_group d ON c.daftar_group = d.group_id
                INNER JOIN daftar_baru f ON f.registrasi_id = c.registrasi_id
                LEFT JOIN com_user u ON c.request_by = u.user_id
                WHERE c.daftar_approval = 'rejected' 
				AND c.daftar_completed = '1' 
				AND b.flow_id = '1'  
                AND c.pelabuhan_id = ?
                AND YEAR(c.tgl_tanda_pendaftaran) = ? 
				AND MONTH(c.tgl_tanda_pendaftaran) = ? 
				AND c.daftar_group LIKE ?
                GROUP BY c.registrasi_id
                ORDER BY c.tgl_pengajuan ASC
                LIMIT ?, ?";
//var_dump($sql);
//exit;				
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function get_list_file_required($params) {
        $sql = "SELECT a.* FROM daftar_file_references a
                INNER JOIN daftar_rules_files b ON a.ref_id = b.ref_id
                WHERE b.group_id = ? AND b.subgroup_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    // get list files uploaded
    function get_list_file_uploaded($params) {
        $sql = "SELECT * FROM daftar_files WHERE registrasi_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    // get detail registrasi by id
    function get_detail_registrasi_data_by_id($params) {
        $sql = "SELECT a.*, l.flow_id, c.*,b.pelabuhan_nm as pendaftaran_nm, b.pelabuhan_kd as pendaftaran_kd, c.*, d.jenisdetail_ket, e.jenis_ket, 
				f.negara_nm, g.pelabuhan_nm as penerbitan_nm, g.pelabuhan_kd as penerbitan_kd, h.bahan_ket, i.penggerak_ket, j.subgroup_nm,
                task_nm, k.mdd'tanggal_proses', m.group_nm, u.operator_name'pengirim'
                FROM daftar_registrasi a
                INNER JOIN daftar_process k ON a.registrasi_id = k.registrasi_id
                INNER JOIN daftar_flow l ON k.flow_id = l.flow_id
                INNER JOIN daftar_group m ON a.daftar_group = m.group_id
				LEFT JOIN com_user u ON a.request_by = u.user_id
				LEFT JOIN pelabuhan b on b.pelabuhan_id = a.pelabuhan_id and b.st_daftar = '1'
				LEFT JOIN daftar_baru c on c.registrasi_id = a.registrasi_id
				LEFT JOIN daftar_jenisdetail_kapal d on d.jenisdetail_id = c.jenisdetail_kapal
				LEFT JOIN daftar_jenis_kapal e on e.jenis_id = d.jenis_id
				LEFT JOIN negara f on f.negara_id = c.bendera_asal
				LEFT JOIN pelabuhan g on g.pelabuhan_id = c.tempat_terbit and g.st_ukur = '1'
				LEFT JOIN daftar_bahan_kapal h on h.bahan_id = c.bahan_kapal
				LEFT JOIN daftar_penggerak_kapal i on i.penggerak_id = c.penggerak_kapal
				LEFT JOIN daftar_subgroup j on j.subgroup_id = a.bukti_hakmilik
				WHERE a.registrasi_id = ? AND a.pelabuhan_id = ? AND daftar_request_st = '1'  AND k.action_st = 'done'  AND k.process_st = 'reject'
                GROUP BY a.registrasi_id";
        $query = $this->db->query($sql, $params);
//var_dump($sql); exit;
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    // get detail
    function get_reject_daftar_by_registrasi($params) {
        $sql = "SELECT a.*, group_nm, group_alias, u.operator_name'pengirim', airlines_nm, airlines_address, airlines_nationality,
                p.operator_name'published_by', p.operator_nip, p.operator_pangkat, p.jabatan
                FROM daftar_registrasi a
                INNER JOIN daftar_group b ON a.daftar_group = b.group_id
                INNER JOIN airlines c ON a.airlines_id = c.airlines_id
                LEFT JOIN com_user u ON a.daftar_request_by = u.user_id
                LEFT JOIN com_user p ON a.daftar_published_by = p.user_id
                WHERE a.registrasi_id = ?
                AND daftar_completed = '1' AND daftar_approval = 'rejected'";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar rute by kode daftar
    function get_daftar_rute_data_by_kode_daftar($params) {
        $sql = "SELECT a.daftar_id, a.airlines_id, a.kode_daftar, a.kode_frekuensi, a.daftar_start_date, a.daftar_expired_date,
                aircraft_type, aircraft_capacity, dos, ron, pairing, a.daftar_rute_start, a.daftar_rute_end,
                a.daftar_penundaan_start, a.daftar_penundaan_end,
                (
                        IF(SUBSTRING(b.doop, 1, 1) = 0, 0, 1) +
                        IF(SUBSTRING(b.doop, 2, 1) = 0, 0, 1) +
                        IF(SUBSTRING(b.doop, 3, 1) = 0, 0, 1) +
                        IF(SUBSTRING(b.doop, 4, 1) = 0, 0, 1) +
                        IF(SUBSTRING(b.doop, 5, 1) = 0, 0, 1) +
                        IF(SUBSTRING(b.doop, 6, 1) = 0, 0, 1) +
                        IF(SUBSTRING(b.doop, 7, 1) = 0, 0, 1)
                )'frekuensi',
                b.rute_all, b.flight_no, b.etd, b.eta,
                (((SPLIT_STRING(daftar_published_letter, '/', 2)*25) + SPLIT_STRING(daftar_published_letter, '/', 3)) * (ABS(RIGHT(SPLIT_STRING(daftar_published_letter, '/', 1), 3)) * 10))'published_number', a.notes, b.doop, b.start_date, b.end_date, b.tipe, b.capacity, b.roon, c.daftar_published_letter 
                FROM daftar_rute a
                INNER JOIN daftar_data b ON a.daftar_id = b.daftar_id
                LEFT JOIN daftar_registrasi c ON a.registrasi_id = c.registrasi_id
                WHERE a.daftar_completed = '1' AND a.daftar_approval = 'rejected'
                AND a.registrasi_id = ? AND a.airlines_id = ?
                ORDER BY kode_frekuensi ASC, b.rute_id ASC";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list surat ijin rute by kode frekuensi
    function get_surat_ijin_by_kode_frekuensi($params) {
        $sql = "SELECT b.registrasi_id, daftar_published_letter, daftar_published_date, group_nm,
                ((SPLIT_STRING(daftar_published_letter, '/', 2)*25) + SPLIT_STRING(daftar_published_letter, '/', 3))'reject_number'
                FROM daftar_rute a
                INNER JOIN daftar_registrasi b ON a.registrasi_id = b.registrasi_id
                INNER JOIN daftar_group c ON b.daftar_group = c.group_id
                WHERE kode_frekuensi = ? AND a.registrasi_id <> ? ORDER BY daftar_published_letter DESC";
        //AND ((SPLIT_STRING(daftar_published_letter, '/', 2)*25) + SPLIT_STRING(daftar_published_letter, '/', 3)) <= ?
        //ORDER BY daftar_published_letter DESC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get by id for searching
    function get_preferences_by_group_and_name($params) {
        $sql = "SELECT * FROM com_preferences WHERE pref_group = ? AND pref_nm = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list slot by id
    function get_list_data_slot_by_id($params) {
        $sql = "SELECT a.* FROM daftar_slot_time a
                INNER JOIN daftar_registrasi b ON a.registrasi_id = b.registrasi_id
                WHERE a.registrasi_id = ? AND airlines_id = ?";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list rute by kode frekuensi
    function get_list_data_rute_by_kode_frekuensi_old($params) {
        $sql = "SELECT a.*, b.kode_daftar, b.kode_frekuensi, daftar_published_letter,
                (
                IF(SUBSTRING(b.dos, 1, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 2, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 3, 1) = 0, 0, 1) +
                IF(SUBSTRING(b.dos, 4, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 5, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 6, 1) = 0, 0, 1) +
                IF(SUBSTRING(b.dos, 7, 1) = 0, 0, 1)
                )'frekuensi',
                aircraft_type, aircraft_capacity, dos, ron, pairing, daftar_start_date, daftar_expired_date,
                ((SPLIT_STRING(daftar_published_letter, '/', 2)*25) + SPLIT_STRING(daftar_published_letter, '/', 3))'reject_number'
                FROM daftar_data a
                INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                LEFT JOIN daftar_registrasi c ON b.registrasi_id = c.registrasi_id
                WHERE b.kode_frekuensi = ? AND c.registrasi_id <> ? AND (((SPLIT_STRING (daftar_published_letter, '/', 2) * 25) + SPLIT_STRING (daftar_published_letter, '/', 3)) + ABS(RIGHT(SPLIT_STRING(daftar_published_letter, '/', 1), 3))) < ? 
                ORDER BY a.etd ASC, daftar_published_letter DESC, rute_id ASC 
                LIMIT 2";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list rute by kode frekuensi
    function get_list_data_rute_by_kode_frekuensi_old_preview($params) {
        $sql = "SELECT a.*, b.kode_daftar, b.kode_frekuensi, daftar_published_letter,
                (
                IF(SUBSTRING(b.dos, 1, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 2, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 3, 1) = 0, 0, 1) +
                IF(SUBSTRING(b.dos, 4, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 5, 1) = 0, 0, 1) + IF(SUBSTRING(b.dos, 6, 1) = 0, 0, 1) +
                IF(SUBSTRING(b.dos, 7, 1) = 0, 0, 1)
                )'frekuensi',
                aircraft_type, aircraft_capacity, dos, ron, pairing, daftar_start_date, daftar_expired_date,
                ((SPLIT_STRING(daftar_published_letter, '/', 2)*25) + SPLIT_STRING(daftar_published_letter, '/', 3))'reject_number'
                FROM daftar_data a
                INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                LEFT JOIN daftar_registrasi c ON b.registrasi_id = c.registrasi_id
                WHERE b.kode_frekuensi = ? AND c.registrasi_id <> ?
                ORDER BY daftar_published_letter DESC, rute_id ASC
                LIMIT 2";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list editorial kepada
    function get_list_editorial_kepada($params) {
        $sql = "SELECT redaksional_nm
            FROM daftar_tembusan a
            LEFT JOIN redaksional b ON b.redaksional_id = a.tembusan_value
            WHERE a.registrasi_id = ? AND b.redaksional_group = 'kepada'
            ORDER BY a.tembusan_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list editorial tembusan
    function get_list_editorial_tembusan($params) {
        $sql = "SELECT redaksional_nm
            FROM daftar_tembusan a
            LEFT JOIN redaksional b ON b.redaksional_id = a.tembusan_value
            WHERE a.registrasi_id = ? AND b.redaksional_group = 'tembusan'
            ORDER BY a.tembusan_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list redaksional
    function get_list_redaksional($params) {
        $sql = "SELECT a.pref_id, a.pref_group, a.pref_nm, a.pref_value
            FROM com_preferences a
            RIGHT JOIN daftar_tembusan b ON b.tembusan_value = a.pref_nm
            WHERE a.pref_group = 'redaksional' AND b.registrasi_id = ?
            ORDER BY a.pref_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi by registrasi
    function get_total_frekuensi_by_registrasi_id($params) {
        $params = ' ? ';
        return $params;
    }

    /* ================================= MEMO ================================= */

    // get list memos
    function get_list_memos_by_daftar($params) {
        $sql = "SELECT a.*, operator_name
                FROM daftar_memos a
                LEFT JOIN com_user b On a.memo_by = b.user_id
                WHERE registrasi_id = ?
                ORDER BY memo_date DESC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /* ================================= UPDATE ================================= */

    // get detail registrasi published by id
    function get_detail_registrasi_by_id($params) {
        $sql = "SELECT a.*, group_nm, group_alias, u.operator_name'pengirim', airlines_nm, airlines_address, airlines_nationality, 
                p.operator_name'published_by', p.operator_nip, p.operator_pangkat, p.jabatan, (((SPLIT_STRING(daftar_published_letter, '/', 2)*25) + SPLIT_STRING(daftar_published_letter, '/', 3)) * 10000)'published_number'
                FROM daftar_registrasi a
                INNER JOIN daftar_group b ON a.daftar_group = b.group_id
                INNER JOIN airlines c ON a.airlines_id = c.airlines_id
                LEFT JOIN com_user u ON a.daftar_request_by = u.user_id
                LEFT JOIN com_user p ON a.daftar_published_by = p.user_id
                WHERE a.registrasi_id = ? 
                AND daftar_completed = '1' AND daftar_approval = 'rejected'";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar rute by id
    function get_list_daftar_rute_reject_by_id($params) {
        $sql = "SELECT a.daftar_id, a.airlines_id, kode_daftar, kode_frekuensi, registrasi_id, 
                daftar_completed, daftar_approval, daftar_type, daftar_flight, daftar_st, daftar_start_date, daftar_expired_date, 
                daftar_rute_start, daftar_rute_end, pairing, notes,
                MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'total_frekuensi' 
                FROM daftar_rute a
                INNER JOIN daftar_data b On a.daftar_id = b.daftar_id 
                WHERE a.registrasi_id = ? AND a.airlines_id = ? AND a.daftar_approval = 'rejected'
                GROUP BY a.daftar_id
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar data by id
    function get_list_daftar_data_by_id($params) {
        $sql = "SELECT rute_id, a.daftar_id, rute_all, tipe, capacity, flight_no, etd, eta, doop, roon, 
                start_date, end_date, a.is_used_score, GET_FREKUENSI_FROM_DOS(doop)'frekuensi',
                kode_daftar, kode_frekuensi, daftar_approval, b.daftar_penundaan_start, b.daftar_penundaan_end, c.airlines_iata_cd
                FROM daftar_data a
                INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id 
                INNER JOIN airlines c ON c.airlines_id = b.airlines_id 
                WHERE a.daftar_id = ?
                ORDER BY rute_all ASC, etd ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi reject by registrasi
    function get_total_frekuensi_reject_by_registrasi_id($params) {
        $sql = "SELECT 
                MIN(start_date)'start_date', MAX(end_date)'end_date', SUM(frekuensi)'frekuensi',
                IF(MIN(start_date) < CURRENT_DATE, CURRENT_DATE, MIN(start_date))'valid_start_date',
                IF(MAX(end_date) < CURRENT_DATE, CURRENT_DATE, MAX(end_date))'valid_end_date'
                FROM 
                (
                        SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.registrasi_id = ? AND b.daftar_approval = 'rejected'
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get user by role
    function get_com_user_by_role($params) {
        $sql = "SELECT operator_name, operator_nip, operator_pangkat
                FROM com_user a
                INNER JOIN com_role_user b ON a.user_id = b.user_id
                WHERE role_id = ? AND operator_pangkat IS NOT NULL
                LIMIT 0, 1";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // utilities
    function terbilang($x) {
        $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        if ($x < 12)
            return " " . $abil[$x];
        elseif ($x < 20)
            return Terbilang($x - 10) . "belas";
        elseif ($x < 100)
            return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
        elseif ($x < 200)
            return " seratus" . Terbilang($x - 100);
        elseif ($x < 1000)
            return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
        elseif ($x < 2000)
            return " seribu" . Terbilang($x - 1000);
        elseif ($x < 1000000)
            return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
        elseif ($x < 1000000000)
            return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
    }

    // get detail airport
    function get_airport_score_by_code($params) {
        $sql = "SELECT airport_nm, is_used_score, airport_region FROM airport WHERE airport_iata_cd = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list slot uploaded
    function get_list_slot_time_by_id($params) {
        $sql = "SELECT a.* FROM daftar_slot_time a
                INNER JOIN daftar_registrasi b ON a.registrasi_id = b.registrasi_id
                WHERE a.registrasi_id = ? AND airlines_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list redaksional by registrasi
    function get_list_redaksional_by_registrasi($params) {
        $sql = "SELECT a.pref_id, a.pref_group, a.pref_nm, a.pref_value 
                FROM com_preferences a 
                RIGHT JOIN daftar_tembusan b ON b.tembusan_value = a.pref_nm
                WHERE a.pref_group = 'redaksional' AND b.registrasi_id = ?
                ORDER BY a.pref_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list editorial by registrasi
    function get_list_editorial_by_registrasi($params) {
        $sql = "SELECT a.tembusan_id, redaksional_nm 
                FROM daftar_tembusan a 
                INNER JOIN redaksional b ON b.redaksional_id = a.tembusan_value 
                WHERE a.registrasi_id = ? AND b.redaksional_group = ? 
                ORDER BY a.tembusan_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /* ==================== DOWNLOAD PUBLISHED LETTER ==================== */

    // get list daftar rute by kode daftar
    function get_daftar_data_by_registrasi_id($params) {
        $sql = "SELECT rute_id, a.daftar_id, rute_all, tipe, capacity, flight_no, etd, eta, doop, roon, 
                start_date, end_date, a.is_used_score, GET_FREKUENSI_FROM_DOS(doop)'frekuensi',
                kode_daftar, kode_frekuensi, daftar_approval, b.daftar_penundaan_start, b.daftar_penundaan_end, c.airlines_iata_cd
                FROM daftar_data a
                INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id 
                LEFT JOIN airlines c ON c.airlines_id = b.airlines_id 
                WHERE b.registrasi_id = ? AND b.daftar_approval = 'rejected' 
                ORDER BY b.daftar_id ASC, a.rute_all ASC, a.etd ASC, a.start_date";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi
    function get_total_frekuensi($params) {
        $sql = "SELECT a.daftar_id, pairing, MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'total_frekuensi' 
                FROM daftar_rute a
                INNER JOIN daftar_data b On a.daftar_id = b.daftar_id 
                WHERE a.registrasi_id = ? AND a.airlines_id = ? AND a.daftar_id = ? AND a.daftar_approval = 'rejected'
                GROUP BY a.daftar_id
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get published number domestik
    function get_published_number_dom($kode = "DRJU-DAU") {
        // -- AU.008/page/dossier/DRJU-DAU-TAHUN
        $sql = "SELECT SPLIT_STRING(daftar_published_letter, '/', 2)'pages', SPLIT_STRING(daftar_published_letter, '/', 3)'number'
                FROM daftar_registrasi a 
                WHERE RIGHT(SPLIT_STRING(daftar_published_letter, '/', 4), 4) = YEAR(CURRENT_DATE) AND SPLIT_STRING(daftar_published_letter, '/', 1) = 'AU.012'
                ORDER BY ABS(SPLIT_STRING(daftar_published_letter, '/', 2)) DESC, ABS(SPLIT_STRING(daftar_published_letter, '/', 3)) DESC
                LIMIT 0, 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            // create next number
            $pages = intval($result['pages']);
            $number = intval($result['number']) + 1;
            if ($number > 25) {
                $number = 1;
                $pages++;
            }
            return 'AU.012/' . $pages . '/' . $number . '/' . $kode . '-' . date('Y');
        } else {
            return 'AU.012/1/1/' . $kode . '-' . date('Y');
        }
    }

    // get barcode value
    function get_barcode_value($params) {
        $sql = "SELECT * 
            FROM com_preferences a 
            WHERE a.pref_group = 'barcode' AND a.pref_nm = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar rute old by kode frekuensi
    function get_history_daftar_rute_old_by_kode_frekuensi($params) {
        $sql = "SELECT a.daftar_id, a.airlines_id, a.kode_daftar, kode_frekuensi, a.registrasi_id, 
                a.daftar_completed, a.daftar_approval, a.daftar_type, a.daftar_flight, daftar_st, daftar_start_date, daftar_expired_date, 
                a.daftar_rute_start, a.daftar_rute_end, pairing, notes,
                MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'total_frekuensi', c.daftar_published_letter
                FROM daftar_rute a
                INNER JOIN daftar_data b On a.daftar_id = b.daftar_id 
                INNER JOIN daftar_registrasi c ON c.registrasi_id = a.registrasi_id 
                WHERE a.kode_frekuensi = ? AND a.airlines_id = ? 
                AND ((SPLIT_STRING(c.daftar_published_letter, '/', 2)*25) + SPLIT_STRING(c.daftar_published_letter, '/', 3)) < ? 
                AND c.registrasi_id <> ?
                AND a.daftar_completed = '1'
                AND a.daftar_approval = 'approved'
                GROUP BY a.daftar_id
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    
    // get nomor surat sebelumnya
    function get_published_letter_old($params) {
        $sql = "SELECT a.daftar_published_letter, a.daftar_published_date, a.daftar_perihal, a.daftar_flight, b.group_nm 
        FROM daftar_registrasi a 
        LEFT JOIN daftar_group b ON b.group_id = a.daftar_group 
            WHERE a.registrasi_id = ? AND airlines_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

}
