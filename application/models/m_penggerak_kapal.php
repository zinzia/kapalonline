<?php

class m_penggerak_kapal extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    // get total aircraft
    function get_total_penggerak_kapal($params) {
        $sql = "SELECT count(*)'total' FROM daftar_penggerak_kapal
                WHERE penggerak_ket LIKE ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return array();
        }
    }

    // get all data airlines
    function get_all_penggerak_kapal($params) {
        $sql = "SELECT * FROM daftar_penggerak_kapal
                WHERE penggerak_ket LIKE ? 
                ORDER BY penggerak_ket ASC
                LIMIT ?, ?";
				//var_dump($params);
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list data aircraft
    function get_list_penggerak_kapal() {
        $sql = "SELECT * FROM daftar_penggerak_kapal ORDER BY penggerak_ket ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// get all data airport by id
    function get_penggerak_kapal_by_id($params) {
        $sql = "SELECT * FROM daftar_penggerak_kapal 
                WHERE penggerak_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// insert
    function insert($params) {
        $sql = "INSERT INTO daftar_penggerak_kapal (penggerak_ket, penggerak_ket_eng) 
                VALUES (?, ?)";
        return $this->db->query($sql, $params);
    }

// update
    function update($params) {
        $sql = "UPDATE daftar_penggerak_kapal set penggerak_ket = ?, penggerak_ket_eng = ? 
               WHERE penggerak_id = ?";
        return $this->db->query($sql, $params);
    }

// delete
    function delete($params) {
        $sql = "DELETE FROM daftar_penggerak_kapal WHERE penggerak_id = ? ";
        return $this->db->query($sql, $params);
    }

    function is_exist_penggerak_kapal($params) {
        $sql = "SELECT * FROM daftar_penggerak_kapal WHERE penggerak_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $query->free_result();
            // return
            return true;
        } else {
            return false;
        }
    }

}
