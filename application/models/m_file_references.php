<?php

class m_file_references extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    // get total aircraft
    function get_total_file_references($params) {
        $sql = "SELECT count(*)'total' FROM daftar_file_references
                WHERE ref_field LIKE ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return array();
        }
    }

    // get all data airlines
    function get_all_file_references($params) {
        $sql = "SELECT * FROM daftar_file_references
                WHERE ref_field LIKE ? 
                ORDER BY ref_field ASC
                LIMIT ?, ?";
				//var_dump($params);
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list data aircraft
    function get_list_file_references() {
        $sql = "SELECT * FROM daftar_file_references ORDER BY ref_field ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// get all data airport by id
    function get_file_references_by_id($params) {
        $sql = "SELECT * FROM daftar_file_references 
                WHERE ref_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// insert
    function insert($params) {
        $sql = "INSERT INTO daftar_file_references (ref_field, ref_name, ref_size, ref_allowed, ref_required) 
                VALUES (?, ?, ?, ?, ?)";
        return $this->db->query($sql, $params);
    }

// update
    function update($params) {
        $sql = "UPDATE daftar_file_references set ref_field = ?, ref_name = ?, ref_size = ?, ref_allowed = ?, ref_required = ? 
               WHERE ref_id = ?";
        return $this->db->query($sql, $params);
    }

// delete
    function delete($params) {
        $sql = "DELETE FROM daftar_file_references WHERE ref_id = ? ";
        return $this->db->query($sql, $params);
    }

    function is_exist_file_references($params) {
        $sql = "SELECT * FROM daftar_file_references WHERE ref_field = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $query->free_result();
            // return
            return true;
        } else {
            return false;
        }
    }

}
