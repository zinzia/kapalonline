<?php

class m_registrasi extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    // get data id
    function get_data_id() {
        $time = microtime(true);
        $id = str_replace('.', '', $time);
        return $id;
    }

		
	// get sub_group
    function get_subgroup_by_group_id($params) {
        $sql = "SELECT * FROM daftar_subgroup WHERE group_id = ? ORDER BY subgroup_id ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
	
		// list all pelabuhan
    function get_tempat_daftar() {
        $sql = "SELECT * FROM pelabuhan  WHERE st_daftar = '1' 
				ORDER BY pelabuhan_nm ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }


    // get default season
    function get_default_season() {
        $sql = "SELECT pref_value FROM com_preferences WHERE pref_group = 'season' AND pref_nm = 'def'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['pref_value'];
        } else {
            return 'S15';
        }
    }

    // get detail registrasi by id
    function get_registrasi_pending_by_id($params) {
        $sql = "SELECT a.*, group_nm, c.process_id, c.catatan
                FROM daftar_registrasi a
                INNER JOIN daftar_group b ON a.daftar_group = b.group_id
                INNER JOIN daftar_process c ON c.registrasi_id = a.registrasi_id
                WHERE a.registrasi_id = ? AND a.request_by = ? 
                AND a.daftar_request_st = '1' AND a.daftar_completed = '0' AND a.daftar_approval = 'waiting'
                AND a.daftar_group = ? AND c.process_st = 'waiting' AND c.flow_id = ?";
				
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get catatatan permohonan
    function get_catatan_perbaikan_by_registrasi($params) {
        $sql = "SELECT * 
                FROM daftar_process
                WHERE registrasi_id = ?
				AND mdd_finish IS NOT NULL
                ORDER BY mdd_finish DESC 
                LIMIT 1";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return trim($result['catatan']);
        } else {
            return '';
        }
    }

    // get detail daftar rute by id
    function get_daftar_rute_pending_by_id($params) {
        $sql = "SELECT daftar_id, airlines_id, kode_daftar, kode_frekuensi, registrasi_id, 
                daftar_completed, daftar_approval, daftar_type, daftar_flight, daftar_st, daftar_start_date, daftar_expired_date, 
                daftar_rute_start, daftar_rute_end, pairing, is_used_score 
                FROM daftar_rute
                WHERE registrasi_id = ? AND airlines_id = ? 
                AND daftar_completed = '0' 
                AND daftar_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get validasi total rute yang masih aktif
    function get_total_rute_existing_by_new_rute($params) {
        $sql = "SELECT COUNT(*)'total'
                FROM daftar_rute 
                WHERE airlines_id = ? AND (daftar_rute_start = ? OR daftar_rute_start = ? OR daftar_rute_end = ? OR daftar_rute_end = ?)
                AND daftar_completed = '1' AND daftar_approval = 'approved' AND daftar_active = '1'";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }

    // get validasi total rute yang masih waiting on proses
    function get_total_rute_process_by_new_rute($params) {
        $sql = "SELECT COUNT(*)'total' FROM daftar_registrasi 
                WHERE airlines_id = ? AND (daftar_rute_start = ? OR daftar_rute_start = ? OR daftar_rute_end = ? OR daftar_rute_end = ?)
                AND daftar_completed = '0' AND registrasi_id <> ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }

    // get daftar rute dicabut
    function get_total_rute_existing_canceled($params) {
        $sql = "SELECT COUNT(*) 'total', 
                (DATEDIFF(b.daftar_published_date, NOW()) <= 365)'diff'
                FROM daftar_rute a 
                INNER JOIN daftar_registrasi b ON b.registrasi_id = a.registrasi_id
                WHERE a.airlines_id = ? 
                AND (a.daftar_rute_start = ? OR a.daftar_rute_start = ? OR a.daftar_rute_end = ? OR a.daftar_rute_end = ?) 
                AND a.daftar_completed = '1' AND a.daftar_approval = 'approved' AND a.daftar_st = 'pencabutan' AND (DATEDIFF(b.daftar_published_date, NOW()) <= 365) AND (b.daftar_group = '7' OR b.daftar_group = '27') 
                AND b.input_by = 'operator'
                GROUP BY b.registrasi_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }

    // get validasi total rute yang masih sudah diinputkan
    function get_total_rute_by_registrasi_id($params) {
        $sql = "SELECT COUNT(*)'total' 
                FROM daftar_rute
                WHERE registrasi_id = ?
                AND daftar_rute_start <> ? AND daftar_rute_end <> ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }

    // get list daftar rute pending by id
    function get_list_daftar_rute_pending_by_id($params) {
        $sql = "SELECT a.daftar_id, airlines_id, kode_daftar, kode_frekuensi, registrasi_id, 
                daftar_completed, daftar_approval, daftar_type, daftar_flight, daftar_st, daftar_start_date, daftar_expired_date, 
                daftar_rute_start, daftar_rute_end, pairing, 
                MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'total_frekuensi'
                FROM daftar_rute a
                LEFT JOIN daftar_data b On a.daftar_id = b.daftar_id      
                WHERE a.registrasi_id = ? AND a.airlines_id = ? AND a.daftar_completed = '0'
                GROUP BY a.daftar_id
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar data pending by id
    function get_list_daftar_data_pending_by_id($params) {
        $sql = "SELECT rute_id, a.daftar_id, rute_all, tipe, capacity, flight_no, etd, eta, doop, roon, 
                start_date, end_date, a.is_used_score, GET_FREKUENSI_FROM_DOS(doop)'frekuensi',
                kode_daftar, kode_frekuensi
                FROM daftar_data a
                INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                WHERE a.daftar_id = ?
                ORDER BY rute_all ASC, etd ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get daftar rute empty data by id
    function get_daftar_rute_empty_data_by_id($params) {
        $sql = "SELECT IFNULL(MIN(total), 1)'total' 
                FROM 
                (
                        SELECT a.daftar_id, COUNT(b.rute_id)'total'
                        FROM daftar_rute a
                        LEFT JOIN daftar_data b ON a.daftar_id = b.daftar_id
                        WHERE registrasi_id = ? AND airlines_id = ?
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }

    // get daftar rute empty data by id
    function get_total_daftar_data_by_registrasi($params) {
        $sql = "SELECT COUNT(b.rute_id)'total'
                FROM daftar_rute a
                INNER JOIN daftar_data b ON a.daftar_id = b.daftar_id
                WHERE registrasi_id = ? AND airlines_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }

    // get detail airport
    function get_airport_score_by_code($params) {
        $sql = "SELECT airport_iata_cd, is_used_score, airport_utc_sign, airport_utc FROM airport WHERE airport_iata_cd = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // check layanan
    function get_services_flight($params) {
        $sql = "SELECT COUNT(*)'total'
                FROM 
                (
                        SELECT rute_all FROM daftar_data WHERE daftar_id = ? GROUP BY rute_all
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }

    // check nomor penerbangan pada daftar data
    function get_flight_no_by_daftar_id($params) {
        $sql = "SELECT flight_no 
                FROM daftar_data 
                WHERE daftar_id = ? AND rute_all = ?
                GROUP BY flight_no";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['flight_no'];
        } else {
            return '';
        }
    }

    // get list rute aktif by kode
    function get_flight_no_daftar_rute_by_kode_frekuensi($params) {
        $sql = "SELECT flight_no
                FROM daftar_rute a 
                INNER JOIN daftar_data b ON a.daftar_id = b.daftar_id
                WHERE a.airlines_id = ? AND a.daftar_completed = '1'
                AND a.daftar_approval = 'approved' AND a.daftar_flight = ?
                AND a.daftar_active = '1' AND a.kode_frekuensi = ?
                GROUP BY b.flight_no";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $data = array();
            foreach ($result as $rec) {
                $data[] = $rec['flight_no'];
            }
            return $data;
        } else {
            return array();
        }
    }

    // get list rute aktif by kode frekuensi
    function get_detail_daftar_rute_by_id($params) {
        $sql = "SELECT * FROM daftar_rute WHERE daftar_id = ?";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi by registrasi
    function get_total_frekuensi_by_registrasi_id($params) {
        $sql = "SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', SUM(frekuensi)'frekuensi' 
                FROM 
                (
                        SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.registrasi_id = ?
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi by daftar_id
    function get_total_frekuensi_by_daftar_id($params) {
        $sql = "SELECT MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(daftar_id, flight_no))'frekuensi'
                FROM daftar_data a
                WHERE daftar_id = ?
                GROUP BY a.daftar_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi by kode_daftar
    function get_total_frekuensi_existing_by_kode_daftar($params) {
        $sql = "SELECT COUNT(daftar_id)'total_rute', MIN(start_date)'start_date', MAX(end_date)'end_date', SUM(frekuensi)'frekuensi' 
                FROM 
                (
                        SELECT b.daftar_id, MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.kode_daftar = ? AND b.daftar_completed = '1'
                        AND b.daftar_approval = 'approved'
                        AND b.daftar_active = '1'
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar data aktif by id
    function get_list_daftar_data_existing_by_kode_frekuensi($params) {
        $sql = "SELECT rute_id, a.daftar_id, rute_all, tipe, capacity, flight_no, etd, eta, doop, roon, 
                start_date, end_date, a.is_used_score, GET_FREKUENSI_FROM_DOS(doop)'frekuensi',
                kode_daftar, kode_frekuensi
                FROM daftar_data a
                INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                WHERE b.kode_frekuensi = ?
                AND b.daftar_completed = '1'
                AND b.daftar_approval = 'approved'
                AND b.daftar_active = '1' 
                ORDER BY rute_all ASC, etd ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get flight no daftar data aktif by id
    function get_flight_no_daftar_data_existing_by_kode_frekuensi($params) {
        $sql = "SELECT flight_no
                FROM daftar_data a
                INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                WHERE b.kode_frekuensi = ?
                AND b.daftar_completed = '1'
                AND b.daftar_approval = 'approved'
                AND b.daftar_active = '1'
                GROUP BY flight_no";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $flight_no = array(
                '1' => '',
                '2' => '',
            );
            $i = 1;
            foreach ($result as $data) {
                $flight_no[$i++] = $data['flight_no'];
            }
            return $flight_no;
        } else {
            $flight_no = array(
                '1' => '',
                '2' => '',
            );
            return $flight_no;
        }
    }

    // get list daftar rute by kode daftar
    function get_list_daftar_rute_aktif_by_kode_daftar($params) {
        $sql = "SELECT a.daftar_id, a.airlines_id, a.kode_daftar, a.kode_frekuensi, a.registrasi_id, 
                a.daftar_completed, a.daftar_approval, a.daftar_type, a.daftar_flight, a.daftar_st, 
                a.daftar_rute_start, a.daftar_rute_end, pairing, 
                MAX(b.daftar_valid_start)'daftar_start_date', MAX(b.daftar_valid_end)'daftar_expired_date',
                MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'total_frekuensi'
                FROM daftar_rute a
                INNER JOIN daftar_registrasi b ON a.registrasi_id = b.registrasi_id
                LEFT JOIN daftar_data c On a.daftar_id = c.daftar_id 
                WHERE a.kode_daftar = ? AND a.airlines_id = ?
                AND a.daftar_completed = '1'
                AND a.daftar_approval = 'approved'
                AND a.daftar_active = '1'
                GROUP BY a.kode_frekuensi
                ORDER BY a.kode_frekuensi ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi by kode_frekuensi
    function get_total_frekuensi_existing_by_kode_frekuensi($params) {
        $sql = "SELECT COUNT(daftar_id)'total_rute', MIN(start_date)'start_date', MAX(end_date)'end_date', SUM(frekuensi)'frekuensi' 
                FROM 
                (
                        SELECT b.daftar_id, MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.kode_frekuensi = ? AND b.daftar_completed = '1'
                        AND b.daftar_approval = 'approved'
                        AND b.daftar_active = '1'
                        GROUP BY a.daftar_id
                ) result";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list daftar rute by kode frekuensi
    function get_detail_daftar_rute_aktif_by_kode_frekuensi($params) {
        $sql = "SELECT a.*
                FROM daftar_rute a
                WHERE a.kode_frekuensi = ? AND a.airlines_id = ? 
                AND a.daftar_completed = '1'
                AND a.daftar_approval = 'approved'
                AND a.daftar_active = '1'
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /*
     * EXEC
     */

    // UPDATE REGISTRASI
    function update_daftar_registrasi($params, $where) {
        // where
        $this->db->where($where);
        // execute
        return $this->db->update('daftar_registrasi', $params);
    }

    // INSERT RUTE
    function insert_daftar_rute($params) {
        // execute
        return $this->db->insert('daftar_rute', $params);
    }

    // UPDATE RUTE
    function update_daftar_rute($params, $where) {
        // where
        $this->db->where($where);
        // execute
        return $this->db->update('daftar_rute', $params);
    }

    // INSERT DATA RUTE
    function insert_daftar_data($params) {
        // execute
        return $this->db->insert('daftar_data', $params);
    }

    // DELETE RUTE
    function delete_daftar_rute($where) {
        // where
        $this->db->where($where);
        // execute
        return $this->db->delete('daftar_rute', $where);
    }

    // DELETE DATA
    function delete_daftar_data($where) {
        // where
        $this->db->where($where);
        // execute
        return $this->db->delete('daftar_data', $where);
    }

    // DELETE DATA SLOT
    function delete_daftar_data_slot($where) {
        // where
        $this->db->where($where);
        // execute
        return $this->db->delete('daftar_data_slot', $where);
    }

    // INSERT DATA SLOT
    function insert_daftar_data_slot($params) {
        // execute
        return $this->db->insert('daftar_data_slot', $params);
    }

    // INSERT PROCESS
    function insert_daftar_process($params) {
        // execute
        return $this->db->insert('daftar_process', $params);
    }

    // UPDATE PROCESS
    function update_daftar_process($params, $where) {
        // where
        $this->db->where($where);
        // execute
        return $this->db->update('daftar_process', $params, $where);
    }

    /*
     * SLOT
     */

    // get list slot by id
    function get_list_data_slot_by_id($params) {
        $sql = "SELECT a.* FROM daftar_slot_time a
                INNER JOIN daftar_registrasi b ON a.registrasi_id = b.registrasi_id
                WHERE a.registrasi_id = ? AND airlines_id = ?";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get detail slot  by id
    function get_detail_slot_by_id($params) {
        $sql = "SELECT * FROM daftar_slot_time WHERE registrasi_id = ? AND slot_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // INSERT SLOT
    function insert_slot($params) {
        // execute
        return $this->db->insert('daftar_slot_time', $params);
    }

    // UPDATE SLOT
    function update_slot($params, $where) {
        // where
        $this->db->where($where);
        // execute
        return $this->db->update('daftar_slot_time', $params);
    }

    // DELETE SLOT
    function delete_slot($params) {
        $sql = "DELETE a.* FROM daftar_slot_time a
                INNER JOIN daftar_registrasi b ON a.registrasi_id = b.registrasi_id
                WHERE slot_id = ? AND airlines_id = ?";
        return $this->db->query($sql, $params);
    }

    /*
     * FILES ATTACHMENT
     */

    // get detail files  by id
    function get_detail_files_by_id($params) {
        $sql = "SELECT * FROM daftar_files WHERE registrasi_id = ? AND ref_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list files references
    function get_list_file_required_domestik($params) {
        $sql = "SELECT a.* FROM daftar_file_references a
                INNER JOIN daftar_rules_files b ON a.ref_id = b.ref_id
                WHERE b.group_id = ? AND b.data_flight = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list files references
    function get_list_file_required_internasional($params) {
        $sql = "SELECT a.* FROM daftar_file_references a
                INNER JOIN daftar_rules_files b ON a.ref_id = b.ref_id
                WHERE b.group_id = ? AND b.data_flight = ? AND b.airlines_st = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list files uploaded
    function get_list_file_uploaded($params) {
        $sql = "SELECT * FROM daftar_files WHERE registrasi_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list files completed
    function is_file_completed($params) {
        $sql = "SELECT registrasi_id
                FROM daftar_rules_files a
                INNER JOIN daftar_file_references r ON a.ref_id = r.ref_id
                LEFT JOIN daftar_files b ON a.ref_id = b.ref_id AND registrasi_id = ?
                WHERE a.group_id = ? AND a.data_flight = ? AND registrasi_id IS NULL 
                AND ref_required = '1'";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    // update files
    function update_files($delete, $insert) {
        // delete
        $sql = "DELETE FROM daftar_files  WHERE registrasi_id = ? AND ref_id = ?";
        $this->db->query($sql, $delete);
        // insert
        $sql = "INSERT INTO daftar_files (file_id, registrasi_id, file_path, file_name, ref_id, mdd)
                VALUES (?, ?, ?, ?, ?, NOW())";
        return $this->db->query($sql, $insert);
    }

    // get list files completed
    function is_file_completed_int($params) {
        $sql = "SELECT registrasi_id
                FROM daftar_rules_files a
                INNER JOIN daftar_file_references r ON a.ref_id = r.ref_id
                LEFT JOIN daftar_files b ON a.ref_id = b.ref_id AND registrasi_id = ?
                WHERE a.group_id = ? AND a.data_flight = ? AND registrasi_id IS NULL AND a.airlines_st = ?
                AND ref_required = '1'";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    // get total frekuensi by registrasi by kode frekuensi
    function get_total_frekuensi_by_kode_frekuensi($params) {
        $sql = "SELECT b.kode_frekuensi, MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.registrasi_id = ?
                        GROUP BY a.daftar_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get total frekuensi existing by kode_daftar
    function get_total_frekuensi_existing_by_kode_daftar_v2($params) {
        $sql = " SELECT b.kode_frekuensi, MIN(start_date)'start_date', MAX(end_date)'end_date', MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'frekuensi'
                        FROM daftar_data a
                        INNER JOIN daftar_rute b ON a.daftar_id = b.daftar_id
                        WHERE b.kode_daftar = ? AND b.daftar_completed = '1'
                        AND b.daftar_approval = 'approved'
                        AND b.daftar_active = '1'
                        GROUP BY a.daftar_id";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // check daftar rute aktif selected
    function check_daftar_rute_selected($params) {
        $sql = "SELECT COUNT(*)'total'
                FROM daftar_rute
                WHERE kode_frekuensi = ?
                AND airlines_id = ?
                AND daftar_approval = 'waiting'
                AND daftar_active = '0'
                AND daftar_st = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }
    
    // get list daftar rute by kode frekuensi
    function get_list_daftar_rute_aktif_by_kode_frekuensi($params) {
        $sql = "SELECT a.daftar_id, airlines_id, kode_daftar, kode_frekuensi, registrasi_id, 
                daftar_completed, daftar_approval, daftar_type, daftar_flight, daftar_st, daftar_start_date, daftar_expired_date, 
                daftar_rute_start, daftar_rute_end, pairing, notes,
                MAX(GET_FREKUENSI_TOTAL(a.daftar_id, flight_no))'total_frekuensi'
                FROM daftar_rute a
                INNER JOIN daftar_data b On a.daftar_id = b.daftar_id
                WHERE a.kode_frekuensi = ? AND a.airlines_id = ?
                AND a.daftar_completed = '1'
                AND a.daftar_approval = 'approved'
                AND a.daftar_active = '1'
                GROUP BY a.daftar_id
                ORDER BY daftar_rute_start ASC";
        $query = $this->db->query($sql, $params);
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    
    // get total daftar rute terpilih
    function get_total_daftar_rute_terpilih($params) {
        $sql = "SELECT daftar_id FROM daftar_rute a
                WHERE a.registrasi_id = ? AND a.airlines_id = ? AND a.daftar_completed = '0'
                AND kode_daftar IS NOT NULL AND kode_frekuensi IS NOT NULL";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return true;
        } else {
            return false;
        }
    }
    
    // check daftar rute aktif by perpanjangan
    function check_daftar_rute_by_perpanjangan($params) {
        $sql = "SELECT COUNT(*)'total'
                FROM daftar_rute
                WHERE daftar_kode_old = ?
                AND airlines_id = ?
                AND daftar_completed = '1'
                AND daftar_approval = 'approved'
                AND daftar_active = '1'";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return 0;
        }
    }
}
