<?php

class m_jenisdetail_kapal extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    // get total aircraft
    function get_total_jenisdetail_kapal($params) {
        $sql = "SELECT count(*)'total' FROM daftar_jenisdetail_kapal
                WHERE jenisdetail_ket LIKE ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return array();
        }
    }

    // get all data
    function get_all_jenisdetail_kapal($params) {
        $sql = "SELECT * FROM daftar_jenisdetail_kapal a 
		        LEFT JOIN daftar_jenis_kapal b 
				ON a.jenis_id = b.jenis_id
                WHERE a.jenisdetail_ket LIKE ? 
                ORDER BY a.jenisdetail_ket ASC
                LIMIT ?, ?";
				//var_dump($params);
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    function get_all_jenis_kapal() {
        $sql = "SELECT * FROM daftar_jenis_kapal  
                ORDER BY jenis_ket ASC";
				//var_dump($params);
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list data aircraft
    function get_list_jenisdetail_kapal() {
        $sql = "SELECT * FROM daftar_jenisdetail_kapal ORDER BY jenisdetail_ket ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// get all data airport by id
    function get_jenisdetail_kapal_by_id($params) {
        $sql = "SELECT * FROM daftar_jenisdetail_kapal 
                WHERE jenisdetail_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// insert
    function insert($params) {
        $sql = "INSERT INTO daftar_jenisdetail_kapal (jenisdetail_ket, jenis_id) 
                VALUES (?, ?)";
        return $this->db->query($sql, $params);
    }

// update
    function update($params) {
        $sql = "UPDATE daftar_jenisdetail_kapal set jenisdetail_ket = ?, jenis_id = ? 
               WHERE jenisdetail_id = ?";
        return $this->db->query($sql, $params);
    }

// delete
    function delete($params) {
        $sql = "DELETE FROM daftar_jenisdetail_kapal WHERE jenisdetail_id = ? ";
        return $this->db->query($sql, $params);
    }

    function is_exist_jenisdetail_kapal($params) {
        $sql = "SELECT * FROM daftar_jenisdetail_kapal WHERE jenisdetail_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $query->free_result();
            // return
            return true;
        } else {
            return false;
        }
    }

}
