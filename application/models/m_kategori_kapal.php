<?php

class m_kategori_kapal extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    // get total aircraft
    function get_total_kategori_kapal($params) {
        $sql = "SELECT count(*)'total' FROM daftar_kategori_kapal
                WHERE kategori_nm LIKE ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return array();
        }
    }

    // get all data airlines
    function get_all_kategori_kapal($params) {
        $sql = "SELECT * FROM daftar_kategori_kapal
                WHERE kategori_nm LIKE ? 
                ORDER BY kategori_nm ASC
                LIMIT ?, ?";
				//var_dump($params);
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list data aircraft
    function get_list_kategori_kapal() {
        $sql = "SELECT * FROM daftar_kategori_kapal ORDER BY kategori_nm ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// get all data airport by id
    function get_kategori_kapal_by_id($params) {
        $sql = "SELECT * FROM daftar_kategori_kapal 
                WHERE kategori_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// insert
    function insert($params) {
        $sql = "INSERT INTO daftar_kategori_kapal (kategori_nm, kategori_kd) 
                VALUES (?, ?)";
        return $this->db->query($sql, $params);
    }

// update
    function update($params) {
        $sql = "UPDATE daftar_kategori_kapal set kategori_nm = ?, kategori_kd = ?
               WHERE kategori_id = ?";
        return $this->db->query($sql, $params);
    }

// delete
    function delete($params) {
        $sql = "DELETE FROM daftar_kategori_kapal WHERE kategori_id = ? ";
        return $this->db->query($sql, $params);
    }

    function is_exist_kategori_kapal($params) {
        $sql = "SELECT * FROM daftar_kategori_kapal WHERE kategori_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $query->free_result();
            // return
            return true;
        } else {
            return false;
        }
    }

}
