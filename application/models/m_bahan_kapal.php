<?php

class m_bahan_kapal extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    // get total aircraft
    function get_total_bahan_kapal($params) {
        $sql = "SELECT count(*)'total' FROM daftar_bahan_kapal
                WHERE bahan_ket LIKE ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['total'];
        } else {
            return array();
        }
    }

    // get all data airlines
    function get_all_bahan_kapal($params) {
        $sql = "SELECT * FROM daftar_bahan_kapal
                WHERE bahan_ket LIKE ? 
                ORDER BY bahan_ket ASC
                LIMIT ?, ?";
				//var_dump($params);
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    // get list data aircraft
    function get_list_bahan_kapal() {
        $sql = "SELECT * FROM daftar_bahan_kapal ORDER BY bahan_ket ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// get all data airport by id
    function get_bahan_kapal_by_id($params) {
        $sql = "SELECT * FROM daftar_bahan_kapal 
                WHERE bahan_id = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

// insert
    function insert($params) {
        $sql = "INSERT INTO daftar_bahan_kapal (bahan_ket, bahan_ket_eng) 
                VALUES (?, ?)";
        return $this->db->query($sql, $params);
    }

// update
    function update($params) {
        $sql = "UPDATE daftar_bahan_kapal set bahan_ket = ?, bahan_ket_eng = ? 
               WHERE bahan_id = ?";
        return $this->db->query($sql, $params);
    }

// delete
    function delete($params) {
        $sql = "DELETE FROM daftar_bahan_kapal WHERE bahan_id = ? ";
        return $this->db->query($sql, $params);
    }

    function is_exist_bahan_kapal($params) {
        $sql = "SELECT * FROM daftar_bahan_kapal WHERE bahan_ket = ?";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $query->free_result();
            // return
            return true;
        } else {
            return false;
        }
    }

}
